export const environment = {
  production: true,
  //documentPathPrefix: 'C:/Users/desom/Desktop/newest_dist/src/assets/UserDoc',
  documentPathPrefix: '../assets/UserDoc',
  templatePathPrefix: '../assets/templates',
  apiUrlPrefix: 'https://e-crew.skom.com.my/api'
}
