import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  HttpHeaders
} from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // private apiServer = 'http://localhost:9000'
  private apiServer = 'http://192.168.1.9:9000'
  // private apiServer = 'http://eportal-ut.namcheong.com.my/api'
  // private apiServer = 'http://uat-ecrew.skom.com.my/api'
  // private apiServer = 'https://e-crew.skom.com.my/api'

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'lang': localStorage.getItem('language')
    })
  }

  constructor(private http: HttpClient) { }

  login(email: string, username: string, password: string): Observable<boolean> {
    //localStorage.removeItem('access_token');
    return this.http.post<{token: string}>( `${this.apiServer}` + '/users/loginuser',
    {email: email, username: username, password: password}, this.httpOptions)
      .pipe(
        map(result => {
          if (localStorage.getItem('remember_me') != 'true') {
            sessionStorage.setItem('access_token', result.token);
          } else {
            localStorage.setItem('access_token', result.token);
          }


          return true;
        })
      );
  }

  logout() {
    localStorage.removeItem('access_token');
    localStorage.clear();
    sessionStorage.clear();
    console.log("remove token: ", localStorage.getItem('access_token'))
  }

  getAccessToken(): string {
    if (localStorage.getItem('access_token')) {
      return (localStorage.getItem('access_token'));
    } else {
      return (sessionStorage.getItem('access_token'));
    }
  }

  public get loggedIn(): boolean {
    if (localStorage.getItem('access_token')) {
      return (localStorage.getItem('access_token') !== null);
    } else {
      return (sessionStorage.getItem('access_token') !== null);
    }
  }
}