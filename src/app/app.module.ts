/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core'
import { HttpClient, HttpClientModule } from '@angular/common/http'
import { CoreModule } from './@core/core.module'
import { ThemeModule } from './@theme/theme.module'
import { AppComponent } from './app.component'
import { AppRoutingModule } from './app-routing.module'
import { MatDialogModule } from '@angular/material/dialog'
import { MatButtonModule} from '@angular/material/button'
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RouterModule } from '@angular/router'

import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
  NbTreeGridModule
  //NbCheckboxModule,
} from '@nebular/theme'

import { FormsModule } from '@angular/forms'
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime'
import {
  SmartTableDatepickerComponent,
  SmartTableDatepickerRenderComponent,
} from './smart-table-datepicker/smart-table-datepicker.component'
import { CheckboxComponent } from './shared/CheckboxComponent.component'
import { DownloadButtonModule } from './shared/DownloadButton.module'
import { DialogContentExampleDialog, SimpleDialogComponent } from './pages/applicant_jobportal/crew-job-portal.component'
import { PagesModule } from './pages/pages.module';
import { PasswordDialogModule } from './shared/password-dialog/password-dialog.module'
import { SignatureDialogModule } from './shared/signature-dialog/signature-dialog.module'

// authentication module
//import { AuthService } from './auth.service';
import { BaseService } from './services/base.service'
import { AuthGuard } from './auth.guard';
import { AdminAuthGuard } from './admin-auth.guard';

import { JwtModule } from '@auth0/angular-jwt';
import { SignatureComponent } from './pages/jobportal/signature/signature.component';
import { SignatureDialogComponent } from './shared/signature-dialog/signature-dialog.component';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { environment } from '../environments/environment'

export function tokenGetter() {
  if (localStorage.getItem('access_token')) {
    return localStorage.getItem('access_token');
  } else {
    return sessionStorage.getItem('access_token');
  }
}

@NgModule({
  declarations: [
    AppComponent,
    SmartTableDatepickerComponent,
    SmartTableDatepickerRenderComponent,
    DialogContentExampleDialog,
    SimpleDialogComponent,
    SignatureComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    ThemeModule.forRoot(),

    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    NbTreeGridModule,
    CoreModule.forRoot(),
    //NbCheckboxModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    DownloadButtonModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    // Add this import here
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['localhost:4200', 'http://uat-ecrew.skom.com.my'],
        disallowedRoutes: ['localhost:4200/api/auth']
      }
    }),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    PagesModule,
    PasswordDialogModule,
    SignatureDialogModule
  ],
  // exports: [
  //   MatButtonModule,
  //   MatFormFieldModule,
  //   MatInputModule,
  //   MatRippleModule,
  // ],
  entryComponents: [
    SmartTableDatepickerComponent,
    SmartTableDatepickerRenderComponent,
    CheckboxComponent,
    DialogContentExampleDialog,
    SimpleDialogComponent,
    SignatureComponent
    
  ],
  bootstrap: [AppComponent],
  providers: [
    BaseService,
    AuthGuard,
    AdminAuthGuard
  ],
})
//export class MaterialModule {};

export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
