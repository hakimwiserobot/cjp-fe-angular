export interface User {
  Id: string
  LoginEmail: string
  username: string
  email: string
  password: string
  new_password: string
  new_email: string
  retyped_password: string
  current_acc_password: string
}

export interface Admin {
  Id: string
  LoginEmail: string
  username: string
  email: string
  password: string
  new_password: string
  new_email: string
  retyped_password: string
  current_acc_password: string
}