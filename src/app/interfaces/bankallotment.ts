export interface BankAllotment {
    Id:number
    BeneficiaryType:string
    Name:string
    MiddleName:string
    LastName:string
    Relationship:string
    ContactCtryCode:string
    ContactNumber:string
    IdentityType:String
    Identification:string

    BankName:string
    BankAccountNum:string
    BankPlace:string
    BankSwissCode:string

    Currency:string

    FileBankBook:string
    FileMarriageCert:string
    FileICPassport:string
    FileBirthCert:string
    FileBirthCert2:string
    FileFamilyCard:string

    Signature:string
    isSelected:boolean
  }