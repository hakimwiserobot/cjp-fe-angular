import { Component, OnDestroy, OnInit } from '@angular/core'
import { UserService } from '../../services/user.service'
import { Injectable } from '@angular/core'
import { Subscription } from 'rxjs'
import { Router, ActivatedRoute } from '@angular/router'

@Injectable({
  providedIn: 'root'
})

@Component({
  templateUrl: './verify-email.html'
})
export class VerifyEmailComponent implements OnInit, OnDestroy {
  _subscription: Subscription
  email: string
  forgot_error: string = null;
  forgot_done: string = null;

  constructor(
    private service: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.verifyEmail();
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe()
    }
  }

  verifyEmail() {
    var token = this.route.snapshot.queryParamMap.get('token');
    var email = this.route.snapshot.queryParamMap.get('email');

    this._subscription = this.service.verifyEmail({ 
      token: token, email: email }
      ).subscribe(
        (result: any) => {
          alert(result.message)
          setTimeout(() => {
            this.router.navigateByUrl('/applicant-login');
          }
          , 500);
        },
        (err: any) => {
          console.log(err)
          if(err) {
            alert('Failed to activate email! Please try again.')
          }
        }
      );
  }

}
