import { Component, OnDestroy, OnInit } from '@angular/core'
import { UserService } from '../../services/user.service'

import { User } from '../../interfaces/user'

import { Injectable } from '@angular/core'
import { Subscription } from 'rxjs'

import { Router, ActivatedRoute, CanActivate,  ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
//import * as bcrypt from 'bcryptjs';

import { BaseService } from '../../services/base.service'
import { first } from 'rxjs/operators';

import { TranslateService } from '@ngx-translate/core';


@Injectable({
  providedIn: 'root'
})

@Component({
  templateUrl: './user-login.html',
  styleUrls: ['./user-login.component.scss'],
})
export class UserLoginComponent implements OnInit, OnDestroy{
  _subscription: Subscription
  login_user: User;
  email: string;
  password: string;
  username: string;
  retyped_password: string;
  register_email: string;
  register_password: string;
  register_username: string;
  register_retyped_password: string;
  register_idtype: string;
  register_idnumber: string;
  login_error: string = null;
  register_error: string = null;
  routeURL: string;
  isRememberMe: boolean = false;
  btnLblLanguage: string = 'BM'
  //salt = bcrypt.genSaltSync(10);

  isLoadingLogin: boolean = false
  isLoadingRegister: boolean = false

  constructor(private service: UserService, 
    private router: Router,
    private baseService: BaseService,
    private translate: TranslateService) {

    this.routeURL = this.router.url;
  }

  ngOnInit(): void {

    var language = localStorage.getItem('language');
    if (language == 'en') {
      this.btnLblLanguage = 'BM';
    } else {
      this.btnLblLanguage = 'EN';
    }
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe()
    }
  }

  onChangeTxtfld() {
    this.login_error = '';
    this.register_error = '';
  }

  loginUser() {
    this.login_error = '';
    const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    if (!emailRegexp.test(this.email)) {
      alert(this.translate.instant('userLoginPage.error.email'));
    } else {
      const btnLogin = (<HTMLInputElement>document.getElementById('btn_login'))
      btnLogin.className = 'btn btn-theme-loading';
      this.isLoadingLogin = true;

      localStorage.setItem('remember_me', this.isRememberMe.toString())
      this.baseService.loginUser(this.email, this.username, this.password)
        .pipe(first())
        .subscribe(
          (result: any) => {
            console.log("result: ", result, " email: ", this.email)
            var redirectUrl = sessionStorage.getItem('redirect');
            sessionStorage.setItem('redirect', '');
            if (redirectUrl != null && redirectUrl != '' && redirectUrl.search(this.email) > 1) {
              var url = new URL(redirectUrl);
              this.router.navigateByUrl(url.pathname+url.search);
            } else {
              this.router.navigate(['/pages/applicant_jobportal/dashboard-applicant/'], { queryParams: { email: this.email }})
            }            
          },
          (err: any) => {
            if(err) {
              this.login_error = err.error.error
            }
            if (this.login_error == null) {
              this.login_error = this.translate.instant('userLoginPage.error.authenticate');
            }
            console.log(err)
            
            btnLogin.className = 'btn btn-theme';
            this.isLoadingLogin = false;
          }
        );
    }
  }

  logoutUser() {
    this.baseService.logout();
    this.router.navigate(['applicant-login']);
  }

  registerUser() {
    //this.login_user.password = bcrypt.hashSync(this.login_user.password, this.salt)
    this.register_error = '';
    const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    if (!emailRegexp.test(this.register_email)) {
      alert ("Invalid email address");
    } else {
      const btnRegister = (<HTMLInputElement>document.getElementById('btn_register'))
      btnRegister.className = 'btn btn-theme-loading';
      this.isLoadingRegister = true;

      const nric = this.register_idtype == "IC" ? this.register_idnumber : '';
      const passport = this.register_idtype == "Passport" ? this.register_idnumber : '';

      this.baseService.registerUser(this.register_email, this.register_username, nric, passport, this.register_password, this.register_retyped_password)
      .pipe(first())
      .subscribe(
        (result: any) => {
          console.log("result: ", result, " email: ", this.register_email)
          alert(this.translate.instant("alert.activationEmail"))

          this.router.navigate(['/pages/applicant_jobportal/dashboard-applicant/'], { queryParams: { email: this.register_email }})
        },
        (err: any) => {
          console.log("error: ", err)
          this.register_error = 'Unable to register. Please try again.'
          console.log(this.register_error)
          if(err) {
            if (err.error != null && err.error.error != null) {
              this.register_error = err.error.error;
            }
          }
          btnRegister.className = 'btn btn-theme';
          this.isLoadingRegister = false;
        }
      );
    }
  }

  useLanguage(event) {
    if (event.srcElement.innerText == "BM") {
      event.srcElement.innerText = 'EN'
      localStorage.setItem('language', 'my');
      this.translate.use('my');
    } else {
      event.srcElement.innerText = 'BM'
      localStorage.setItem('language', 'en');
      this.translate.use('en');
    }
  }
}