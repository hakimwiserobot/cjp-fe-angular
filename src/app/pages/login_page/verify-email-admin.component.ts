import { Component, OnDestroy, OnInit } from '@angular/core'
import { UserService } from '../../services/user.service'

import { User } from '../../interfaces/user'

import { Injectable } from '@angular/core'
import { Subscription } from 'rxjs'

import { Router, ActivatedRoute } from '@angular/router'

@Injectable({
  providedIn: 'root'
})

@Component({
  templateUrl: './verify-email-admin.html'
})

export class VerifyEmailAdminComponent implements OnInit, OnDestroy {
  _subscription: Subscription
  email: string
  forgot_error: string = null;
  forgot_done: string = null;

  constructor(
    private service: UserService, 
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.verifyEmailAdmin();
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe()
    }
  }

  verifyEmailAdmin() {
    var token = this.route.snapshot.queryParamMap.get('token');
    var email = this.route.snapshot.queryParamMap.get('email');

    this._subscription = this.service.verifyEmailAdmin({ 
      token: token, email: email }
      ).subscribe(
        (result: any) => {
          alert(result.message)
          setTimeout(() => {
            this.router.navigate(['/admin']);
          }
          , 500);
        },
        (err: any) => {
          console.log(err)
          if(err) {
            alert('Failed to activate email! Please try again')
          }
        }
      );
  }

}
