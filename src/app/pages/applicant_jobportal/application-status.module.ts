import { NgModule } from '@angular/core'
import { NbCardModule } from '@nebular/theme'

import { ThemeModule } from '../../@theme/theme.module'
import { ApplicationStatusComponent } from './application-status.component'
import { TranslateModule } from '@ngx-translate/core'

@NgModule({
  imports: [NbCardModule, ThemeModule, TranslateModule],
  declarations: [ApplicationStatusComponent],
})
export class ApplicationStatusModule {}
