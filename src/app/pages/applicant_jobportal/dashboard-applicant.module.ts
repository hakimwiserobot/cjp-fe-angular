import { NgModule } from '@angular/core'
import { NbCardModule } from '@nebular/theme'

import { ThemeModule } from '../../@theme/theme.module'
import { DashboardApplicantComponent } from './dashboard-applicant.component'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [NbCardModule, ThemeModule, TranslateModule],
  declarations: [DashboardApplicantComponent],
})
export class DashboardApplicantModule {}
