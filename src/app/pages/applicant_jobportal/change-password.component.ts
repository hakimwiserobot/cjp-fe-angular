import { Component, OnDestroy, OnInit } from '@angular/core'
import { UserService } from '../../services/user.service'
import { BaseService } from '../../services/base.service'

import { User } from '../../interfaces/user'

import { Injectable } from '@angular/core'
import { Subscription } from 'rxjs'

import { Router, ActivatedRoute } from '@angular/router'
import { TranslateService } from '@ngx-translate/core';
//import * as bcrypt from 'bcryptjs';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'ngx-applicant',
  templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  _subscription: Subscription
  //user_change_password: User;
  email: string
  password: string
  new_password: string;
  activateStatus: string
  new_email: string;
  current_acc_password: string
  username: string;
  retyped_password: string;
  login_error: string = null;
  //salt = bcrypt.genSaltSync(10);

  constructor(private service: UserService, private router: Router,
    private route: ActivatedRoute, private baseService: BaseService,
    private translate: TranslateService) { 
      //console.log("constructor href: ", window.location.href)
    }

  ngOnInit(): void {
    this.username = localStorage.getItem('user_name');
    this.email = localStorage.getItem("user_email");
    this.activateStatus = localStorage.getItem("verify_status");
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe()
    }
  }

  _refreshData() {
  }

  changeEmail() {

    if (!this.new_email || !this.current_acc_password) {
      alert(this.translate.instant('userChangePasswordPage.alert.emptyFields'))
    } else {
      const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
      if (!emailRegexp.test(this.new_email) || (this.new_email == this.email)) {
        alert(this.translate.instant('userChangePasswordPage.alert.invalidEmail'));
      } else {

        this._subscription = this.service.updateUserEmail({ 
          email: this.email,
          new_email: this.new_email,
          username: this.username,
          password: this.current_acc_password }
          ).subscribe((result: any) => {

          if(result.token == null) {
            if(result.error) {
              this.login_error = result.error
              alert(this.login_error)
            }
          } else {
            this.new_email = '';
            this.current_acc_password = '';

            alert(this.translate.instant('userChangePasswordPage.alert.successChgEmail'));

            this.baseService.logout();
            this.router.navigate(['applicant-login']);
          }
          this._subscription.unsubscribe()
        },
        (error) => {
          alert(error);
        })
      }
    }
  }

  resendActivation() {
    this._subscription = this.service.resendActivation({ 
      email: this.email,
      username: this.username })
      .subscribe(
        (result: any) => {
          alert(this.translate.instant('userChangePasswordPage.alert.resendActivation'))
        },
        (err: any) => {
          alert(this.translate.instant('userChangePasswordPage.alert.errorResendActivation'))
        }
      );
  }

  changePassword() {
    //console.log("password: ", this.new_password)
    this._subscription = this.service.updateUserPassword({ 
        email: this.email,
        password: this.password,
        new_password: this.new_password,
        retyped_password: this.retyped_password }
        ).subscribe((result: any) => {
        if(result.token == null) {
          if(result.error) {
            //console.log(result)
            this.login_error = result.error
            alert(this.login_error)
          }
        } else {
          alert(this.translate.instant('userChangePasswordPage.alert.passwordChg'));
        
          this.password = '';
          this.new_password = '';
          this.retyped_password = '';
        }
        this._subscription.unsubscribe()
      },
      (error) => {
        alert(this.translate.instant('userChangePasswordPage.alert.incorrectPassword'));
      })
  }
}