import { Component, OnDestroy, OnInit, Injectable, ViewChild } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { formatDate } from '@angular/common'
import { Subscription } from 'rxjs'
import { MatDialog } from '@angular/material/dialog'
import { TranslateService } from '@ngx-translate/core';

import { ApplicantService } from '../../services/applicant.service'
import { ApplicationService } from '../../services/application.service'
import { GenerateDocService } from '../../services/generateDoc.service'
import { DownloadService } from '../../services/download.service'

import { Applicant } from '../../interfaces/applicant'

import { environment } from '../../../environments/environment'

import { SimpleDialogComponent, DialogContentExampleDialog } from './crew-job-portal.component'

import { SignatureDialogComponent } from '../../shared/signature-dialog/signature-dialog.component'
import { PasswordDialogComponent } from '../../shared/password-dialog/password-dialog.component'

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'ngx-applicationacceptance',
  templateUrl: './application-acceptance.component.html',
  styleUrls: ['../jobportal/open-vacancy.component.scss', './application-acceptance.component.scss'],
})
export class ApplicationAcceptanceComponent implements OnInit, OnDestroy {
  applicationId: string = '';
  applicationEmail: string = '';

  fullname: string = '';
  nric: string = '';
  passport: string = '';
  position: string = '';
  vessel: string = '';
  startDate: string = '';
  endDate: string = '';
  dailyRate: string = '';
  allowance: string = '';
  allowanceRemark: string = '';
  allowanceStandby: string = '';
  allowanceStandbyRemark: string = '';
  allowanceOther: string = '';
  allowanceOtherRemark: string = '';
  urlSEADoc: string = '';
  signature: string = '';
  isAccepted: boolean = false;
  isRejected: boolean = false;
  isTerminated: boolean = false;
  adminName: string = ''

  _subscription: Subscription
  isLoading = false
  standbyRates = []

  constructor(private translate: TranslateService, 
    private applicantService: ApplicantService, 
    private applicationService: ApplicationService,
    private generateDocService: GenerateDocService,
    private downloadService: DownloadService,
    private activatedRoute: ActivatedRoute, 
    private router:Router,
    public dialog: MatDialog,) {
    
  }

  ngOnInit(): void {
    this.applicationId = this.activatedRoute.snapshot.queryParamMap.get('ApplyID');
    this.applicationEmail = this.activatedRoute.snapshot.queryParamMap.get('LoginEmail');
    
    this.getApplicant();
    this.getApplication();
  }

  ngOnDestroy(): void { }

  private getApplicant() {
    this.applicantService.getApplicantByLoginEmail(this.applicationEmail).subscribe(
      (result:Applicant) => {
        this._refreshApplicantData(result);
      },
      (err) => alert('Failed to load applicant')
    )
  }

  private getApplication() {
    var path = this.applicationEmail + '/' + this.applicationId
    this.applicantService.getApplicantApplyById(path).subscribe(
      (result) => {
        this._refreshApplicationData(result);
      },
      (err) => {
        alert('Failed to load applicant')
      }
    )
  }

  private _refreshApplicantData(data:Applicant) {
    this.fullname = data.Name
    if (data.MiddleName != null && data.MiddleName != '') {
      this.fullname += ' ' + data.MiddleName
    }
    if (data.LastName != null && data.LastName != '') {
      this.fullname += ' ' + data.LastName
    }

    this.nric = data.IC ? data.IC : null;
    this.passport = data.Passport ? data.Passport : null;
  }

  private _refreshApplicationData(data) {
    this.standbyRates = [
      { Id: 1, Rate: this.translate.instant('selection.title.halfDailyRate') },
      { Id: 2, Rate: this.translate.instant('selection.title.fullDailyRate') },
    ]
    
    this.position = data.OfferPosition;
    this.vessel = data.NameofVessel;

    if (data.ContractPeriodFrom != null && data.ContractPeriodFrom != '') {
      this.startDate = formatDate(data.ContractPeriodFrom, 'dd/MM/yyyy', 'en-MY');
    }
    if (data.ContractPeriodTo != null && data.ContractPeriodTo != '') {
      this.endDate = formatDate(data.ContractPeriodTo, 'dd/MM/yyyy', 'en-MY');
    }

    this.dailyRate = data.Currency + ' ' + (data.DailyRate ? data.DailyRate : '0') + ' / day'
    this.allowance = data.Currency + ' ' + (data.Allowance ? data.Allowance : '0') + ' / day'
    this.allowanceStandby = data.Currency + ' ' + (data.StandbyAllowance ? data.StandbyAllowance : '0') + ' / day'
    this.allowanceOther = data.Currency + ' ' + (data.OtherAllowance ? data.OtherAllowance : '0') + ' / day'

    if (data.TypesofAllowance) {
      this.allowanceRemark = "(" + data.TypesofAllowance + ")";
    }
    if (data.StandbyRate) {
      this.allowanceStandbyRemark = "(" + this.standbyRates.find(rate => rate.Id == data.StandbyRate).Rate+ ")";
    }
    if (data.allowanceRemark) {
      this.allowanceOtherRemark = "(" + data.allowanceRemark + ")";
    }

    if (data.AcceptFlag == 'Y') {
      this.isAccepted = true;
      this.isRejected = false;
      this.isTerminated = false;
    } else if (data.AcceptFlag == 'N') {
      this.isAccepted = false;
      this.isRejected = true;
      this.isTerminated = false;
    }

    if (data.Status == "Terminated") {
      this.isAccepted = false;
      this.isRejected = false;
      this.isTerminated = true;
    }

    this.urlSEADoc = data.FileSEA;
    this.adminName = data.ConfirmByName;
  }

  submitAsReject() {
    if (window.confirm(this.translate.instant('userApplicationAcceptancePage.alert.rejectOffer'))) {
      this.applicationService.updateAsReject(this.applicationId, this.applicationEmail).subscribe(
        (result) => {
          if (result['success']) {
            alert(result['message']);
            this.getApplication();
            // this.router.navigate(['/pages/applicant_jobportal/application-status']);
          }
        },
        (err) => alert('Failed to submit')
      )
    }
  }

  submitAsAccept() {
    if (window.confirm(this.translate.instant('userApplicationAcceptancePage.alert.acceptOffer'))) {

      let modalSignatureConfig = {}

      if (window.innerWidth < 768) {
        modalSignatureConfig = {
          position: {
            top: '0',
            right: '0'
          },
          height: '100%',
          width: '100vw',
          maxWidth: '100vw',
          panelClass: ['custom-dialog'],
          data: this.signature
        }
      } else {
        modalSignatureConfig = {
          width: '100%',
          height: '95%',
          panelClass: ['custom-dialog'],
          data: this.signature
        }
      }
      
      // Request signature
      const dialogRef = this.dialog.open(SignatureDialogComponent,
        modalSignatureConfig
      );
      dialogRef.afterClosed().subscribe(result => {
        console.log('The signature dialog was closed');

        if (result) {
          this.signature = result;

          // Request password
          const dialogRef2 = this.dialog.open(PasswordDialogComponent, {
            panelClass: 'custom-modalbox',
            data:this.applicationEmail
          });
          dialogRef2.afterClosed().subscribe(result => {
            console.log('The password dialog was closed');

            if (result) {
              this.isLoading = true;

              const btnAccept = (<HTMLInputElement>document.getElementById('btnAccept'));
              const btnReject = (<HTMLInputElement>document.getElementById('btnReject'));
              
              btnAccept.disabled = true;
              btnReject.disabled = true;

              this.applicationService.updateAsAccept(this.applicationId, this.applicationEmail, this.signature).subscribe(
                async (result) => {
                  if (result['success']) {
                    await this.onGenerateSEA();
                    await this.onGenerateBAF();
                    await this.onSendAcceptedEmail();

                    alert(result['message']);
                    this.getApplication();
                    // this.router.navigate(['/pages/applicant_jobportal/application-status']);
                    btnAccept.disabled = false;
                    btnReject.disabled = false;
                  }
                },
                (err) => { 
                  alert('Failed to submit')
                  btnAccept.disabled = false;
                  btnReject.disabled = false;
                }
              )
            }
          });
        }
      });
    }
  }

  onGenerateSEA() {
    return new Promise((resolve, reject) => {
      const subscription = this.generateDocService.SEA(this.applicationEmail, this.applicationId, this.adminName).subscribe(
        (result: any[]) => {
          console.log('Successfully generate signed SEA document')
          resolve(true)
        },
        (err) => {
          resolve(false)
        }
      )
    })
  }

  onGenerateBAF() {
    return new Promise((resolve, reject) => {
      const subscription = this.generateDocService.BAF(this.applicationEmail, this.applicationId, this.adminName).subscribe(
        (result: any[]) => {
          console.log('Successfully generate BAF document')
          resolve(true)
        },
        (err) => {
          alert('Failed to generate BAF document');
          resolve(false)
        }
      )
    })
  }

  onSendAcceptedEmail() {
    return new Promise((resolve, reject) => {
      const subscription = this.applicationService.sendEmailAccepted(this.applicationId).subscribe(
        (result: any[]) => {
          console.log('Successfully send accepted email')
          resolve(true)
        },
        (err) => {
          alert('Failed to send accepted email');
          resolve(false)
        }
      )
    })
  }

  downloadSEA() {
    if (this.urlSEADoc) {
      var link= document.createElement('a');
      var url = environment.documentPathPrefix +'/'+ this.applicationEmail + '/' + this.urlSEADoc;
      link.href = url;
      var reg = / /g;
      link.download = 'SEA'+ this.fullname.replace(reg, '_').toLowerCase() + '.pdf';
      link.click();
      console.log(link)
    } else {
      alert(this.translate.instant('userApplicationAcceptancePage.alert.noDoc'))
    }
  }

  viewSEA() {
    if (this.urlSEADoc) {
      // var url = environment.documentPathPrefix + '/' + this.applicationEmail + '/' + this.urlSEADoc;
      // var url = 'http://localhost:4200/assets/UserDoc/hakim@wiserobot.com/Chief Steward_hakim@wiserobot.com_SEA_20220622114657_signed_declare-result.pdf';
      // window.open(url);

      this.downloadService.file(this.applicationEmail, this.urlSEADoc).subscribe(
        (result: any) => {
          if (result) {
            var url = window.URL.createObjectURL(result);
            var elementA = document.createElement("a");
            elementA.href = url;
            elementA.target = '_blank';
            // elementA.download = 'SEA.pdf';
            elementA.click();
          }
        },
        (err) => {
          alert('Failed to download file');
        }
      )
    } else {
      alert(this.translate.instant('userApplicationAcceptancePage.alert.noDoc'))
    }
  }
}