import { Component, OnDestroy, OnInit } from '@angular/core'

import { identity, Subscription } from 'rxjs'
import { ApplicationStatus } from '../../interfaces/applicationstatus'
import { OpenVacancy } from '../../interfaces/openvacancy'
import { OpenVacancyService } from '../../services/openvacancy.service'
import { formatDate } from '@angular/common'
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['../jobportal/open-vacancy.component.scss', './crew-job-portal.component.scss'],
  templateUrl: './application-status.component.html',
})
export class ApplicationStatusComponent implements OnInit {
    username: string
    activateStatus: string
    allStatus: any[] = []
    allVacancies: any[] = [] // Added by Hakim on 19 Jan 2021
    userEmail: string
  
    private _statusSubscription: Subscription
    private _vacanciesSubscription: Subscription

    constructor(
      private statusService: OpenVacancyService,
      private router: Router,
      private translate: TranslateService
    ) {}
  
    async ngOnInit(): Promise<void> {
      this.username = localStorage.getItem('user_name');
      this.userEmail = localStorage.getItem('user_email');
      this.activateStatus = localStorage.getItem("verify_status");

      if (this.activateStatus === "0") {
        alert(this.translate.instant('userApplicationStatusPage.alert.activateAccount'));
        this.router.navigate(['/pages/applicant_jobportal/change-password'])
      }

      await this._getVacancies() // Added by Hakim on 19 Jan 2021
      this._getStatus() 
    }
  
    ngOnDestroy(): void {
      if (this._statusSubscription) {
        this._statusSubscription.unsubscribe()
      }

      // Added by Hakim on 19 Jan 2021 - Start
      if (this._vacanciesSubscription) {
        this._vacanciesSubscription.unsubscribe()
      }
      // Added by Hakim on 19 Jan 2021 - End
    }
  
    navigatePage(id,psid,ApplyID) {
      //this.router.navigateByUrl('/pages/applicant_jobportal/crew_job_portal')
      this.router.navigate(['/pages/applicant_jobportal/crew_job_portal/'], { queryParams: { openvacancy: id, psid: psid, status: 2,LoginEmail:this.userEmail,ApplyID:ApplyID}})
    }

    navigatePageAcceptance(id,psid,ApplyID) {
      //this.router.navigateByUrl('/pages/applicant_jobportal/crew_job_portal')
      this.router.navigate(['/pages/applicant_jobportal/application-acceptance/'], { queryParams: { openvacancy: id, psid: psid, status: 2,LoginEmail:this.userEmail,ApplyID:ApplyID}})
    }

    private _getStatus() {
      this._statusSubscription = this.statusService.getAppliedOpenVacancy(this.userEmail).subscribe(
        (result: any[]) => {
          this._refreshVacancyData(result),
          console.log("result in here")
        },
        (err) => alert('Failed to load vacancies')
      )
    }

    private _getVacancies() {
      return new Promise((resolve, reject) => {
        this._vacanciesSubscription = this.statusService.getAllOpenVacancies().subscribe(
          (result: any[]) => {
            this.allVacancies = result
            return resolve(true)
          },
          (err) => { 
            alert('Failed to load vacancies')
            return resolve(false)
          }
        )
      })
    }

  
    private _refreshVacancyData(result: any[]) {

      let sortedStatus = result.sort((data1, data2) => {
        return new Date(data2.DtApplication).getTime() - new Date(data1.DtApplication).getTime();
      });

      this.allStatus = sortedStatus.map((status: ApplicationStatus) => {

        let dtSubmit = ''
        if (status.SubmitDt) {
          dtSubmit = formatDate(status.SubmitDt, 'dd/MM/yyyy h:mm a', 'en-MY')
        }
        
        let vacancy = this.allVacancies.filter(v => v.PSId == status.PositionID)

        if (status.AcceptFlag == 'Y') {
          status.AcceptFlag = 'Accepted';
        } else if (status.AcceptFlag == 'N') {
          status.AcceptFlag = 'Crew-Rejected';
          status.Status = 'Crew-Rejected';
        } else {
          status.AcceptFlag = '';
        }
        return {
          Id: status.Id,
          PositionID: status.PositionID,
          VacancyID: vacancy[0] ? vacancy[0].Id : '',
          Position: status.Position,
          DateUpdate: formatDate(status.DtApplication, 'dd/MM/yyyy h:mm a', 'en-MY'),
          DateSubmit: dtSubmit ? dtSubmit : 'N/A', // Added by Hakim on 19 Jan 2021
          Status: status.SubmitFlag === "N" ?  'Draft' : status.Status,
          SubmitFlag: status.SubmitFlag === "N" ?  'Draft' : '',
          AcceptFlag: status.AcceptFlag
        }

      
          // return {
          //   Id: status.Id,
          //   PositionID: status.PositionID,
          //   VacancyID: vacancy[0].Id ? vacancy[0].Id : '',
          //   Position: status.Position,
          //   DateUpdate: dtUpdate,
          //   DateSubmit: dtSubmit, // Added by Hakim on 19 Jan 2021
          //   Status: status.Status
          // }

      })
    }
  }