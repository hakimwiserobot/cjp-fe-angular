import { NgModule } from '@angular/core'
import { NbDateFnsDateModule } from '@nebular/date-fns'
import {
  NbCardModule,
  NbDatepickerModule,
  NbCheckboxModule,
  NbTooltipModule
} from '@nebular/theme'
import {
  SmartTableDatepickerComponent,
  SmartTableDatepickerRenderComponent,
} from '../../smart-table-datepicker/smart-table-datepicker.component'
import { Ng2SmartTableModule } from 'ng2-smart-table'
import { ThemeModule } from '../../@theme/theme.module'
import { CrewJobPortalComponent } from './crew-job-portal.component'
import { NbThemeModule, NbButtonModule, NbLayoutModule } from '@nebular/theme'
import { FormsModule } from '@angular/forms'
import { TranslateModule } from '@ngx-translate/core'

@NgModule({
  imports: [
    NbCardModule,
    ThemeModule,
    NbCheckboxModule,
    Ng2SmartTableModule,
    NbThemeModule,
    NbButtonModule,
    NbLayoutModule,
    NbTooltipModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDateFnsDateModule.forRoot({ format: 'dd.MM.yyyy' }),
    //NbDateFnsDateModule.forChild({ format: 'dd.MM.yyyy' }),
    TranslateModule
  ],
  declarations: [CrewJobPortalComponent]
})
export class CrewJobPortalModule {}
