import { Component, OnDestroy, OnInit } from '@angular/core'
import { DownloadButton } from '../../../shared/DownloadButton.component'

import { LocalDataSource } from 'ng2-smart-table'
import { Subscription } from 'rxjs'
import { ApplicantService } from '../../../services/applicant.service' 
import { VesselNameService } from '../../../services/vessel-name.service'
import { Position } from '../../../interfaces/position'
import { Applicant, ApplicantDocument } from '../../../interfaces/applicant'
import { formatDate } from '@angular/common'
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ngx-admin-update-applicant',
  templateUrl: './admin-update-applicant.component.html',
  styleUrls: ['./admin-update-applicant.component.scss']
})
export class AdminUpdateApplicantComponent implements OnInit, OnDestroy {

  fileExcelEmpId: File
  searchName: string = ''
  searchEmpId: string = ''
  searchVessel: string = ''
  searchDate: any
  searchDate2: any
  username: string
  userEmail: string
  activateStatus: string
  totalRecord: number = 0;
  recordNotFound = []

  exportData: any[] = []
  public source = new LocalDataSource()

  settings = {
    hideSubHeader: true, // hide the add new fields
    columns: {
      No: {
        title: 'No',
        filter: false,
        editable: false,
        addable: false,
      },
      Id: {
        title: 'Id',
        hide: true
      },
      // Email: {
      //   title: 'Email',
      //   filter: false,
      // },
      // Name: {
      //   title: 'Applicant Name',
      //   filter: false,
      // },
      IC: {
        title: 'IC',
        filter: false,
      },
      Passport: {
        title: 'Passport',
        filter: false,
      },
      EmpId: {
        title: 'Emp Id',
        filter: false,
      }
    },
    actions: {
      delete: false,
      add: false,
      edit: false,
    },
  }

  constructor(
    // private _subscription: Subscription,
    private _applicantService: ApplicantService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.username = localStorage.getItem('admin_user_name');
    this.userEmail = localStorage.getItem("admin_user_email");
    this.activateStatus = localStorage.getItem("admin_verify_status");
    if (this.activateStatus === "0" && this.username !== 'admin') {
      alert("Please activate your account first.")
      this.router.navigate(['/pages/settings/change-password'])
    }
  }

  ngOnDestroy(): void {
    // if (this._subscription) {
    //   this._subscription.unsubscribe()
    // }
  }

  formatDate(date: Date) {
    return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear()
  }

  filterDate(dateFrom: Date, dateTo: Date, date: Date) {
    return date >= dateFrom && date <= dateTo
  }

  handleFileInput(files: FileList) {
    var ext = files[0].name.match(/\.([^\.]+)$/)[1];
    if (ext == 'xlsx') {
      this.fileExcelEmpId = files[0];
    } else {
      alert("Invalid type. Please choose .xlsx file. Thank you.")
    }
  }

  // Service section
  async uploadExcel() {
    const subscription = await new Promise((resolve, reject) => {
      this._applicantService.uploadExcelEmpId(this.fileExcelEmpId).subscribe(
        (result: any) => {
          if (result.success) {
            this.totalRecord = result.totalRecord;
            this.refreshNotFoundData(result.recordNotFound);

            let inputFile = document.getElementById('file') as HTMLInputElement;
            inputFile.value = '';
            
            resolve(true)
          } else {
            resolve(false)
          }
        },
        (err) => {
          alert('Failed to upload file')
          reject(false)
        }
      )
    })

    if (subscription) {
      alert("Successfully upload excel");
      // location.reload();
    } else {
      alert("Failed to upload excel. Please try again later");
    }
  }

  downloadTemplate() {
    var link= document.createElement('a');
    var url = environment.templatePathPrefix + '/EmpidTemplate.xlsx';
    link.href = url;
    link.download = url.substr(url.lastIndexOf('/') + 1);
    link.click();
  }

  refreshNotFoundData(data) {
    this.recordNotFound = data.map((item, index) => {
      return {
        No: index + 1,
        IC:item.IC,
        Passport:item.Passport,
        EmpId:item.EmpId
      }
    })
  }

  private async _searchApplicantWithEmpId() {
    
  }

  // Alter service response section
  

  async onSearch(position: string = '') {
    
  }

}
