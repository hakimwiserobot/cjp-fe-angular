import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUpdateApplicantComponent } from './admin-update-applicant.component';

describe('AdminUpdateApplicantComponent', () => {
  let component: AdminUpdateApplicantComponent;
  let fixture: ComponentFixture<AdminUpdateApplicantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUpdateApplicantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUpdateApplicantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
