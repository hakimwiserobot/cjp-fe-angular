import { Component, ComponentFactoryResolver, Injectable, OnDestroy, OnInit } from '@angular/core'
import { VesselTimesheetService } from '../../services/vesseltimesheet.service'
import { PositionService } from '../../services/position.service'
import { LocalDataSource } from 'ng2-smart-table'
import { NbDateService } from '@nebular/theme';

import { Subscription } from 'rxjs'
import { VesselTimesheet } from '../../interfaces/vesseltimesheet'
import { Position } from '../../interfaces/position'
import { Vessel } from '../../interfaces/vessel'

import { Router } from '@angular/router'
import * as XLSX from 'xlsx';
import { formatDate, DatePipe } from '@angular/common'
import { VesselService } from '../../services/vessel.service'
import { DownloadService } from '../../services/download.service'

import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'ngx-vesseltimesheet',
  templateUrl: './vesseltimesheet.component.html',
})
export class VesselTimesheetComponent implements OnInit, OnDestroy {
  vesseltimesheets: any[] = []
  positions: any[] = []
  vessels: any[] = []
  selectedPosition = ''
  selectedVessel = ''
  exportData: any[] = []
  tableData: any[] = []
  exportSheetName: any[] = []
  name: string = ''
  middlename: string = ''
  lastname: string = ''
  status: string = ''
  username: string
  userEmail: string
  activateStatus: string
  url:string
  filePath: string | null

  startDate: Date = undefined;
  endDate: Date = undefined;
  minEndDate: Date = new Date();;
  maxEndDate: Date = this.dateService.addDay(this.minEndDate, 1);

  selectedMonth: number = 0
  selectedYear: number = 0
  months = [{ value:0, title:"January" }, { value:1, title:"February" }, { value:2, title:"March" }, { value:3, title:"April" }, { value:4, title:"May" }, { value:5, title:"June" }, { value:6, title:"July" }, { value:7, title:"August" }, { value:8, title:"September" }, { value:8, title:"October" }, { value:10, title:"November" }, { value:11, title:"December" }];
  _vesseltimesheetSubscription: Subscription
  _positionSubscription: Subscription
  _vesselSubscription: Subscription
  public source = new LocalDataSource()

  constructor(private vesselService: VesselService, private service: VesselTimesheetService, private positionService: PositionService, private downloadService: DownloadService, protected dateService: NbDateService<Date>, private router: Router) { 

  }

  ngOnInit(): void {
    this.selectedYear = new Date().getFullYear();
    this.username = localStorage.getItem('admin_user_name');
    this.userEmail = localStorage.getItem("admin_user_email");
    this.activateStatus = localStorage.getItem("admin_verify_status");
    if (this.activateStatus === "0" && this.username !== 'admin') {
      alert("Please activate your account first.")
      this.router.navigate(['/pages/settings/change-password'])
    }

    // this.getVesselTimesheet()
    this.getPositions()
    this.getVessels()
  }

  ngOnDestroy(): void {
    if (this._vesseltimesheetSubscription) {
      this._vesseltimesheetSubscription.unsubscribe()
    }
    if (this._positionSubscription) {
      this._positionSubscription.unsubscribe()
    }
  }

  getVesselTimesheet() {
    let dateFrom = ''
    let dateTo = ''
    if (this.startDate) {
      dateFrom = this.startDate.getDate() + "/" + (this.startDate.getMonth()+1) + "/" + this.startDate.getFullYear();
    }
    if (this.endDate) {
      dateTo = this.endDate.getDate() + "/" + (this.endDate.getMonth()+1) + "/" + this.endDate.getFullYear();
    }

    if (!this.selectedVessel) {
      this.selectedVessel = 'All';
    }

    if (!this.selectedPosition) {
      this.selectedPosition = 'All';
    }

    this._vesseltimesheetSubscription = this.service.getAllVesselTimesheets(this.name, this.selectedVessel, this.selectedPosition, dateFrom, dateTo).subscribe(
      async (result: any) => {
        this.vesseltimesheets = result
        await this._refreshData()

        const btnSearch = (<HTMLInputElement>document.getElementById('btnSearch'));
        btnSearch.innerText = "SEARCH";
        btnSearch.disabled = false;
      },
      (err) => { 
        alert('Failed to load vesseltimesheets')
      }
    )
  }

  getPositions() {
    this._positionSubscription = this.positionService.getAllPositions().subscribe(
      (result: any) => {
        this.positions = result
        this._refreshPositionData()
      },
      (err) => alert('Failed to load positions')
    )
  }

  getVessels() {
    this._vesselSubscription = this.vesselService.getAllVessels().subscribe(
      (result: any) => {
        this.vessels = result
        this._refreshVesselData()
      },
      (err) => alert('Failed to load vessels')
    )
  }

  onChangeStartDate() {
    this.updateEndDate()
  }

  updateEndDate() {
    this.minEndDate = this.dateService.addDay(this.startDate, 1);
    this.maxEndDate = this.dateService.addMonth(this.dateService.addDay(this.startDate, -1), 1);

    this.endDate = this.maxEndDate
  }

  settings = {
    actions: {
      position: 'right',
      add: false,
      edit: false,
      delete: false,
    },
    hideSubHeader: true,
    columns: {
      No: {
        title: 'No',
      },
      ApplyPosition: {
        title: 'Position',
        filter: false,
      },
      NameofVessel: {
        title: 'Vessel Name',
        filter: false,
      },
      Name: {
        title: 'Name',
        filter: false,
      },
      IC: {
        title: 'IC/Passport',
        filter: false,
      },
      ContractPeriodFrom: {
        title: 'Contract From',
        filter: false,
      },
      ContractPeriodTo: {
        title: 'Contract To',
        filter: false,
      },
      ApplyStatus: {
        title: 'Status',
        filter: false,
      },
      // action: {
      //   title: 'Action',
      //   type: 'html',
      //   filter: false,
      // },
    },
  }

  filterDate(dateFrom: Date, dateTo: Date, dateDataFrom: Date, dateDataTo: Date) {
    if (dateDataFrom >= dateFrom && dateDataTo <= dateTo) {
      return true;
    } else if (dateDataFrom < dateFrom && dateDataTo > dateTo) {
      return true;
    } else if (dateDataTo >= dateFrom && dateDataTo <= dateTo) {
      return true;
    } else if (dateDataFrom >= dateFrom && dateDataFrom <= dateTo) {
      return true;
    } else {
      return false;
    }
  }

  filterDateString(dateFrom: string, dateTo: string, dateDataFrom: string, dateDataTo: string) {
    if (dateDataTo != null && dateDataFrom != null) {
      if (dateDataTo.search(dateTo) >= 0 && dateDataFrom.search(dateFrom) >= 0) {
        return true
      }
    }
    
    return false;
  }

  // retrieve vessel name for searching
  retrieveVesselId(vessel: string) {
    let obj = this.vessels.find(o => o.VesselName === vessel);
    if (obj) {
      return obj.Id.toString();
    } else {
      return '';
    }
  }

  async onSearch(position: string = '', vessel: string = '') {

    var checkField = this.checkMandatory()

    if (checkField) {
    //   const dateTo = (<HTMLInputElement>document.getElementById('update_date_to')).value
    //   const dateFrom = (<HTMLInputElement>document.getElementById('update_date_from')).value
    //   let dts = dateTo.split(/[/]/)
    //   const dateToObject = new Date(parseInt(dts[2]), parseInt(dts[1]) - 1, parseInt(dts[0]) + 1)
    //   // const dateToObject = dts[2] + '-' + dts[1] + '-' + dts[0];
    //   dts = dateFrom.split(/[/]/)
    //   const dateFromObject = new Date(parseInt(dts[2]), parseInt(dts[1]) - 1, parseInt(dts[0]))
    //   // const dateFromObject = dts[2] + '-' + dts[1] + '-' + dts[0];

    //   var retval = []
    //   for (let i = 0; i < this.vesseltimesheets.length; i++) {
    //     let vesselNameId = ''; 
    //     if (this.vesseltimesheets[i].NameofVessel) {
    //       vesselNameId = this.retrieveVesselId(this.vesseltimesheets[i].NameofVessel);
    //     }

    //     const dateDataFromString = this.vesseltimesheets[i].ContractPeriodFrom
    //     const dateDataToString = this.vesseltimesheets[i].ContractPeriodTo
    //     // const t = dateString.split(/[- :]/)
    //     // const date = new Date(parseInt(t[0]), parseInt(t[1]) - 1, parseInt(t[2]))
    //     const dateDataFrom = new Date(dateDataFromString)
    //     const dateDataTo = new Date(dateDataToString)
    //     let filterCond = true

    //     if (dateFrom !== '' && dateTo !== '') {
    //       filterCond = this.filterDate(dateFromObject, dateToObject, dateDataFrom, dateDataTo)
    //       // filterCond = this.filterDateString(dateFromObject, dateToObject, dateDataFromString, dateDataToString);
    //     }
    //     else if (dateFrom !== '') { // No dateTo
    //       filterCond = this.filterDate(dateFromObject, new Date(2999, 1, 1), dateDataFrom, dateDataTo)
    //     }
    //     else if (dateTo !== '') {
    //       filterCond = this.filterDate(new Date(1900, 1, 1), dateToObject, dateDataFrom, dateDataTo)
    //     }

    //     let vesseltimesheetName = ''

    //     if (this.vesseltimesheets[i].Name != null && this.vesseltimesheets[i].Name != null ) {
    //       vesseltimesheetName = vesseltimesheetName + this.vesseltimesheets[i].Name
    //     }

    //     if (this.vesseltimesheets[i].MiddleName != null && this.vesseltimesheets[i].MiddleName != '') {
    //       vesseltimesheetName = vesseltimesheetName + ' ' + this.vesseltimesheets[i].MiddleName
    //     }

    //     if (this.vesseltimesheets[i].LastName != null && this.vesseltimesheets[i].LastName != '') {
    //       vesseltimesheetName = vesseltimesheetName + ' ' + this.vesseltimesheets[i].LastName
    //     }

    //     if (filterCond && (position === '19' || position === '' || (position !== '' && this.vesseltimesheets[i].ApplyPositionID == position)) 
    //       && (vessel === '0' || vessel === '' || (vessel !== '' && vesselNameId == vessel)) 
    //       && (this.name == '' || vesseltimesheetName.toLowerCase().indexOf(this.name.toLowerCase()) > -1)
    //       && (this.status == '' || this.vesseltimesheets[i].ApplyStatus == this.status)) {
    //       if (this.vesseltimesheets[i].IC == '888888888888') {
    //         console.log("Found again");
    //       }
    //       retval.push(this.vesseltimesheets[i])
    //     }
    //   }

    //   await this.source.load(
    //     retval.map((item: VesselTimesheet, index: number) => {
    //       var fullname = [item.Name, item.MiddleName, item.LastName].filter(Boolean).join(" ");
    //       return {
    //         No: index + 1,
    //         Id: item.Id,
    //         ApplyPosition: item.ApplyPosition,
    //         ApplyPositionID: item.ApplyPositionID,
    //         NameofVessel: item.NameofVessel,
    //         Name: fullname,
    //         IC: item.IC,
    //         ContractPeriodFrom: formatDate(item.ContractPeriodFrom, 'dd/MM/yyyy', 'en-MY'),
    //         ContractPeriodTo: formatDate(item.ContractPeriodTo, 'dd/MM/yyyy', 'en-MY'),
    //         ApplyStatus: item.ApplyStatus,
    //         //action: '<a href="pages/jobportal/view-vesseltimesheet/' + item.Id + '">View</a>',
    //       }
    //     })
    //   )
      
    //   // this.tableData = [];
    //   this.tableData = retval
    //   await this._setupExportData()

      const btnSearch = (<HTMLInputElement>document.getElementById('btnSearch'));
      btnSearch.innerText = "SEARCHING...";
      btnSearch.disabled = true;

      this.getVesselTimesheet();
    } else {
      alert("Start Date and End Date are mandatory fields!")
    }

    
  }

  checkMandatory() {
    const dateTo = (<HTMLInputElement>document.getElementById('update_date_to')).value
    const dateFrom = (<HTMLInputElement>document.getElementById('update_date_from')).value

    if (!dateTo || !dateFrom) {
      return false
    } else {
      return true
    }
  }

  async exportToExcel() {
    var checkField = this.checkMandatory()

    await this.onSearch(this.selectedPosition, this.selectedVessel)

    if (checkField) {
      
      let excelToGenerate = [];

      let passData: any = {};

      const btnExport = (<HTMLInputElement>document.getElementById('btnExport'));
      let rowInfo = [];
      let dateFrom: Date = undefined;
      let dateTo: Date = undefined;

      dateFrom = this.startDate;
      dateTo = this.endDate;
      rowInfo = this.tableData;

      let timesheetYear = Number(dateFrom.getFullYear());
      let timesheetMonth = Number(dateFrom.getMonth()) + 1;

      excelToGenerate.push({
        rowInfo: rowInfo,
        selectedMonth: timesheetYear + '-' + timesheetMonth + '-1',
        dateFrom: dateFrom.getDate() + '/' + (dateFrom.getMonth()+1) + '/' + dateFrom.getFullYear(),
        dateTo: dateTo.getDate() + '/' + (dateTo.getMonth()+1) + '/' + dateTo.getFullYear(),
        totalVessel: [...new Set(this.tableData.map(item => item.NameofVessel))]
      })

      btnExport.innerText = "EXPORTING...";
      btnExport.disabled = true;
      
      console.log(excelToGenerate);

      for (let i = 0; i < excelToGenerate.length; i++) {
        const subscription = this.service.getExport(excelToGenerate[i]).subscribe(
          async (result: any) => {
  
            if (result.value) {
  
              let filename = result.value
              this.downloadService.timesheet(filename).subscribe(
                (result: any) => {
                  if (result) {
                    var url = window.URL.createObjectURL(result);
                    var elementA = document.createElement("a");
                    elementA.href = url;
                    elementA.download = filename;
                    elementA.click();
                  }
                  btnExport.innerText = "EXPORT TO EXCEL";
                  btnExport.disabled = false;
                  subscription.unsubscribe();
                },
                (err) => {
                  alert('Failed to download file');
                  btnExport.innerText = "EXPORT TO EXCEL";
                  btnExport.disabled = false;
                  subscription.unsubscribe();
                }
              )
            }
          },
          (err) => {
            btnExport.innerText = "EXPORT TO EXCEL";
            btnExport.disabled = false;
            console.log(err);
            alert('Failed to export excel file. Please try again.')
          }
        )
      }
      
    } else {
      alert("Start Date and End Date are mandatory fields!")
    }
  }

  private async _refreshData() {
    await this.source.load(
      this.vesseltimesheets.map((item: VesselTimesheet, index: number) => {
        var fullname = [item.Name, item.MiddleName, item.LastName].filter(Boolean).join(" ");
        return {
          No: index + 1,
          Id: item.Id,
          ApplyPosition: item.ApplyPosition,
          ApplyPositionID: item.ApplyPositionID,
          NameofVessel: item.NameofVessel,
          Name: fullname,
          IC: item.IC,
          ContractPeriodFrom: formatDate(item.ContractPeriodFrom, 'dd/MM/yyyy h:mm a', 'en-MY'),
          ContractPeriodTo: formatDate(item.ContractPeriodTo, 'dd/MM/yyyy h:mm a', 'en-MY'),
          ApplyStatus: item.ApplyStatus,
          //action: '<a href="pages/jobportal/view-vesseltimesheet/' + item.Id + '">View</a>',
        }
      })
    )
    this.tableData = []
    this.tableData = this.vesseltimesheets
    await this._setupExportData()
  }

  private _refreshPositionData() {
    this.positions.map((item: Position) => {
      return {
        Id: item.Id,
        Position: item.Position
      }
    })
    this.positions.unshift({ Id: 19, Position: 'All' })
  }

  private _refreshVesselData() {
    this.vessels.map((item: Vessel) => {
      return {
        Id: item.Id,
        VesselName: item.VesselName
      }
    })
    this.vessels.unshift({ Id: 0, VesselName: 'All' })
  }

  private async _setupExportData() {
    // restructure the export setup data file
    this.source.getAll().then((data) => {
      var unique = [];
      var distinct = [];
      for( let i = 0; i < data.length; i++ ) {
        if( !unique[data[i].NameofVessel]){
          distinct.push(data[i].NameofVessel);
          unique[data[i].NameofVessel] = 1;
        }
      }

      this.exportSheetName = distinct;

      const result = Object.values(data.reduce((acc, x) => {
        acc[x.NameofVessel] = [...(acc[x.NameofVessel] || []), x ];
        return acc;
      }, {}));
      
      this.exportData = result
    });
  }
}