import { Component, Input, OnDestroy, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core'
import { Router } from '@angular/router'

import { CheckboxComponent } from '../../shared/CheckboxComponent.component'
import { UserIdConfigService } from '../../services/useridconfigure.service'
import { Subscription } from 'rxjs'
import { LocalDataSource } from 'ng2-smart-table'
import { UserModule } from '../../interfaces/usermodule'
import { environment } from '../../../environments/environment'

import { NbSortDirection, NbSortRequest, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';

interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}

interface FSEntry {
  name: string;
  parentModule: string;
  selected: boolean;
  type: string;
}

@Component({
  selector: 'ngx-useridconfigurecrud',
  templateUrl: './useridconfigurecrud.component.html',
  styleUrls: ['../jobportal/open-vacancy.component.scss'],
})
export class UserIdConfigureCrudComponent implements OnInit, OnDestroy {
  Id: any
  config: any[]
  modules: any[]
  mappedModules: any[]
  allManagers: any[] = []
  name: string
  userId: string
  userName: string
  firstName: string
  middleName: string
  lastName: string
  password: string // Added by Hakim on 3 Feb 2021
  selectedManager: string
  signaturePath: string
  signatureAdminPath: string
  signature: File
  signatureAdmin: File
  adminDetails: any = [] // Added by Hakim on 3 Feb 2021
  username: string
  userEmail: string
  activateStatus: string
  userModules: any[]

  isCreate: boolean = true
  public source = new LocalDataSource()
  private _subscription: Subscription
  private _moduleSubscription: Subscription
  private _managerSubscription: Subscription

  customColumn = 'name';
  defaultColumns = ['selected'];
  allColumns = [ this.customColumn, ...this.defaultColumns ];
  dataSourceTreeGrid: NbTreeGridDataSource<any>;
  dataTreeGrid:TreeNode<FSEntry>[] = [];

  @Input() rowData: any
  @Input() data: any

  @Output() save: EventEmitter<any> = new EventEmitter()

  constructor(
    private router: Router,
    private service: UserIdConfigService,
    private dataSourceBuilder: NbTreeGridDataSourceBuilder<any>
  ) {
    const navigation = this.router.getCurrentNavigation();
    this.Id = parseInt(navigation.extras as string);
    this.isCreate = isNaN(this.Id);
    // this.dataSource = this.dataSourceBuilder.create(this.dataTable);
  }

  ngOnInit(): void {
    this.username = localStorage.getItem('admin_user_name');
    this.userEmail = localStorage.getItem("admin_user_email");
    this.activateStatus = localStorage.getItem("admin_verify_status");
    if (this.activateStatus === "0" && this.username !== 'admin') {
      alert("Please activate your account first.")
      this.router.navigate(['/pages/settings/change-password'])
    }
    this.getAdminId()
    this._getData()
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe()
    }
    if (this._moduleSubscription) {
      this._moduleSubscription.unsubscribe()
    }
    if (this._managerSubscription) {
      this._managerSubscription.unsubscribe()
    }
  }

  // Added by Hakim on 3 Feb 2021 - Start
  getAdminId() {
    this.service
      .getAdminDetails(localStorage.getItem('adminUsername'))
      .subscribe(
        (result: any) => {
          if(result.length != 0){
            this.adminDetails = result[0]
          }
        },
        (err) => alert('Failed to load admin details')
      )
  }
  // Added by Hakim on 3 Feb 2021 - End

  private _getData() {
    if (!this.isCreate) {
      this._subscription = this.service.getUserIdConfigById(this.Id).subscribe(
        (result: any) => {
          this.config = result
          console.log("config: ", this.config)
          if (result && result.length > 0) {
            const cfg = this.config?.find(c => c.UserConfigureID === this.Id.toString())
            this.name = cfg.Name || ''
            this.userId = cfg.UserID || ''
            this.userName = cfg.UserName || ''
            this.firstName = cfg.FirstName || ''
            this.middleName = cfg.MiddleName || ''
            this.lastName = cfg.LastName || ''
            this.password = cfg.Password || '' // Added by Hakim on 3 Feb 2021
            this.signaturePath = cfg.Signature
            this.signatureAdminPath = cfg.SignatureAdmin
            this.selectedManager = cfg.ManagerId || ''
          }
          this._refreshData()
        },
        (err) => alert('Failed to load user ID config')
      )
    }

    this._moduleSubscription = this.service.getAllUserModules().subscribe(
      (result: any) => {
        this.modules = result
        this._refreshData()
      },
      (err) => alert('Failed to load modules')
    )

    this._managerSubscription = this.service.getAllManagers().subscribe(
      (result: any) => {
        this.allManagers = result.map(m => {
          return {
            ManagerId: m.ManagerId,
            ManagerName: m.LastName ? `${m.LastName}, ${m.FirstName}` : m.FirstName
          }
        })
      }
    )
  }

  settings = {
    hideSubHeader: true, // hide the add new fields
    columns: {
      No: {
        title: 'No',
        filter: false,
        editable: false,
        addable: false,
      },
      Chk: {
        title: 'Check',
        type: 'custom',
        filter: false,
        renderComponent: CheckboxComponent,
        valuePrepareFunction: (value) => {
          return { value, column: 'Chk' }
        }
      },
      Module: {
        title: 'Module',
        filter: false,
      },
      SubModule: {
        title: 'Sub Module',
        filter: false,
      }
    },
    actions: {
      delete: false,
      add: false,
      edit: false,
      position: 'right'
    },
  }

  signatureChange(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.signature = fileList[0];
    }
  }

  signatureAdminChange(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.signatureAdmin = fileList[0];
    }
  }


  viewSignature() {
    if (this.signaturePath && this.signaturePath.length > 0) {
      window.open(`${environment.documentPathPrefix}/${this.signaturePath}`, '_blank')
    }
  }

  removeSignature() {
    if (window.confirm('Are you sure you want to remove signature?')) {
      const subscription = this.service.deleteSignature(this.Id).subscribe(
        (result: any) => {
          console.log(result)
          if(result.success_code == 0) {
            alert('Successfully removed Signature')
            this.signaturePath = null
          }
        },
        (err) => alert('Failed to remove signature')
      )
    }
  }

  
  viewSignatureAdmin() {
    if (this.signatureAdminPath && this.signatureAdminPath.length > 0) {
      window.open(`${environment.documentPathPrefix}/${this.signatureAdminPath}`, '_blank')
    }
  }

  removeSignatureAdmin() {
    if (window.confirm('Are you sure you want to remove manager\'s signature?')) {
      const subscription = this.service.deleteSignatureAdmin(this.Id).subscribe(
        (result: any) => {
          console.log(result)
          if(result.success_code == 0) {
            alert('Successfully removed manager\'s Signature')
            this.signatureAdminPath = null
          }
        },
        (err) => alert('Failed to remove manager\'s signature')
      )
    }
  }

  async onSaveConfirm(event) {
    if (window.confirm('Are you sure you want to save?')) {
      const manager = this.allManagers.find(m => m.ManagerId === Number(this.selectedManager))
      console.log(manager , this.Id)
      const subscription = this.service.updateUserIdConfig(this.Id,
        JSON.stringify(
          // (await this.source.getAll())
          this.mappedModules
            .map((item: any) => {
              return {
                UserConfigureID: item.UserConfigureID,
                ModuleID: item.ModuleID,
                UserID: this.userId,
                Name: this.name,
                UserName: this.userName,
                FirstName: this.firstName,
                MiddleName: this.middleName,
                LastName: this.lastName,
                Chk: item.Chk,
                Module: item.Module,
                SubModule: item.SubModule,
                ManagerId: manager ? manager.ManagerId : null,
                ManagerName: manager ? manager.ManagerName : null,
                Password: this.password,
                UpdatedBy: this.adminDetails.Id,
              }
            })
        )
      )
        .subscribe(async (res: any) => {
          if (res == null) {
            alert('Failed to update user config')
          } else {
            let isError = false
            if (this.signature) {
              await new Promise((resolve, reject) => {
                const fileUploadSubscription = this.service.uploadSignature(this.Id, this.userId, this.signature).subscribe(
                  (sigRes: any) => {
                    if (!sigRes) {
                      alert('Failed to upload signature')
                      isError = true
                    } else {
                      // alert('Successfully updated user config!')
                      // this.router.navigate(['pages/admin_module/user_id_configure'])
                    }
                    fileUploadSubscription.unsubscribe()
                    resolve(true)
                  },
                  (err) => {
                    alert('Failed to upload signature. Make sure you select the correct file.')
                    reject()
                  }
                )
              })
            }

            if (this.signatureAdmin) {
              await new Promise((resolve, reject) => {
                const fileUploadSubscription = this.service.uploadSignatureAdmin(this.Id, this.userId, this.signatureAdmin).subscribe(
                  (sigRes: any) => {
                    if (!sigRes) {
                      alert('Failed to upload signature Manager')
                      isError = true
                    } else {
                      // alert('Successfully updated user config!')
                      // this.router.navigate(['pages/admin_module/user_id_configure'])
                    }
                    fileUploadSubscription.unsubscribe()
                    resolve(true)
                  },
                  (err) => {
                    alert('Failed to upload signature Manager. Make sure you select the correct file.')
                    reject()
                  }
                )
              })
            }

            if (!isError) {
              alert('Successfully updated user config!')
              this.router.navigate(['pages/admin_module/user_id_configure'])
            }
          }
          subscription.unsubscribe()
        },
          (err) => alert('Failed to update user config')
        )
    } else {
      event.confirm.reject()
    }
  }

  // Added by Hakim on 3 Feb 2021 - Start
  async onSavePasswordConfirm(event) {
    if (window.confirm('Are you sure you want to save password?')) {
      const manager = this.allManagers.find(m => m.ManagerId === Number(this.selectedManager))
      console.log(manager , this.Id)
      const subscription = this.service.updateUserIdConfigPassword(this.Id,
        JSON.stringify({ Password: this.password })
      )
        .subscribe((res: any) => {
          if (res == null) {
            alert('Failed to update user password')
          } else {
            alert('Successfully updated user password!')
          }
          subscription.unsubscribe()
        },
          (err) => alert('Failed to update user password')
        )
    } else {
      event.confirm.reject()
    }
  }
  // Added by Hakim on 3 Feb 2021 - End

  async onCreateConfirm(event) {
    if (window.confirm('Are you sure you want to create user config?')) {
      const manager = this.allManagers.find(m => m.ManagerId === Number(this.selectedManager))
      const subscription = this.service.addUserIdConfig(
        JSON.stringify(
          // (await this.source.getAll())
          this.mappedModules
            .map((item: any) => {
              return {
                ModuleID: item.ModuleID,
                UserID: this.userId,
                Name: this.name,
                UserName: this.userName,
                FirstName: this.firstName,
                MiddleName: this.middleName,
                LastName: this.lastName,
                Chk: item.Chk,
                Module: item.Module,
                SubModule: item.SubModule,
                ManagerId: manager ? manager.ManagerId : null,
                ManagerName: manager ? manager.ManagerName : null,
                Password: this.password, // Added by Hakim on 3 Feb 2021
                CreatedBy: this.adminDetails.Id // Added by hakim on 3 Feb 2021
              }
            })
        )
      )
        .subscribe(async (res: any) => {
          if (res && res.Id) {
            let isError = false
            if (this.signature) {
              await new Promise((resolve, reject) => {
                const fileUploadSubscription = this.service.uploadSignature(res.Id, this.userId, this.signature).subscribe(
                  (sigRes: any) => {
                    if (!sigRes) {
                      alert('Failed to upload signature')
                      isError = true
                    } else {
                      // alert('Successfully created user config')
                      // this.router.navigate(['pages/admin_module/user_id_configure'])
                    }
                    fileUploadSubscription.unsubscribe()
                    resolve(true)
                  },
                  (err) => {
                    isError = true
                    alert('Failed to upload signature. Make sure you select the correct file.')
                    reject()
                  }
                )
              })
            }

            if (this.signatureAdmin) {
              await new Promise((resolve, reject) => {
                const fileUploadSubscription = this.service.uploadSignatureAdmin(res.Id, this.userId, this.signatureAdmin).subscribe(
                  (sigRes: any) => {
                    if (!sigRes) {
                      alert('Failed to upload signature Manager')
                      isError = true
                    } else {
                      // alert('Successfully created user config')
                      // this.router.navigate(['pages/admin_module/user_id_configure'])
                    }
                    fileUploadSubscription.unsubscribe()
                    resolve(true)
                  },
                  (err) => {
                    isError = true
                    alert('Failed to upload signature Manager. Make sure you select the correct file.')
                    reject()
                  }
                )
              })
            }

            if (!isError) {
              alert('Successfully updated user config!')
            }

            this.router.navigate(['pages/admin_module/user_id_configure'])
          } else {
            alert('Failed to create user config')
          }
          subscription.unsubscribe()
        }, (err) => { 
          alert('Failed to create user config')
        })
    } else {
      event.confirm.reject()
    }
  }

  onCancel(event) {
    if (window.confirm('Are you sure you want to Cancel the Edit?')) {
      this.router.navigate(['pages/admin_module/user_id_configure'])
    } else {
      event.confirm.reject()
    }
  }

  private _refreshData() {
    if (!this.modules) {
      return
    }

    this.mappedModules = this.modules.map((item: UserModule, index: number) => {
      const cfg = this.config?.find(c => c.ModuleID === item.Id.toString())

      return {
        No: index + 1,
        UserConfigureID: this.Id,
        ModuleID: item.Id,
        UserID: this.userId,
        Name: this.name,
        UserName: this.userName,
        Chk: cfg ? cfg.Chk : 'N',
        Module: item.Module,
        SubModule: item.SubModule
      }
    })

    // this.source.load(this.mappedModules);
    this.mappingModulesDataToTreeGrid(this.mappedModules);
  }

  private mappingModulesDataToTreeGrid(modules) {

    this.dataTreeGrid = [
      {
        data: { name: 'Job Portal', selected:true, parentModule:'', type:'Module' },
        children: [],
      },
      {
        data: { name: 'Report', selected:false, parentModule:'', type:'Module' },
        children: [],
      },
      {
        data: { name: 'Admin Module', selected:false, parentModule:'', type:'Module' },
        children: [],
      }
    ]

    for(let i = 0; i < modules.length; i++) {
      let module = modules[i];
      if (module.Module == 'Job Portal') {
        this.dataTreeGrid[0].children.push({ data:{ name: module.SubModule, selected:module.Chk == 'Y' ? true : false, parentModule:module.Module, type:'SubModule' }});
      } else if (module.Module == 'Report') {
        this.dataTreeGrid[1].children.push({ data:{ name: module.SubModule, selected:module.Chk == 'Y' ? true : false, parentModule:module.Module, type:'SubModule' }});
      } else if (module.Module == 'Admin Module') {
        this.dataTreeGrid[2].children.push({ data:{ name: module.SubModule, selected:module.Chk == 'Y' ? true : false, parentModule:module.Module, type:'SubModule' }});
      }
    }

    this.dataSourceTreeGrid = this.dataSourceBuilder.create(this.dataTreeGrid);
  }

  async onToggleAccess(data) {
    console.log(data);

    if (data.type == "SubModule") {
      let moduleIndex = 0;
      if (data.parentModule == 'Job Portal') {
        moduleIndex = 0;
      } else if (data.parentModule == 'Report') {
        moduleIndex = 1;
      } else if (data.parentModule == 'Admin Module') {
        moduleIndex = 2;
      }

      let module = this.dataTreeGrid[moduleIndex].children.find(item => item.data.name == data.name);
      module.data.selected = !module.data.selected;

      let mappedModule = this.mappedModules.find(module => module.SubModule == data.name);
      mappedModule.Chk = module.data.selected ? 'Y' : 'N';
      console.log(this.mappedModules);
    }
  }
}

