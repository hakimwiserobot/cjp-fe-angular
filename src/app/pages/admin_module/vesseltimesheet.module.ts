import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { NbDateFnsDateModule } from '@nebular/date-fns'
import {
  NbCardModule,
  NbDatepickerModule,
  NbCheckboxModule,
} from '@nebular/theme'
import { Ng2SmartTableModule } from 'ng2-smart-table'
import { ThemeModule } from '../../@theme/theme.module'
import { NbThemeModule, NbButtonModule, NbLayoutModule } from '@nebular/theme'

import { VesselTimesheetComponent } from './vesseltimesheet.component'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule.forRoot(),
    NbDateFnsDateModule,
    Ng2SmartTableModule,
    ThemeModule,
    NbThemeModule,
    NbButtonModule,
    NbLayoutModule,
  ],
  declarations: [VesselTimesheetComponent],
})
export class VesselTimesheetModule {}
