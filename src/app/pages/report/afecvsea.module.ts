import { NgModule } from '@angular/core'
import { NbCardModule } from '@nebular/theme'

import { ThemeModule } from '../../@theme/theme.module'
import { AfeCvSeaComponent } from './afecvsea.component'

import { Ng2SmartTableModule } from 'ng2-smart-table'
import { NbThemeModule, NbButtonModule, NbLayoutModule } from '@nebular/theme'
import { FormsModule } from '@angular/forms'

import { PagingTableModule } from '../../shared/paging-table/paging-table.module';

@NgModule({
    declarations: [AfeCvSeaComponent],
    imports: [
        NbCardModule,
        ThemeModule,
        Ng2SmartTableModule,
        NbThemeModule,
        NbButtonModule,
        NbLayoutModule,
        FormsModule,
        PagingTableModule
    ]
})
export class AfeCvSeaModule {}
