import { Component, OnDestroy, OnInit } from '@angular/core'
import { DownloadButton } from '../../shared/DownloadButton.component'
import { PagingTableComponent } from '../../shared/paging-table/paging-table.component'
import { Router } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table'
import { Subscription } from 'rxjs'
import { ApplicantService } from '../../services/applicant.service'
import { PositionService } from '../../services/position.service'
import { GenerateDocService } from '../../services/generateDoc.service'
import { Position } from '../../interfaces/position'
import { Applicant, ApplicantDocument } from '../../interfaces/applicant'
import { formatDate } from '@angular/common'

@Component({
  selector: 'ngx-afecvsea',
  templateUrl: './afecvsea.component.html',
})
export class AfeCvSeaComponent implements OnInit, OnDestroy {
  allApplicants: any[] = []
  allPositions: any[] = []
  allDocuments: ApplicantDocument[] = []
  searchName: string = ''
  searchEmail: string = ''
  searchPosition: string = ''
  username: string
  userEmail: string
  activateStatus: string
  currentPage: number = 1;
  totalPages: number = 1;
  isLoading = false;

  private _applicantSubscription: Subscription
  private _positionSubscription: Subscription

  public source = new LocalDataSource();

  constructor(
    private applicantService: ApplicantService,
    private positionService: PositionService,
    private generateDocService: GenerateDocService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.username = localStorage.getItem('admin_user_name');
    this.userEmail = localStorage.getItem("admin_user_email");
    this.activateStatus = localStorage.getItem("admin_verify_status");
    if (this.activateStatus === "0" && this.username !== 'admin') {
      alert("Please activate your account first.")
      this.router.navigate(['/pages/settings/change-password'])
    }
    this._getApplicants()
    this._getPositions()

    this.getDataForPage = this.getDataForPage.bind(this);
  }

  ngOnDestroy(): void {
    if (this._applicantSubscription) {
      this._applicantSubscription.unsubscribe()
    }
    if (this._positionSubscription) {
      this._positionSubscription.unsubscribe()
    }
  }

  async getDataForPage(page: number) {
    console.log('Get data for page ' + page);
    this.currentPage = page;
    await this._getApplicants();
  }

  settings = {
    hideSubHeader: true, // hide the add new fields
    columns: {
      No: {
        title: 'No',
        filter: false,
        editable: false,
        addable: false,
      },
      Id: {
        title: 'Id',
        hide: true
      },
      Position: {
        title: 'Position',
        filter: false,
      },
      Email: {
        title: 'Email',
        filter: false,
      },
      Name: {
        title: 'Name',
        filter: false,
      },
      VesselName: {
        title: 'Vessel Name',
        filter: false,
      },
      DtApplication: {
        title: 'Profile',
        filter: false,
      },
      Status: {
        title: 'Status',
        filter: false,
      },
      Afe: {
        title: 'AFE',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          if (value) {
            return { label: 'Download AFE', filePath: value }
          } else {
            return { label: 'Generate AFE', filePath: value, type: 'generate', callback: () => {
              const data = row;
              this._positionSubscription = this.generateDocService.AFE(data.Email, data.Id, localStorage.getItem('adminUsername')).subscribe(
                (result: any[]) => {
                  alert('Successfully generate AFE document for ' + data.Email)
                  window.location.reload();
                },
                (err) => alert('Failed to load generate document')
              )
            }}
          }
        }
      },
      Cv: {
        title: 'CV',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          if (value) {
            return { label: 'Download CV', filePath: value }
          } else {
            return { label: 'Generate CV', filePath: value, type: 'generate', callback: () => {
              const data = row;
              this._positionSubscription = this.generateDocService.CV(data.Email, data.Id, localStorage.getItem('adminUsername')).subscribe(
                (result: any[]) => {
                  alert('Successfully generate CV document for ' + data.Email)
                  window.location.reload();
                },
                (err) => alert('Failed to load generate document')
              )
            }}
          }
        }
      },
      Sea: {
        title: 'SEA',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          const data = row;
          if (value) {
            value = value.replace('-result', ''); // To cater for old format
            value = value.replace('.pdf', '.docx');
            return { label: 'Download SEA', filePath: value, type: 'download' }
          } else {
            return { label: 'Generate SEA', filePath: value, type: 'generate', callback: () => {
              this._positionSubscription = this.generateDocService.SEA(data.Email, data.Id, localStorage.getItem('adminUsername')).subscribe(
                (result: any[]) => {
                  alert('Successfully generate SEA documents for ' + data.Email)
                  window.location.reload();
                },
                (err) => alert('Failed to load generate documents')
              )
            }}
          }
        }
      },
      SeaPDF: {
        title: 'SEA PDF',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          const data = row;
          if (value) {
            return { label: 'Download SEA', filePath: value, type: 'download' }
          } else {
            return { label: 'Generate SEA', filePath: value, type: 'generate', callback: () => {
              this._positionSubscription = this.generateDocService.SEA(data.Email, data.Id, localStorage.getItem('adminUsername')).subscribe(
                (result: any[]) => {
                  alert('Successfully generate SEA documents for ' + data.Email)
                  window.location.reload();
                },
                (err) => alert('Failed to generate documents')
              )
            }}
          }
        }
      }
      // SendEmail: {
      //   title: 'Send Email',
      //   type: 'custom',
      //   filter: false,
      //   renderComponent: DownloadButton,
      //   valuePrepareFunction: (value, row) => {
      //     if (value > 0) {
      //       return { label: 'Resend email', filePath: 'asd', type: 'download' }
      //     } else {
      //       return { label: 'Send email', filePath: 'asd', type: 'generate', callback: () => {
      //         const data = row;
      //         this._positionSubscription = this.generateDocService.SendEmail(data.Id).subscribe(
      //           (result) => {
      //             if (result['success']) {
      //               alert(result['message'])
      //             } else {
      //               alert(result['message'])
      //             }
      //           },
      //           (err) => alert('Failed to send email')
      //         )
      //       }}
      //     }
      //   }
      // },
    },
    actions: {
      delete: false,
      add: false,
      edit: false,
    },
  }

  private _getApplicants() {
    return new Promise((resolve, reject) => {
      this.applicantService.getAllApplicantApplies(this.currentPage, this.searchName, this.searchEmail, this.searchPosition).subscribe(
        (result: any) => {
          if (result.success) {
            let data = result.data;
            this.totalPages = result.totalPages;
            this._refreshApplicantData(data)
            resolve(true);
          } else {
            alert('Failed to load applicants');
            resolve(false);
          }
        },
        (err) => { alert('Failed to load applicants'); resolve(false); } 
      )
    })
  }

  private _getPositions() {
    this._positionSubscription = this.positionService.getAllPositions().subscribe(
      (result: any[]) => {
        this._refreshPositionData(result)
      },
      (err) => alert('Failed to load positions')
    )
  }

  private _refreshApplicantData(result: any[]) {
    // this.source.load(
    //   this.allApplicants = result.map((applicant: Applicant, i: number) => {
    //     let FullName = '';
    //     if (applicant.Name != null) FullName += applicant.Name
    //     if (applicant.MiddleName != null) FullName += ' ' + applicant.MiddleName
    //     if (applicant.LastName != null) FullName += ' ' + applicant.LastName

    //     if (applicant.ApplyStatus != "Terminated") {
    //       if (applicant.AcceptFlag == 'Y' && applicant.AcceptBy != null && applicant.AcceptBy != '') {
    //         applicant.ApplyStatus = 'Accepted';
    //       } else if (applicant.AcceptFlag == 'Y' && applicant.AcceptBy != null && applicant.AcceptBy != '') {
    //         applicant.ApplyStatus = 'Crew-Rejected';
    //       } 
    //     }

    //     return {
    //       No: i + 1,
    //       Id: applicant.Id,
    //       Position: applicant.ApplyPosition,
    //       Email: applicant.LoginEmail,
    //       Name: FullName,
    //       VesselName: applicant.NameofVessel || '',
    //       DtApplication: applicant.ApplyDtApplication
    //         ? formatDate(new Date(applicant.ApplyDtApplication), 'dd/MM/yyyy h:mm a', 'en-MY')
    //         : '',
    //       Status: applicant.ApplyStatus,
    //       Afe: applicant.LoginEmail + '/' + applicant.FileAFE,
    //       Cv: applicant.LoginEmail + '/' + applicant.FileCV,
    //       Sea: applicant.FileSEA ? applicant.LoginEmail + '/' + applicant.FileSEA.replace('_declare-result.pdf','.docx') : '',
    //       SeaPDF: applicant.LoginEmail + '/' + applicant.FileSEA,
    //       SendEmail: applicant.EmailSent
    //     }
    //   })
    // )

    this.allApplicants = result.map((applicant: Applicant, i: number) => {
      let FullName = '';
      if (applicant.Name != null) FullName += applicant.Name
      if (applicant.MiddleName != null) FullName += ' ' + applicant.MiddleName
      if (applicant.LastName != null) FullName += ' ' + applicant.LastName

      if (applicant.ApplyStatus != "Terminated") {
        if (applicant.AcceptFlag == 'Y' && applicant.AcceptBy != null && applicant.AcceptBy != '') {
          applicant.ApplyStatus = 'Accepted';
        } else if (applicant.AcceptFlag == 'Y' && applicant.AcceptBy != null && applicant.AcceptBy != '') {
          applicant.ApplyStatus = 'Crew-Rejected';
        } 
      }

      return {
        No: i + 1,
        Id: applicant.Id,
        Position: applicant.ApplyPosition,
        Email: applicant.LoginEmail,
        Name: FullName,
        VesselName: applicant.NameofVessel || '',
        DtApplication: applicant.ApplyDtApplication
          ? formatDate(new Date(applicant.ApplyDtApplication), 'dd/MM/yyyy h:mm a', 'en-MY')
          : '',
        Status: applicant.ApplyStatus,
        Afe: applicant.LoginEmail + '/' + applicant.FileAFE,
        Cv: applicant.LoginEmail + '/' + applicant.FileCV,
        Sea: applicant.FileSEA ? applicant.LoginEmail + '/' + applicant.FileSEA.replace('_declare-result.pdf','.docx') : '',
        SeaPDF: applicant.LoginEmail + '/' + applicant.FileSEA,
        SendEmail: applicant.EmailSent
      }
    })
  }

  private _refreshPositionData(result: any[]) {
    this.allPositions = result.map((position: Position) => {
      return {
        Id: position.Id,
        Position: position.Position
      }
    })
    this.allPositions.unshift({ Id: -1, Position: 'All' })
  }

  async onSearch(event) {
    event.srcElement.innerText = "Searching..."
    event.srcElement.disabled = true
    this.isLoading = true;
    
    this.currentPage = 1;
    await this._getApplicants();

    // clear the filter
    // this.source.setFilter([])

    // const filters = []

    // console.log("check search name")
    // console.log(this.searchName)

    // if (this.searchName && this.searchName.length > 0) {
    //   filters.push({
    //     field: 'Name',
    //     search: this.searchName,
    //   })
    // }

    // if (this.searchEmail && this.searchEmail.length > 0) {
    //   filters.push({
    //     field: 'Email',
    //     search: this.searchEmail,
    //   })
    // }

    // if (this.searchPosition > 0) {
    //   const found = this.allPositions.find(p => p.Id === Number(this.searchPosition))

    //   if (found) {
    //     filters.push({
    //       field: 'Position',
    //       search: found.Position,
    //     })
    //   }
    // }

    // this.source.setFilter(filters)

    event.srcElement.innerText = "Search"
    event.srcElement.disabled = false
    this.isLoading = false;
  }
}
