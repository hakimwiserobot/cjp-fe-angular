import { Component, OnDestroy, OnInit } from '@angular/core'
import { MatrixService } from '../../services/matrix.service'
import { MatrixTemplateService } from '../../services/matrix-template.service'
import { VesselNameService } from '../../services/vessel-name.service'
import { MatrixDataService } from '../../services/matrix-data.service'
import { ApplicantService } from '../../services/applicant.service'
import { RelationshipService } from '../../services/relationship.service'
import { Router } from '@angular/router';
import * as XLSX from 'xlsx'
import { Subscription } from 'rxjs'
import { ReligionService } from '../../services/religion.service'
import { Relationship } from '../../interfaces/relationship'

@Component({
  selector: 'ngx-matrix',
  templateUrl: './matrix.component.html',
})
export class MatrixComponent implements OnDestroy, OnInit {
  matrixTemplates = []
  vessels = []
  genders = []
  relationships = []
  newTableDetails: any
  vesselName = ""
  selectedTemplate: any
  selectedDate: any
  selectedDate2: any
  selectedFormat = 'Horizontal' // Default to Horizontal
  hasSearch = false // Used to denote whether first search has occurred
  formats = ['Horizontal', 'Vertical']
  tableHeader = []
  username: string
  userEmail: string
  activateStatus: string

  private _matrixSubscription: Subscription
  private _matrixTemplateSubscription: Subscription
  private _vesselNameSubscription: Subscription
  private _genderSubscription: Subscription

  constructor(
    private matrixService: MatrixService,
    private matrixTemplateService: MatrixTemplateService,
    private vesselNameService: VesselNameService,
    private matrixDataService: MatrixDataService,
    private applicantService: ApplicantService,
    private relationshipService: RelationshipService,
    private router: Router
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.username = localStorage.getItem('admin_user_name');
    this.userEmail = localStorage.getItem("admin_user_email");
    this.activateStatus = localStorage.getItem("admin_verify_status");
    if (this.activateStatus === "0" && this.username !== 'admin') {
      alert("Please activate your account first.")
      this.router.navigate(['/pages/settings/change-password'])
    }

    this.newTableDetails = {}
    this.newTableDetails.dict = []
    this.newTableDetails.horizontal = [] // Added by Hakim on 29 Jan 2021
    this.newTableDetails.arr = [] // Added by Hakim on 29 Jan 2021
    this.newTableDetails.arrVertical = []

    await this.getGender()
    await this.getRelationship()

    this._matrixSubscription = this.matrixService.getMatrix().subscribe(
      (result: any) => {
        this.matrixTemplates = []
        for (let i = 0; i < result.length; i++) {
          const matrixTemplate = {}
          matrixTemplate['matrixName'] = result[i].Matrix
          this.matrixTemplates.push(matrixTemplate)
        }
      },
      (err) => alert('Failed to load Matrix')
    )

    this._matrixTemplateSubscription = this.matrixTemplateService.getMatrixTemplate().subscribe(
      (result: any) => {
        // Result is an JSON array
        for (let i = 0; i < this.matrixTemplates.length; i++) {
          this.matrixTemplates[i].Item = []
          this.matrixTemplates[i].ItemDesc = []
          this.matrixTemplates[i].SeqNo = []
          for (let j = 0; j < result.length; j++) {
            if (result[j].Matrix === this.matrixTemplates[i].matrixName) {
              this.matrixTemplates[i].Item.push(result[j].Item)
              this.matrixTemplates[i].ItemDesc.push(result[j].ItemDesc)
              this.matrixTemplates[i].SeqNo.push(result[j].SeqNo[0])
            }
          }
        }
      },
      (err) => alert('Failed to load Matrix')
    )

    this._vesselNameSubscription = this.vesselNameService.getVesselName().subscribe(
      (result: any) => {
        this.vessels = [{ VesselCode:'All', VesselName:'All'}]
        this.vessels = this.vessels.concat(result),
        console.log("check vessels")
        console.log(this.vessels)
        
      },
      (err) => alert('Failed to load Vessel Name')
    )
  }

  ngOnDestroy(): void {
    if (this._matrixSubscription) {
      this._matrixSubscription.unsubscribe()
    }
    if (this._matrixTemplateSubscription) {
      this._matrixTemplateSubscription.unsubscribe()
    }
    if (this._vesselNameSubscription) {
      this._vesselNameSubscription.unsubscribe()
    }
  }

  getGender() {
    return new Promise((resolve, reject) => {
      this._genderSubscription = this.applicantService.getGender().subscribe(
        (result: any) => {
          this.genders = result
          return resolve(true)
        },
        (err) => { console.log(err); return resolve(false); }
      )
    })
  }

  getRelationship() {
    return new Promise((resolve, reject) => {
      this._genderSubscription = this.relationshipService.getAllRelationships().subscribe(
        (result: any) => {
          this.relationships = result
          return resolve(true)
        },
        (err) => { console.log(err); return resolve(false); }
      )
    })
  }

  exportToExcel() {

    if (this.selectedFormat == 'Horizontal') {
      let dataToExport = []
      dataToExport.push(this.tableHeader)
      dataToExport = dataToExport.concat(this.newTableDetails.arr)
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataToExport, {skipHeader: true});
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'matrix.xlsx');
    } else {
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.newTableDetails.arr2, {skipHeader: true});
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'matrix.xlsx');
    }
  }

  alert(event) {
    if (!this.selectedTemplate) {
      alert('Please select a matrix template')
      return
    }
    if (!this.vesselName) {
      this.vesselName = 'All'
    }
    if (!this.selectedDate || !this.selectedDate2) {
      alert('Please select a date range period')
      return
    }

    event.srcElement.innerText = "Searching..."
    event.srcElement.disabled = true

    this.tableHeader = []
    this.newTableDetails = {}
    this.newTableDetails.dict = []
    this.newTableDetails.horizontal = [] // Added by Hakim on 29 Jan 2021
    this.newTableDetails.arr = [] // Added by Hakim on 29 Jan 2021
    this.newTableDetails.arr2 = [] // Added by Hakim on 8 March 2021
    this.hasSearch = true
    let selectedDateRange = (this.selectedDate ? this.selectedDate : ' ')  + "/" + (this.selectedDate2 ? this.selectedDate2 : ' ')
    let selectedDateFormat1 = ''
    let selectedDateFormat2 = ''
    const subscription = this.matrixDataService.getMatrixData(selectedDateRange, this.vesselName).subscribe(
      (result: any) => {
        // if (result && result.length > 0) {
        for (let i = 0; i < result.length; i++) {

          // Only take the first result
          const matrixData = result[i]
          matrixData.Position = matrixData.ApplyPosition
          const selectedMatrix = this.matrixTemplates.find(t => t.matrixName === this.selectedTemplate)
        
          // Added by Hakim on 28 Jan 2021 - Start
          var itemKey = selectedMatrix.Item
          var itemHeader = selectedMatrix.ItemDesc
          if (selectedMatrix.Item != null && selectedMatrix.Item != undefined) {
            if (selectedDateFormat1 == '' && selectedDateFormat2 == '') {
              selectedDateFormat1 = selectedMatrix.Item.find(data => data == 'DateFormat1')
              selectedDateFormat2 = selectedMatrix.Item.find(data => data == 'DateFormat2')
            }

            if (i == 0) {

              if (itemKey.indexOf('DateFormat1') >= 0) {
                itemKey.splice(itemKey.indexOf('DateFormat1'), 1);
              } else if (itemKey.indexOf('DateFormat2') >= 0) {
                itemKey.splice(itemKey.indexOf('DateFormat2'), 1);
              }

              if (itemHeader.indexOf('Date Format dd/mm/yyyy') >= 0) {
                itemHeader.splice(itemHeader.indexOf('Date Format dd/mm/yyyy'), 1);
              } else if (itemHeader.indexOf('Date Format mm/dd/yyyy') >= 0) {
                itemHeader.splice(itemHeader.indexOf('Date Format mm/dd/yyyy'), 1);
              }

              this.tableHeader = itemHeader      
              Object.assign(this.newTableDetails.arr2, itemHeader)
            }
          }

          let dictHorizontal = {}
          let arrHorizontal = []
          // this.newTableDetails.arr2[i] = selectedMatrix.Item
          // this.newTableDetails.arr2[i].push('N/A')
          // Added by Hakim on 28 Jan 2021 - End

          for (let j = 0; j < selectedMatrix.Item.length; j++) {
            let keyDesc = selectedMatrix.ItemDesc[j]
            let key = selectedMatrix.Item[j]
            let value = matrixData[key]

            // Change date format
            if (value != null) {
              let dataDate = new Date(value)
              let isDateKey = keyDesc.toLowerCase().search('date')
              if (dataDate.getDate() != null && isDateKey >= 0 && !isNaN(dataDate.getDate())) {
                if (selectedDateFormat2 != null) {
                  value = (dataDate.getMonth()+1) + '/' + dataDate.getDate() + '/' + dataDate.getFullYear()
                } else {
                  value = dataDate.getDate() + '/' + (dataDate.getMonth()+1) + '/' + dataDate.getFullYear()
                }
              }
            }

            // Set gender
            if (key == "Gender" && this.genders.length > 0) {
              let gender = this.genders.filter((gender) => { return gender.Id == matrixData[key] })
              value = gender.length > 0 ? gender[0].Gender : ''
            }

            // Set relationship
            if (key == "BeneRelationship" && this.relationships.length > 0) {
              let relationship = this.relationships.filter((relationship) => { return relationship.Id == matrixData[key] })
              value = relationship.length > 0 ? relationship[0].Relationship : ''
            }

            this.newTableDetails.dict.push({
              "key": keyDesc,
              "value": value
            })

            arrHorizontal.push(value)
            dictHorizontal[key] = value

            if (typeof this.newTableDetails.arr2[j] == 'string') {
              let temp = this.newTableDetails.arr2[j]
              this.newTableDetails.arr2[j] = [temp]
            }

            let arrTemp = this.newTableDetails.arr2[j]
            arrTemp.push(value ? value : '')
            this.newTableDetails.arr2[j] = arrTemp
          }

          // for (let key in matrixData) {
          //   var index = selectedMatrix.Item.indexOf(key)
          //   if (index >= 0) {
          //     // Added by Hakim on 28 Jan 2021 - Start
          //     // Change date format
          //     if (matrixData[key] != null) {
          //       let dataNumber = Number(matrixData[key])
          //       let dataDate = new Date(matrixData[key])
          //       if (dataDate.getDate() != null && !dataNumber && !isNaN(dataDate.getDate())) {
          //         if (selectedDateFormat2 != null) {
          //           matrixData[key] = (dataDate.getMonth()+1) + '/' + dataDate.getDate() + '/' + dataDate.getFullYear()
          //         } else {
          //           matrixData[key] = dataDate.getDate() + '/' + (dataDate.getMonth()+1) + '/' + dataDate.getFullYear()
          //         }
          //       }
          //     }
          //     // Added by Hakim on 28 Jan 2021 - End

          //     // Added by Hakim on 3 March 2021 - Start
          //     // Set gender
          //     if (key == "Gender" && this.genders.length > 0) {
          //       let gender = this.genders.filter((gender) => { return gender.Id == matrixData[key] })
          //       matrixData[key] = gender.length > 0 ? gender[0].Gender : ''
          //     }
          //     // Added by Hakim on 3 March 2021 - End

          //     this.newTableDetails.dict.push({
          //       "key": selectedMatrix.ItemDesc[index],
          //       "value": matrixData[key]
          //     })

          //     arrHorizontal[index] = matrixData[key]
          //     // arrHorizontal.push(matrixData[key]) // Added by Hakim on 29 Jan 2021
          //     dictHorizontal[key] = matrixData[key] // Added by Hakim on 29 Jan 2021

          //     // Added by Hakim on 29 Jan 2021 - Start
          //     // if (i == 0) {
          //       // this.tableHeader.push(selectedMatrix.ItemDesc[index],)
          //     // }
          //     // Added by Hakim on 29 Jan 2021 - End
          //   }
          // }
          this.newTableDetails.arr.push(arrHorizontal) // Added by Hakim on 29 Jan 2021
          this.newTableDetails.horizontal.push(dictHorizontal) // Added by Hakim on 29 Jan 2021
        }

        event.srcElement.innerText = "Search"
        event.srcElement.disabled = false

        subscription.unsubscribe()
      },
      (err) => {
        event.srcElement.innerText = "Search"
        event.srcElement.disabled = false
        
        alert('Failed to query matrix data ' + err)
      }
    )
  }
}
