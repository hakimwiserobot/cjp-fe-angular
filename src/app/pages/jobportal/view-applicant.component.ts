import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core'
import { ApplicantService } from '../../services/applicant.service'
import { ApplicationService } from '../../services/application.service'
import { PositionService } from '../../services/position.service'
import { ImoNoService } from '../../services/imono.service'
import { VesselService } from '../../services/vessel.service'
import { PortOfRegistryService } from '../../services/portofregistry.service'
import { AllowanceService } from '../../services/allowance.service'
import { IssuingAuthorityService } from '../../services/issuingauthority.service'
import { UserIdConfigService } from '../../services/useridconfigure.service'
import { RelationshipService } from '../../services/relationship.service'
import { GenerateDocService } from '../../services/generateDoc.service'
import { BaseService } from '../../services/base.service'
import { Vessel } from '../../interfaces/vessel'
import { formatDate, DatePipe } from '@angular/common'

import { Injectable } from '@angular/core'
import { Subscription } from 'rxjs'
import {
  Applicant,
  ApplicantDocument,
  ApplicantStatus,
  ApplicantNextOfKin,
  ApplicantDropdownId,
  ApplicantTransferLog,
  Currency,
  ApplicantGeneralQuestion, // Added by Hakim on 2 Feb 2021
  ApplicantGeneralAnswer, // Added by Hakim on 2 Feb 2021
  ApplicantMedicalQuestion, // Added by Hakim 2 Feb 2021
  ApplicantMedicalAnswer // Added by Hakim 2 Feb 2021
} from '../../interfaces/applicant'
import { Position } from '../../interfaces/position'
import { ImoNo, VesselType } from '../../interfaces/imono'
import { PortOfRegistry } from '../../interfaces/portofregistry'
import { Allowance } from '../../interfaces/allowance'
import { Router, ActivatedRoute } from '@angular/router'
import { IssuingAuthority } from '../../interfaces/issuingauthority'
import { environment } from '../../../environments/environment'

import { MatDialog, MatDialogConfig } from '@angular/material/dialog'
import { SignatureComponent } from './signature/signature.component'

@Injectable({
  providedIn: 'root',
})
@Component({
  selector: 'ngx-view-applicant',
  templateUrl: './view-applicant.html',
  styleUrls: ['./view-applicant.scss'],
})
export class ViewApplicantComponent implements OnInit, OnDestroy {
  showReviseModal : boolean;
  ReviseDataDetail: any = []
  data: any = []
  applicant: any = []
  originalApplicantData: any = []
  applicantStatus: any = []
  positions: any = []
  imonos: any = []
  vessels: any = []
  portsOfRegistry: any = []
  currencies: any = []
  allowances: any = []
  genderlist: any = []
  educationlist: any = []
  relationshiplist: any = []
  positionid: string
  daily_rate: string
  standby_rate: string
  allowance_amount: string
  contract_period: string
  issuingAuthority: any = []
  standbyRates: any = []
  signature: string
  signatureAdmin: string
  reason: string
  adminDetails: any = []
  generalQuestion: any = [] // Added by Hakim on 2 Feb 2021
  generalAnswer: any = [] // Added by Hakim on 2 Feb 2021
  medicalReportQuestion: any = [] // Added by Hakim on 2 Feb 2021
  medicalReportAnswer: any = [] // Added by Hakim on 2 Feb 2021
  disableAllBtn:Boolean = false
  username: string
  userEmail: string
  activateStatus: string

  _subscription: Subscription

  constructor(
    private signatureService: UserIdConfigService,
    private relationshipService: RelationshipService,
    private vesselService: VesselService,
    private service: ApplicantService,
    private applicationService: ApplicationService,
    private positionService: PositionService,
    private imoNoService: ImoNoService,
    private portOfRegistryService: PortOfRegistryService,
    private activatedRoute: ActivatedRoute,
    private allowanceService: AllowanceService,
    private issuingAuthorityService: IssuingAuthorityService,
    private router: Router,
    private dialog: MatDialog,
    private baseService:BaseService,
    private generateDocService: GenerateDocService,
    private elementRef : ElementRef
  ) {
    const navigation = this.router.getCurrentNavigation()
    this.standbyRates = [
      { Id: 1, Rate: '0.5x daily rate' },
      { Id: 2, Rate: '1.0x daily rate' },
    ]
  }

  async ngOnInit(): Promise<void> {
    this.username = localStorage.getItem('admin_user_name');
    this.userEmail = localStorage.getItem("admin_user_email");
    this.activateStatus = localStorage.getItem("admin_verify_status");
    if (this.activateStatus === "0" && this.username !== 'admin') {
      alert("Please activate your account first.")
      this.router.navigate(['/pages/settings/change-password'])
    }

    this.medicalReportQuestion = []
    this.medicalReportAnswer = []
    this.applicant.next_of_kin = []
    this.applicant.transferLog = []
    this.applicant.applicant_dropdown = []
    this.applicant.applicant_documents = []
    this.applicant.SEAExp = []
    const applicationId = this.activatedRoute.snapshot.params.Id
    this.getAllowances()
    this.getCurrency()
    this.getGender()
    this.getEducation() // Added by Hakim on 2 Feb 2021
    await this.getRelationships()
    //this.getApplicantById(135)
    //this.getApplicantById(1274)
    this.getApplicantById(applicationId)
    this.getPositions()
    this.getImoNo()
    this.getVesselType()
    this.getApplicantStatus()
    this.getPortsOfRegistry()
    this.getIssuingAuthorities()
    this.getAdminId()
    this.getApplicantGeneralQuestion() // Added by Hakim on 2 Feb 2021
    this.getApplicantMedicalReportQuestion() // Added by Hakim on 2 Feb 2021
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe()
    }
  }

  getIssuingAuthorities() {
    this._subscription = this.issuingAuthorityService
      .getAllIssuingAuthorities()
      .subscribe(
        (result: any) => {
          this.issuingAuthority = result
          this._refreshIssuingAuthorityData()
        },
        (err) => alert('Failed to load issuing authorities')
      )
  }

  _refreshIssuingAuthorityData() {
    this.issuingAuthority.map((item: IssuingAuthority, index: number) => {
      return {
        No: index + 1,
        Id: item.Id,
        Name: item.Name,
        Description: item.Description,
      }
    })
  }

  getCurrency() {
    this.service.getCurrency().subscribe(
    (result: any) => {
      this.currencies = result
      this._refreshCurrencyData()
    },
      (err) => alert('Failed to load Currency')
    )
  }

  _refreshCurrencyData() {
    this.currencies.map((item: Currency) => {
      return {
        Id: item.Id,
        Currency: item.Currency
      }
    })
  }

  viewFile(filePath) {
    if (filePath && filePath.length > 0) {
      window.open(`${environment.documentPathPrefix}/` + filePath, '_blank')
    }
  }

  getApplicantById(Id) {
    this.service.getApplicantById(Id).subscribe(
      (result) => {
        this.data = result
        this.applicant = result

        this._refreshData()

        this.originalApplicantData = Object.assign([], this.applicant);

      },
      (err) => alert('Failed to load applicant')
    )
  }

  getApplicantDocument(Id, LoginEmail, PositionID) {
    this.applicant.applicant_documents = []
    //this.applicantLoginEmail = this.activatedRoute.snapshot.params.LoginEmail
    // route.snapshot.paramMap.get

    this.service
      .getApplicantDocument(
        '?Id=' + Id + '&LoginEmail=' + LoginEmail + '&PositionID=' + PositionID
      )
      .subscribe(
        (result) => {
          this.applicant.applicant_documents = result
          this._refreshApplicantDocument()
        },
        (err) => alert('Failed to load applicant')
      )
  }

  _refreshApplicantDocument() {
    this.applicant.applicant_documents.map((item: ApplicantDocument) => {
      return {
        DocumentID: item.DocumentID,
        Document: item.Document,
        Chk: item.Chk,
        DocNo: item.DocNo,
        DtExpiry: new Date(item.DtExpiry),
        DtIssue: new Date(item.DtIssue),
        Position: item.Position,
        ChkDocType: item.DocType,
        ChkDocFile: item.DocFile,
        ApplicantDocNo: item.ApplicantDocNo,
        ApplicantDocDtIssue: item.ApplicantDocDtIssue,
        ApplicantDocDtExpiry: item.ApplicantDocDtExpiry,
        ApplicantDocType: item.ApplicantDocType,
        ApplicantDocFileName: item.ApplicantDocFileName,
        ApplicantDocFile: item.ApplicantDocFile,
        FilePath: item.FilePath,
        FileNamePath: item.FileNamePath,
        FileText: item.FileText,
      }
    })
    //this.applicant.applicant_documents.DocDtIssue=new Date(this.applicant.applicant_documents.DocDtIssue);
    //this.applicant.applicant_documents.DocDtExpiry=new Date(this.applicant.applicant_documents.DocDtExpiry);
  }

  getPositions() {
    this.positionService.getAllPositions().subscribe(
      (result) => {
        this.positions = result
        this._refreshPositionData()
      },
      (err) => { console.log(err); alert('Failed to load positions') }
    )
  }

  getApplicantStatus() {
    this.service.getApplicantStatus().subscribe(
      (result: any) => {
        if (result != null) {
          result = result.filter((item) => item.ApplicantStatus != 'Accepted' && item.ApplicantStatus != 'Crew-Rejected');
        }
        this.applicantStatus = result
        this._refreshApplicantStatusData()
      },
      (err) => alert('Failed to load Applicant Status')
    )
  }

  _refreshApplicantStatusData() {
    this.applicantStatus.map((item: ApplicantStatus) => {
      return {
        Id: item.Id,
        IMONo: item.ApplicantStatus,
      }
    })
  }

  getImoNo() {
    this.imoNoService.getAllImoNos().subscribe(
      (result: any) => {
        this.imonos = result
        this._refreshImoNoData()
      },
      (err) => alert('Failed to load IMO No')
    )
  }

  _refreshImoNoData() {
    this.imonos.map((item: ImoNo) => {
      return {
        Id: item.Id,
        IMONo: item.IMONo,
        VesselName: item.VesselName,
      }
    })
  }

  getApplicantDropdown(Id) {
    this.applicant.applicant_dropdown = []
    this.service.getApplicantDropdown(Id).subscribe(
      (result) => {
        this.applicant.applicant_dropdown = result[0]
        this._refreshApplicantDropdownData()
      },
      (err) => alert('Failed to load applicant dropdown ids')
    )
  }

  _refreshApplicantDropdownData() {}

  getPortsOfRegistry() {
    this.portOfRegistryService.getAllPortsOfRegistry().subscribe(
      (result) => {
        this.portsOfRegistry = result
        this._refreshPortOfRegistryData()
      },
      (err) => alert('Failed to load ports of registry')
    )
  }

  _refreshPortOfRegistryData() {
    this.portsOfRegistry.map((item: PortOfRegistry, index: number) => {
      return {
        No: index + 1,
        Id: item.Id,
        PortOfRegistry: item.PortOfRegistry,
      }
    })
  }

  // getVesselType() {
  //   this.imoNoService.getAllVessels().subscribe(
  //     (result: any) => {
  //       this.vessels = result
  //       this._refreshVesselData()
  //     },
  //     (err) => alert('Failed to load Vessel Type')
  //   )
  // }

  getVesselType() {
    this.vesselService.getAllVessels().subscribe(
      (result: any) => {
        this.vessels = result
        this._refreshVesselData()
      },
      (err) => alert('Failed to load vessels')
    )
  }

  _refreshVesselData() {
    this.vessels.map((item: Vessel, index: number) => {
      return {
        No: index + 1,
        Id: item.Id,
        VesselType: item.VesselType,
        VesselName: item.VesselName
      }
    })
  }

  getApplicantNextOfKin(LoginEmail) {
    this.applicant.next_of_kin = []
    this.service.getApplicantNextOfKin(LoginEmail).subscribe(
      (result: any) => {
        this.applicant.next_of_kin = result
        this._refreshNextOfKinData()
      },
      (err) => alert('Failed to load Next Of Kin')
    )
  }

  _refreshNextOfKinData() {
    this.applicant.next_of_kin.map((item: ApplicantNextOfKin) => {
      //let relationshipObj = this.relationshiplist.find(i => i.value == item.NOKRelationship)
      item.NOKGender = this.mapGenderToName(item.NOKGender)
      item.NOKRelationship = this.mapRelationshipToName(item.NOKRelationship)
      return {
        // Id: item.Id,
        // ApplyID: item.ApplyID,
        // UserID: item.UserID,
        // NOKName: item.NOKName,
        NOKRelationship: item.NOKRelationship,
        // NOKOccupaction: item.NOKOccupaction,
        // NOKAge: item.NOKAge,
        // NOKContactNumber: item.NOKContactNumber,
        // NOKDOB: item.NOKDOB,
        // Age: item.NOKAge,
        Id: item.Id,
        ApplyID: item.ApplyID,
        UserID: item.UserID,
        // Added by Hakim on 12 Jan 2021 - Start
        // Update for changes no. 4 in Application For Employment (0106 NC Comment_11012021.docx)
        NOKName: item.NOKName,
        NOKMiddlename: item.NOKMiddleName,
        NOKLastname: item.NOKLastName,
        // Added by Hakim on 12 Jan 2021 - End
        //NOKRelationship: relationshipObj != null ? relationshipObj.title : "", // Update by Hakim on 14 Jan 2021
        NOKOccupaction: item.NOKOccupaction,
        //NOKGender: item.NOKGender == "1" ? "Male" : "Female", // Update by Hakim on 14 Jan 2021
        NOKGender: item.NOKGender,
        NOKAge: item.NOKAge,
        NOKContactNumber: item.NOKContactNumber,
        NOKDOB: item.NOKDOB,
        // Updated by Hakim on 12 Jan 2021 - Start
        // Update for changes no. 4 in Application For Employment (0106 NC Comment_11012021.docx)
        NOKHandicap: item.NOKHandicap,
        NOKEmployment: item.NOKEmployment,
        // Added by Hakim on 12 Jan 2021 - End  
        No: item.SeqNo
      }
    })
  }

  // Added by Hakim on 2 Feb 2021 - Start
  getApplicantGeneralQuestion() {
    this.service.getApplicantGeneralQuestion().subscribe(
    (result: any) => {
      this.generalQuestion = result
      this._refreshApplicantGeneralQuestionData()
    },
      (err) => alert('Failed to load Applicant Status')
    )
  }

  _refreshApplicantGeneralQuestionData() {
    this.generalQuestion.map((item: ApplicantGeneralQuestion) => {
      return {
        Id: item.Id,
        Type: item.Type,
        Question: item.Question,
        YesNo: item.YesNo,
        Rating: item.Rating,
        TxtBox: item.TxtBox,
        FileNeeded: item.FileNeeded,
        PositionRelated: item.PositionRelated
      }
    })
  }

  getApplicantGeneralAnswer(ApplyID: string) {
    this.service.getApplicantGeneralAnswerById(ApplyID).subscribe(
    (result: any) => {
      if(result != null && result.length > 0)
        this.generalAnswer = result
        this._refreshApplicantGeneralAnswerData()
    },
      (err) => alert('Failed to load question')
    )
  }

  _refreshApplicantGeneralAnswerData() {
    this.generalAnswer = this.generalAnswer.map((item: ApplicantGeneralAnswer) => {
      return {
        Id: item.Id,
        Type: item.Type,
        ApplyID: item.ApplyID,
        LoginEmail: item.LoginEmail,
        QuestionId: item.QuestionId,
        Answer: item.Answer == 'Y' ? 'Yes' : item.Answer == 'N' ? 'No' : item.Answer,
        Description: item.Description,
        FilePath: item.FilePath
      }
    })
  }

  getApplicantMedicalReportQuestion() {
    this.service.getApplicantMedicalReportQuestion().subscribe(
    (result: any) => {
      this.medicalReportQuestion = result
      this._refreshApplicantMedicalReportQuestionData()
    },
      (err) => alert('Failed to load Applicant Status')
    )
  }

  _refreshApplicantMedicalReportQuestionData() {
    this.medicalReportQuestion.map((item: ApplicantMedicalQuestion) => {
      return {
        Id: item.Id,
        Type: item.Type,
        Question: item.Question,
        YesNo: item.YesNo,
        Rating: item.Rating,
        TxtBox: item.TxtBox,
        FileNeeded: item.FileNeeded,
        PositionRelated: item.PositionRelated,
        CheckupDt: item.CheckupDt,
        ExpiryDt: item.ExpiryDt
      }
    })
  }

  getApplicantMedicalReportAnswer(ApplyID: string) {
    this.service.getApplicantMedicalReportAnswerById(ApplyID).subscribe(
    (result: any) => {
      if(result != null && result.length > 0)
        this.medicalReportAnswer = result
      this._refreshApplicantMedicalReportAnswerData()
    },
      (err) => alert('Failed to load question')
    )
  }

  _refreshApplicantMedicalReportAnswerData() {
    this.medicalReportAnswer = this.medicalReportAnswer.map((item: ApplicantMedicalAnswer) => {
      return {
        Id: item.Id,
        Type: item.Type,
        ApplyID: item.ApplyID,
        LoginEmail: item.LoginEmail,
        QuestionId: item.QuestionId,
        Answer: item.Answer == 'Y' ? 'Yes' : 'No',
        Description: item.Description,
        FilePath: item.FilePath,
        AnsCheckupDt: item.AnsCheckupDt != null ? new Date(item.AnsCheckupDt).getDate() + '/' + (new Date(item.AnsCheckupDt).getMonth()+1) + '/' + new Date(item.AnsCheckupDt).getFullYear() : '',
        AnsExpiryDt: item.AnsExpiryDt != null ? new Date(item.AnsExpiryDt).getDate() + '/' + (new Date(item.AnsExpiryDt).getMonth()+1) + '/' + new Date(item.AnsExpiryDt).getFullYear() : ''
      }
    })
  }
  // Added by Hakim on 2 Feb 2021 - End

  mapStandByRateToId(id) {
    for (var i =0; i < this.standbyRates.length; i++) {
      var item = this.standbyRates[i]
      if (item.Id == id) {
        //this.applicant.CurrencyID = item.Id;
        return item.Rate
      }
    }
  }

  getGender() {
    this._subscription = this.service.getGender().subscribe(
      (result: any) => {
        this.genderlist = result
        //this._refreshCountryData()
      },
      (err) => alert('Failed to load Gender')
    )
  }

  // Added by Hakim on 2 Feb 2021 - Start
  getEducation() {
    this._subscription = this.service.getEducation().subscribe(
      (result: any) => {
        this.educationlist = result
        //this._refreshCountryData()
      },
      (err) => alert('Failed to load Education')
    )
  }
  // Added by Hakim on 2 Feb 2021 - End

  getRelationships() {
    return new Promise((resolve, reject) => {
      this._subscription = this.relationshipService.getAllRelationships().subscribe(
        (result: any) => {
          this.relationshiplist = result
          return resolve(true)
        },
        (err) => { 
          alert('Failed to load relationships') 
          return resolve(false)
        }
      )
    })
  }

  mapRelationshipToName(id) {
    for (var i =0; i < this.relationshiplist.length; i++) {
      var item = this.relationshiplist[i]
      if (item.Id == id) {
        //this.applicant.CurrencyID = item.Id;
        return item.Relationship
      }
    }
  }

  mapGenderToName(id) {
    for (var i =0; i < this.genderlist.length; i++) {
      var item = this.genderlist[i]
      if (item.Id == id) {
        //this.applicant.CurrencyID = item.Id;
        return item.Gender
      }
    }
  }

  // Added by Hakim on 2 Feb 2021 - Start
  mapEducationToName(id) {
    for (var i =0; i < this.educationlist.length; i++) {
      var item = this.educationlist[i]
      if (item.Id == id) {
        return item.Education
      }
    }
  }
  // Added by Hakim on 2 Feb 2021 - Start

  mapCurrencyToId(name) {
    for (var i =0; i < this.currencies.length; i++) {
      var item = this.currencies[i]
      if (item.Currency == name) {
        //this.applicant.CurrencyID = item.Id;
        return item.Id
      }
    }
  }

  mapCurrencyToName(id) {
    for (var i =0; i < this.currencies.length; i++) {
      var item = this.currencies[i]
      if (item.Id == id) {
        //this.applicant.CurrencyID = item.Id;
        return item.Currency
      }
    }
  }

  mapStandByRateToName(id) {
    for (var i =0; i < this.standbyRates.length; i++) {
      var item = this.standbyRates[i]
      if (item.Id == id) {
        //this.applicant.CurrencyID = item.Id;
        return item.Rate
      }
    }
  }

  _refreshData() {
    this.applicant.CurrencyID = this.mapCurrencyToId(this.applicant.Currency) 
    this.applicant.GenderName = this.mapGenderToName(this.applicant.Gender)
    this.applicant.Education = this.mapEducationToName(this.applicant.Education) // Added by Hakim on 2 Feb 2021
    this.applicant.StandbyRateName = this.mapStandByRateToName(this.applicant.StandbyRate)
    this.applicant.ReviseStatus = 0;
    this.applicant.Reason= '';
    this.applicant.EmergencyContactRelationship = this.mapRelationshipToName(this.applicant.EmergencyContactRelationship)
    this.getApplicantNextOfKin(this.applicant.LoginEmail)
    this.getApplicantSeaExperience(this.applicant.LoginEmail)
    this.getApplicantDocument(
      this.applicant.Id,
      this.applicant.LoginEmail,
      this.applicant.ApplyPositionID
    )
    this.getApplicantDropdown(this.applicant.Id)
    this.getApplicantGeneralAnswer(this.applicant.Id)
    this.getApplicantMedicalReportAnswer(this.applicant.Id)
    this.getTransferLog(this.applicant.Id)
    if (this.applicant.ApplyStatus == "Offered" && this.applicant.AcceptFlag == 'Y' && this.applicant.AcceptBy != null) {
      this.applicant.ApplyStatus = 'Accepted';
      this.applicant.ApplyStatus2 = 'Accepted';
    }
  }

  getTransferLog(id) {
    this.applicant.transferLog = []
    this.service.getApplicantTransferLog(id).subscribe(
      (result: any) => {
        this.applicant.transferLog = result
        this._refreshTransferLogData()
      },
      (err) => alert('Failed to load Revise Log')
    )
  }

  _refreshTransferLogData() {

    this.applicant.transferLog.map((item: ApplicantTransferLog) => {

      item.StandbyRate = this.mapStandByRateToName(item.StandbyRate)

      return {
        Id: item.Id,
        ApplicantApplyID: item.ApplicantApplyID,
        OfferPosition: item.OfferPosition,
        DailyRate: item.DailyRate,
        StandbyRate: item.StandbyRate,
        StandbyAllowance: item.StandbyAllowance,
        Allowance: item.Allowance,
        AllowanceRemarks: item.AllowanceRemarks,
        TypesofAllowance: item.TypesofAllowance,
        RepatriationHomePort: item.RepatriationHomePort,
        ContractPeriodFromInMth: item.ContractPeriodFromInMth,
        ContractPeriodFrom: formatDate(item.ContractPeriodFrom, 'dd/MM/yyyy h:mm a', 'en-MY'),
        ContractPeriodTo: formatDate(item.ContractPeriodTo, 'dd/MM/yyyy h:mm a', 'en-MY'),
        NameofVessel: item.NameofVessel,
        IMONo: item.IMONo,
        PortofRegistry: item.PortofRegistry,
        Currency: item.Currency,
        Salary: item.Salary,
        SalaryRemarks: item.SalaryRemarks,
        OtherAllowance: item.OtherAllowance,
        AdminID: item.AdminID,
        AdminName: item.AdminName,
        OldStatus: item.OldStatus,
        Status: item.Status,
        Reason: item.Reason,
        DtCreated: formatDate(item.DtCreated, 'dd/MM/yyyy h:mm a', 'en-MY'),
        No: item.SeqNo
      }
    })
  }

  _refreshPositionData() {
    this.positions.map((item: Position) => {
      return {
        Id: item.Id,
        Position: item.Position,
      }
    })
  }

  viewRevise(data) {
    this.ReviseDataDetail = [];
    this.ReviseDataDetail.push(data)

    this.showReviseModal = true;
  }

  hideReviseModal()
  {
    this.ReviseDataDetail = [];
    this.showReviseModal = false;
  }

  // Added by Hakim on 4 Feb 2021 - Start
  getApplicantSeaExperience(LoginEmail) {
    this.applicant.SEAExp = []
    this.service.getApplicantSeaExperience(LoginEmail).subscribe(
      (result: any) => {
        this.applicant.SEAExp = result
      },
        (err) => alert('Failed to load certification')
    )
  }
  // Added by Hakim on 4 Feb 2021 - End

  getAllowances() {
    this._subscription = this.allowanceService.getAllAllowances().subscribe(
      (result: any) => {
        this.allowances = result
        this._refreshAllowanceData()
      },
      (err) => alert('Failed to load allowances')
    )
  }

  _refreshAllowanceData() {
    this.allowances.map((item: Allowance, index: number) => {
      return {
        No: index + 1,
        Id: item.Id,
        Allowance: item.Allowance,
      }
    })
  }

  vesselIdOnChange(value) {
    let selectedVesselData = this.vessels.find(data => data.Id == this.applicant.applicant_dropdown.VesselId)
    let selectedIMONoData = this.imonos.find(data => data.VesselName == selectedVesselData.VesselName)
    this.applicant.applicant_dropdown.ImonoId = selectedIMONoData.Id
  }

  // Added by Hakim on 26 Jan 2021 - Start
  contractPeriodFromOnChange(value) {

    this.applicant.ContractPeriodFrom = value
    if (this.applicant.ContractPeriodFromInMth != null && this.applicant.ContractPeriodFrom != null) {
      let period = parseInt(this.applicant.ContractPeriodFromInMth);
      let startDate = new Date(this.applicant.ContractPeriodFrom)
      let endDate = startDate.setMonth(startDate.getMonth()+period)
      endDate = new Date(endDate).setDate(new Date(endDate).getDate()-1)
      this.applicant.ContractPeriodTo = endDate
    }
  }
  // Added by Hakim on 26 Jan 2021 - End

  // Added by Hakim on 27 Jan 2021 - Start
  contractPeriodToOnChange(value) {
    this.applicant.ContractPeriodTo = value
    if (this.applicant.ContractPeriodFrom != null) {
      let startDate = new Date(this.applicant.ContractPeriodFrom)
      let endDate = new Date(this.applicant.ContractPeriodTo)

      let diffYears = endDate.getFullYear()-startDate.getFullYear();
      let diffMonths = endDate.getMonth()-startDate.getMonth();
      let diffDays = endDate.getDate()-startDate.getDate();

      let months = (diffYears*12 + diffMonths);
      let numOfmonth = months.toString();
      if(diffDays>0) {
          numOfmonth += '.'+diffDays;
      } else if(diffDays<0) {
          months--;
          numOfmonth += '.'+(new Date(endDate.getFullYear(),endDate.getMonth(),0).getDate()+diffDays);
      }

      this.applicant.ContractPeriodFromInMth = numOfmonth
    }
  }

  contractPeriodFromInMthOnChange(value) {

    this.applicant.ContractPeriodFromInMth = Math.round(value) // Update by Hakim on 1 Feb 2021
    //console.log(parseInt(value))
    if (this.applicant.ContractPeriodFrom != null && this.applicant.ContractPeriodTo != null) {
      let period = parseInt(this.applicant.ContractPeriodFromInMth);
      let startDate = new Date(this.applicant.ContractPeriodFrom)
      let endDate = startDate.setMonth(startDate.getMonth()+period)
      endDate = new Date(endDate).setDate(new Date(endDate).getDate()-1)
      this.applicant.ContractPeriodTo = endDate
    }
  }
  // Added by Hakim on 27 Jan 2021 - End

  currencyOnChange(value) {
    this.applicant.Currency = this.mapCurrencyToName(value)
  }

  onCancel(event) {
    if (this.applicant.ApplyStatus == 'Offered') {
      this.router.navigate(['pages/jobportal/applicant'])
    } else {
      if (window.confirm('Do you really want to Cancel? Please update to save the changes.')) {
        this.router.navigate(['pages/jobportal/applicant'])
      } else {
        event.confirm.reject()
      }
    }
  }

  onCancelRevise(event) {
    this.applicant.ReviseStatus = 0;
  }

  onSaveSanitize() {
    // sanitize all data, convert id back to number
  }

  mapAllowanceIdToString(AllowanceIdToMap) {
    for (var i = 0; i < this.allowances.length; i++) {
      var item = this.allowances[i]
      if (item.Id == AllowanceIdToMap) {
        return item.Allowance
      }
    }
  }

  mapImonoIdToString(ImonoIdToMap) {
    for (var i = 0; i < this.imonos.length; i++) {
      var item = this.imonos[i]
      if (item.Id == ImonoIdToMap) {
        return item.IMONo
      }
    }
  }

  mapVesselIdToString(VesselIdToMap) {
    for (var i = 0; i < this.vessels.length; i++) {
      var item = this.vessels[i]
      if (item.Id == VesselIdToMap) {
        return item.VesselName
      }
    }
  }

  mapPORIdToString(PORIdToMap) {
    for (var i = 0; i < this.portsOfRegistry.length; i++) {
      var item = this.portsOfRegistry[i]
      if (item.Id == PORIdToMap) {
        return item.PortOfRegistry
      }
    }
  }

  mapApplyStatusIdToString(ApplyStatusIdToMap) {
    for (var i = 0; i < this.applicantStatus.length; i++) {
      var item = this.applicantStatus[i]
      if (item.Id == ApplyStatusIdToMap) {
        return item.ApplicantStatus
      }
    }
  }

  mapApplyPositionIdToString(ApplyPositionIdToMap) {
    for (var i = 0; i < this.positions.length; i++) {
      var item = this.positions[i]
      if (item.Id == ApplyPositionIdToMap) {
        return item.Position
      }
    }
  }

  mapOfferPositionStringToId(OfferPositionName) {
    for (var i = 0; i < this.positions.length; i++) {
      var item = this.positions[i]
      if (item.Position == OfferPositionName) {
        return item.Id
      }
    }
  }

  onSaveValueMapping() {
    this.applicant.TypesofAllowance = this.mapAllowanceIdToString(
      this.applicant.applicant_dropdown.AllowanceId
    )
    this.applicant.IMONo = this.mapImonoIdToString(
      this.applicant.applicant_dropdown.ImonoId
    )
    this.applicant.NameofVessel = this.mapVesselIdToString(
      this.applicant.applicant_dropdown.VesselId
    )
    this.applicant.PortofRegistry = this.mapPORIdToString(
      this.applicant.applicant_dropdown.PORId
    )
    this.applicant.Status = this.mapApplyStatusIdToString(
      this.applicant.applicant_dropdown.ApplyStatusId
    )

    if (this.applicant.ReviseStatus == 0) {
      this.applicant.OfferPosition = this.mapApplyPositionIdToString(
        this.applicant.ApplyPositionID
      )
    } else {
      this.applicant.OfferPosition = this.mapApplyPositionIdToString(
        this.applicant.OfferPositionID
      )
    }
  }

  onUpdate(event) {
    if (!this.applicant.applicant_dropdown.VesselId) {
      alert("Name of vessel is mandatory field.")
    } else {
      if (window.confirm('Do you really want to update?')) {
        this.onSaveValueMapping()
        this.onSaveSanitize()
  
        if (this.applicant.ContractPeriodTo < this.applicant.ContractPeriodFrom) {
          alert('Invalid Contract Period')
        } 
        // else if (this.applicant.adminName == null || this.applicant.adminName == '') {
        //   this.getAdminId();
        //   if (this.applicant.adminName == null || this.applicant.adminName == '') {
        //     alert("Unauthorized to update application. Please login again.")
        //   }
        // } 
        else {
          // find a way to save/update data
          const subscription = this.service
            .updateApplicant(JSON.stringify(this.applicant))
            .subscribe((res: any) => {
              if (res.Id == null) {
                alert('Failed to update applicant')
              } else {
                alert('Update Record Successful')
                //event.confirm.resolve(event.newData)
              }
              subscription.unsubscribe()
            })
          this.router.navigate(['pages/jobportal/applicant'])
        }
      } else {
        //event.confirm.reject()
      }  
    }
  }

  onConfirmValueMapping() {}

  onConfirmAdminSignature() {
    const dialogConfig = new MatDialogConfig()
    //dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true
    dialogConfig.width = '60%'
    dialogConfig.data = this.applicant
    this.dialog.open(SignatureComponent, dialogConfig)
    this.onSaveValueMapping()
  }

  getAdminId() {
    this.signatureService
      .getAdminDetails(localStorage.getItem('adminUsername'))
      .subscribe(
        (result: any) => {
          if(result.length != 0){
            this.adminDetails = result[0]
            if(!Array.isArray(this.adminDetails)){
              this.signature = this.adminDetails.Signature
              this.signatureAdmin = this.adminDetails.SignatureAdmin

              // this.applicant.adminName = this.adminDetails.UserName;
              // this.applicant.adminID = this.adminDetails.UserID;
            }else{
              this.signature = null;
              this.signatureAdmin = null;
            }
          }else{
            this.adminDetails = [];
            this.signature = null;
            this.signatureAdmin = null;
          }
        },
        (err) => alert('Failed to load admin details')
      )
  }

  onRevise(event) {
    this.applicant.ReviseStatus = 1;
    this.applicant.OfferPositionID = this.mapOfferPositionStringToId(this.applicant.OfferPosition)
    this.applicantStatus = this.applicantStatus.filter(item => item.ApplicantStatus !== 'New' && item.ApplicantStatus !== 'Review');
  }

  onTerminate(event) {
    if (this.applicant.Reason == null || this.applicant.Reason == '') {
      alert("Revise reason is required")
    } else {
      if (window.confirm('Do you really want to terminate?')) {

        this.onSaveValueMapping()

        var olddate = new Date(this.originalApplicantData.ContractPeriodTo)
        var newdate = new Date(this.applicant.ContractPeriodTo)
        var startdate = new Date(this.originalApplicantData.ContractPeriodFrom)
        var todaydate = new Date()

        if (olddate < todaydate) {
          alert("Error: Contract ended, cannot terminate anymore")
        // } else if (todaydate < startdate) {
        //   alert("Error: Contract not start yet, cannot terminate")
        } else if (newdate >= olddate) {
          alert("Error: Contract end date is the same or greater than the original contract end date")
        } else {
          event.srcElement.innerText = "Terminating..."
          event.srcElement.disabled = true
          const subscription = this.service
          .updateTerminateApplicant(JSON.stringify(this.applicant))
          .subscribe((res: any) => {
            if (res.success) {
              alert(res.message)
              // this.onSendEmailTerminate();
              this.getApplicantById(this.applicant.Id)
              // this.router.navigate(['pages/jobportal/applicant'])
            } else {
              alert(res.message)
              // Revert button text and enable
              event.srcElement.innerText = "Terminate"
              event.srcElement.disabled = false
            }
            subscription.unsubscribe()
          },(error)=>{
            // Revert button text and enable
            event.srcElement.innerText = "Terminate"
            event.srcElement.disabled = false
            alert(error)
          })

        }

      } else {
        // event.confirm.reject()
      }
    }
  }

  onReoffer(event) {
    if (this.applicant.Reason == null || this.applicant.Reason == '' || !this.applicant.applicant_dropdown.VesselId) {
      alert("Revise reason and Name of vessel are required")
    } else {
      if (window.confirm('Do you really want to re-offer?')) {

        this.onSaveValueMapping()

        event.srcElement.innerText = "Re-offering..."
          event.srcElement.disabled = true
          const subscription = this.service
          .updateReofferApplicant(JSON.stringify(this.applicant))
          .subscribe((res: any) => {
            if (res.success) {
              alert(res.message)
              this.applicant.Id = res.id;
              this.getApplicantById(this.applicant.Id)
              this.router.navigate(['pages/jobportal/view-applicant/'+res.id])
              // location.reload()
            } else {
              alert(res.message)
              // Revert button text and enable
              event.srcElement.innerText = "Re-Offer"
              event.srcElement.disabled = false
            }
            subscription.unsubscribe()
          },(error)=>{
            // Revert button text and enable
            event.srcElement.innerText = "Re-Offer"
            event.srcElement.disabled = false
            alert(error)
            location.reload()
          })
  
      } else {
        // event.confirm.reject()
      }
    }
  }

  onExtendTransfer(event) {
    if (this.applicant.Reason == null || this.applicant.Reason == '' || !this.applicant.applicant_dropdown.VesselId) {
      alert("Revise reason and Name of vessel are required")
    } else {
      if (window.confirm('Do you really want to extend/transfer?')) {

        this.onSaveValueMapping()
      
        event.srcElement.innerText = "Extend/Transferring..."
          event.srcElement.disabled = true
          const subscription = this.service
          .updateExtendTransferApplicant(JSON.stringify(this.applicant))
          .subscribe((res: any) => {
            if (res.success) {
              alert(res.message)
              this.applicant.Id = res.Id;
              this.getApplicantById(this.applicant.Id)
              this.router.navigate(['pages/jobportal/view-applicant/'+res.Id])
              location.reload()
            } else {
              alert(res.message)
              // Revert button text and enable
              event.srcElement.innerText = "Extend/Transfer"
              event.srcElement.disabled = false
            }
            subscription.unsubscribe()
          },(error)=>{
            // Revert button text and enable
            event.srcElement.innerText = "Extend/Transfer"
            event.srcElement.disabled = false
            alert(error)
            location.reload()
          })
  
      } else {
        // event.confirm.reject()
      }
    }
  }

  getSignature(event) {
    if (this.applicant.applicant_dropdown.ApplyStatusId == '19') {
      // Reject Application
      // if (this.applicant.adminName != null && this.applicant.adminName != '') {
        this.onReject(event);
      // } else {
      //   this.getAdminId();
      //   if (this.applicant.adminName == null || this.applicant.adminName == '') {
      //     alert("Unauthorized to reject application. Please login again.")
      //   }
      // }
    } else if (!this.applicant.applicant_dropdown.VesselId) {
      const element = <HTMLInputElement>document.getElementById("vessel_name")
      element.focus();
      alert("Name of vessel is mandatory field.")
    } else if (!this.validateData()) {
      console.log("Empty field");
    } else {
      // Offered Application
      if (this.signature != null && this.signature != '') {
        if (this.signatureAdmin != null && this.signatureAdmin != '') {
          this.onConfirm(event);
        } else {
  
          alert('Please upload Signature at User Id Configure!')
          this.router.navigate(['pages/jobportal/applicant'])
        }
      }else {
        alert('Please upload Signature at User Id Configure!')
        this.router.navigate(['pages/jobportal/applicant'])
      }
    }
  }

  onConfirm(event) {
    // Admin verification
    // if (this.applicant.adminName == null || this.applicant.adminName == '') {
    //     this.getAdminId();
    //     if (this.applicant.adminName == null || this.applicant.adminName == '') {
    //       alert("Unauthorized to approve application. Please login again.")
    //     }
    // } else 
    if (window.confirm('Do you really want to confirm?')) {

      this.onSaveValueMapping()
      this.onConfirmValueMapping()
      this.onSaveSanitize()

      if (this.applicant.ContractPeriodTo < this.applicant.ContractPeriodFrom) {
        alert('Invalid Contract Period')
      } else {
        // Change button text to 'Submitting...' and disable
        let btnUpdate = this.elementRef.nativeElement.querySelector(`#btnUpdate`);
        let btnCancel = this.elementRef.nativeElement.querySelector(`#btnCancel`);
        event.srcElement.innerText = "Confirming..."
        event.srcElement.disabled = true
        btnUpdate.disabled = true
        btnCancel.disabled = true
        const subscription = this.service
        .updateConfirmApplicant(JSON.stringify(this.applicant))
        .subscribe(async (res: any) => {
          if (res.success) {

            event.srcElement.innerText = "Generating AFE..."
            let hasAFE = await this.onGenerateAFE()
            event.srcElement.innerText = "Generating CV..."
            let hasCV = await this.onGenerateCV()
            event.srcElement.innerText = "Generating SEA..."
            let hasSEA = await this.onGenerateSEA()
            
            if (hasAFE && hasCV && hasSEA) {
              event.srcElement.innerText = "Confirmed"
              btnUpdate.disabled = false
              btnCancel.disabled = false
              alert(res.message)
              this.getApplicantById(this.applicant.Id)
            } else {
              event.srcElement.innerText = "Confirm"
              event.srcElement.disabled = false
              btnUpdate.disabled = false
              btnCancel.disabled = false
            }            
          } else {
            alert(res.message)
            // Revert button text and enable
            event.srcElement.innerText = "Confirm"
            event.srcElement.disabled = false
            btnUpdate.disabled = false
            btnCancel.disabled = false
          }
          subscription.unsubscribe()
        },(error)=>{
          // Revert button text and enable
          event.srcElement.innerText = "Confirm"
          event.srcElement.disabled = false
          btnUpdate.disabled = false
          btnCancel.disabled = false
          alert(error)
        })
      }
      
    } else {
      // event.confirm.reject()
    }
  }

  onReject(event) {
    if (window.confirm('Do you really want to reject the application?')) {
      this.onSaveValueMapping()

      // Change button text to 'Submitting...' and disable
      let btnUpdate = this.elementRef.nativeElement.querySelector(`#btnUpdate`);
      let btnCancel = this.elementRef.nativeElement.querySelector(`#btnCancel`);
      event.srcElement.innerText = "Rejecting..."
      event.srcElement.disabled = true
      btnUpdate.disabled = true
      btnCancel.disabled = true
      const subscription = this.service
      .updateRejectApplicant(JSON.stringify(this.applicant))
      .subscribe(async (res: any) => {
        if (res.success) {
          event.srcElement.innerText = "Rejected"
          btnUpdate.disabled = false
          btnCancel.disabled = false
          alert(res.message)
          this.getApplicantById(this.applicant.Id)           
        } else {
          alert(res.message)
          // Revert button text and enable
          event.srcElement.innerText = "Confirm"
          event.srcElement.disabled = false
          btnUpdate.disabled = false
          btnCancel.disabled = false
        }
        subscription.unsubscribe()
      },(error)=>{
        // Revert button text and enable
        event.srcElement.innerText = "Confirm"
        event.srcElement.disabled = false
        btnUpdate.disabled = false
        btnCancel.disabled = false
        alert(error)
      })
    }
  }

  validateData() {
    if ((this.applicant.OfferPositionID == null || this.applicant.OfferPositionID == '')
    && (this.applicant.ApplyPositionID == null || this.applicant.ApplyPositionID == '')) {
      const element = <HTMLInputElement>document.getElementById("position")
      element.focus();
      alert('Please fill in all the agreement data');
      return false;
    } else if (this.applicant.ContractPeriodFromInMth == null || this.applicant.ContractPeriodFromInMth == '') {
      const element = <HTMLInputElement>document.getElementById("contract_period")
      element.focus();
      alert('Please fill in all the agreement data');
      return false;
    } else if (this.applicant.ContractPeriodFrom == null || this.applicant.ContractPeriodFrom == '') {
      const element = <HTMLInputElement>document.getElementById("contract_period_from")
      element.focus();
      alert('Please fill in all the agreement data');
      return false;
    } else if (this.applicant.ContractPeriodTo == null || this.applicant.ContractPeriodTo == '') {
      const element = <HTMLInputElement>document.getElementById("contract_period_to")
      element.focus();
      alert('Please fill in all the agreement data');
      return false;
    } else if (this.applicant.applicant_dropdown.ImonoId == null || this.applicant.applicant_dropdown.ImonoId == '') {
      const element = <HTMLInputElement>document.getElementById("imono")
      element.focus();
      alert('Please fill in all the agreement data');
      return false;
    } else if (this.applicant.applicant_dropdown.PORId == null || this.applicant.applicant_dropdown.PORId == '') {
      const element = <HTMLInputElement>document.getElementById("port_of_registry")
      element.focus();
      alert('Please fill in all the agreement data');
      return false;
    } else if (this.applicant.RepatriationHomePort == null || this.applicant.RepatriationHomePort == '') {
      const element = <HTMLInputElement>document.getElementById("repatriation_port")
      element.focus();
      alert('Please fill in all the agreement data');
      return false;
    } else if (this.applicant.CurrencyID == null || this.applicant.CurrencyID == '') {
      const element = <HTMLInputElement>document.getElementById("currency")
      element.focus();
      alert('Please fill in the currency');
      return false;
    } else if (this.applicant.Salary == null || this.applicant.Salary == '') {
      const element = <HTMLInputElement>document.getElementById("amountSalary")
      element.focus();
      alert('Please fill in the salary amount');
      return false;
    } else {
      return true;
    }
  }

  onGenerateAFE() {
    return new Promise((resolve, reject) => {
      const subscription = this.generateDocService.AFE(this.applicant.LoginEmail, this.applicant.Id, this.applicant.adminName).subscribe(
        (result: any[]) => {
          console.log('Successfully generate AFE document for ' + this.applicant.LoginEmail)
          resolve(true)
        },
        (err) => { 
          alert('Failed to generate AFE document. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s)') 
          resolve(false)
        }
      )
    })
  }

  onGenerateCV() {
    return new Promise((resolve, reject) => {
      const subscription = this.generateDocService.CV(this.applicant.LoginEmail, this.applicant.Id, this.applicant.adminName).subscribe(
        (result: any[]) => {
          console.log('Successfully generate CV document for ' + this.applicant.LoginEmail)
          resolve(true)
        },
        (err) => {
          alert('Failed to generate CV document. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s)') 
          resolve(false)
        }
      )
    })
  }

  onGenerateSEA() {
    return new Promise((resolve, reject) => {
      const subscription = this.generateDocService.SEA(this.applicant.LoginEmail, this.applicant.Id, this.applicant.adminName).subscribe(
        (result: any[]) => {
          console.log('Successfully generate SEA document for ' + this.applicant.LoginEmail)
          resolve(true)
        },
        (err) => {
          alert('Failed to generate SEA document. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s)')
          resolve(false)
        }
      )
    })
  }

  onGenerateBAF() {
    return new Promise((resolve, reject) => {
      const subscription = this.generateDocService.BAF(this.applicant.LoginEmail, this.applicant.Id, this.applicant.adminName).subscribe(
        (result: any[]) => {
          console.log('Successfully generate BAF document for ' + this.applicant.LoginEmail)
          resolve(true)
        },
        (err) => {
          alert('Failed to generate BAF document');
          resolve(false)
        }
      )
    });
  }

  onBan(event) {
    if (window.confirm('Warning: do you really want to ban the applicant?')) {
      this.onSaveValueMapping()
      this.onConfirmValueMapping()
      this.onSaveSanitize()

      // find a way to save/update data
      const subscription = this.service
        .updateBanApplicant(JSON.stringify(this.applicant))
        .subscribe((res: any) => {
          if (res.Id == null) {
            alert('Failed to ban applicant')
          } else {
            alert('Ban Record Successful')
            //event.confirm.resolve(event.newData)
          }
          subscription.unsubscribe()
        })
      this.router.navigate(['pages/jobportal/applicant'])
    } else {
      //event.confirm.reject()
    }
  }

  onSendEmail(event) {
    let btnText = event.srcElement.innerText
    if (this.applicant.ApplyStatus != 'Offered') {
      alert('Unable to send email due to application has not been offered');
    } 
    // else if (this.applicant.FileSEA == null || this.applicant.FileSEA == '') {
    //   alert('Unable to send email due to application SEA document is not available. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document');
    // } 
    else {
      event.srcElement.innerText = "Sending..."
      event.srcElement.disabled = true

      const subscription = this.generateDocService.SendEmail(this.applicant.Id).subscribe(
        (result) => {
          if (result['success']) {
            alert(result['message'])
          } else {
            alert(result['message'])
          }
          window.location.reload();
        },
        (err) => { 
          alert(err);
          event.srcElement.innerText = btnText
          event.srcElement.disabled = false
        }
      )
    }
  }

  onSendEmailTerminate() {
    // if (this.applicant.ApplyStatus != 'Offered') {
    //   alert('Unable to send terminate email due to application has not been offered');
    // } 
    // else if (this.applicant.FileSEA == null || this.applicant.FileSEA == '') {
    //   alert('Unable to send email due to application SEA document is not available. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document');
    // } 
    // else {

      const subscription = this.applicationService.sendEmailTerminated(this.applicant.Id).subscribe(
        (result) => {
          if (result['success']) {
            alert(result['message'])
          } else {
            alert(result['message'])
          }
          window.location.reload();
        },
        (err) => { 
          alert(err);
        }
      )
    // }
  }
}
