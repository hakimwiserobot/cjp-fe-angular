import { Component, OnDestroy, OnInit } from '@angular/core'
import { UserService } from '../../services/user.service'
import { BaseService } from '../../services/base.service'

import { User } from '../../interfaces/user'

import { Injectable } from '@angular/core'
import { Subscription } from 'rxjs'

import { Router, ActivatedRoute } from '@angular/router'
//import * as bcrypt from 'bcryptjs';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'ngx-settings',
  templateUrl: './change-password.component.html',
})
export class AdminChangePasswordComponent implements OnInit, OnDestroy {
  _subscription: Subscription
  //user_change_password: User;
  email: string
  password: string
  new_password: string
  activateStatus: string
  new_email: string
  current_acc_password: string
  username: string
  retyped_password: string
  login_error: string = null
  //salt = bcrypt.genSaltSync(10);

  constructor(private service: UserService, private router: Router,
    private route: ActivatedRoute, private baseService: BaseService) {
    //console.log("constructor href: ", window.location.href)
  }

  ngOnInit(): void {
    console.log('user_email: ', localStorage.getItem('admin_user_email'))
    this.username = localStorage.getItem('admin_user_name');
    this.email = localStorage.getItem("admin_user_email");
    this.activateStatus = localStorage.getItem("admin_verify_status");

    // if the admin user didnt save any email, it will set the field as empty space
    if (!this.email || this.email == 'null') {
      this.email = " "
    } 
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe()
    }
  }

  _refreshData() {}

  changeEmail() {

    if (!this.new_email || !this.current_acc_password) {
      alert("All fields in this section are required!")
    } else {
      if (this.new_email && this.email != " " && this.current_acc_password) {
        const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        if (!emailRegexp.test(this.new_email) || (this.new_email == this.email)) {
          alert ("Invalid new email address");
        } else {
          this._subscription = this.service.updateAdminEmail({ 
            email: this.email,
            new_email: this.new_email,
            username: this.username,
            password: this.current_acc_password }
            ).subscribe((result: any) => {
            if(result.token == null) {
              if(result.error) {
                console.log(result)
                this.login_error = result.error
                alert(this.login_error)
              }
            } else {
              //console.log("changeEmail - result: ", result)
              this.new_email = '';
              this.current_acc_password = '';
  
              alert('Email Updated! Please check your email to activate the new email and login again');
  
              this.baseService.logoutAdmin();
              this.router.navigate(['/admin']);
            }
            this._subscription.unsubscribe()
          },
          (error) => {
            alert(error);
          })
        }
      } else {
        // when default email is missing
        this._subscription = this.service.updateAdminDefaultEmail({ 
          new_email: this.new_email,
          username: this.username,
          password: this.current_acc_password }
          ).subscribe((result: any) => {
          if(result.token == null) {
            if(result.error) {
              console.log(result)
              this.login_error = result.error
              alert(this.login_error)
            }
          } else {
            //console.log("changeEmail - result: ", result)
            this.new_email = '';
            this.current_acc_password = '';

            alert('Email Updated! Please check your email to activate the new email and login again');

            this.baseService.logoutAdmin();
            this.router.navigate(['/admin']);
          }
          this._subscription.unsubscribe()
        },
        (error) => {
          alert(error);
        })
      }
    }
    
    

  }

  resendActivation() {
    if (this.email != " ") {
      this._subscription = this.service.resendActivationAdmin({ 
        email: this.email,
        username: this.username })
        .subscribe(
          (result: any) => {
            alert("The activation email has been sent to your email, please click the activation link within 24 hours and login again")
          },
          (err: any) => {
            alert("Unable to send activation link. Please try again.")
          }
        );
    } else {
      alert("Invalid email! Please try again")
    }
  }

  changePassword() {
    //console.log('password: ', this.new_password)
    if (this.new_password == this.retyped_password) {
      this._subscription = this.service
        .updateAdminPassword({
          username: this.username,
          password: this.password,
          new_password: this.new_password,
          retyped_password: this.retyped_password,
        })
        .subscribe(
          (result: any) => {
            if (result.token == null) {
              if (result.error) {
                console.log(result)
                this.login_error = result.error
                alert(this.login_error)
              }
            } else {
              //console.log('changePassword - result: ', result)
              alert('Password Changed!');
              this.router.navigate(['/pages/dashboard/'])
            }
            this._subscription.unsubscribe()
          },
          (error) => {
            alert('Password Incorrect!');
          }
        )
    } else {
      alert('Retype Password not Match!')
    }
  }
}
