import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbThemeModule, NbButtonModule, NbLayoutModule, NbCheckboxModule } from '@nebular/theme'

import { ThemeModule } from '../../../@theme/theme.module';
import { ApplicantBankAllotmentListComponent } from './applicant-bank-allotment-list.component';
import { TranslateModule } from '@ngx-translate/core'

@NgModule({
  declarations: [ApplicantBankAllotmentListComponent],
  imports: [
    CommonModule,
    Ng2SmartTableModule,
    ThemeModule,
    NbThemeModule,
    NbButtonModule,
    NbLayoutModule,
    NbCheckboxModule,
    TranslateModule
  ]
})
export class ApplicantBankAllotmentListModule { }
