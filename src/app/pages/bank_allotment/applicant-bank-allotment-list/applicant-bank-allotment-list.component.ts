import { Component, Injectable, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { LocalDataSource } from 'ng2-smart-table'
import { CheckboxComponent } from '../../../shared/CheckboxComponent.component'
import { BankAllotmentService } from '../../../services/bankAllotment.service'
import { BankAllotment } from '../../../interfaces/bankallotment'
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ngx-applicant-bank-allotment-list',
  templateUrl: './applicant-bank-allotment-list.component.html',
  styleUrls: ['./applicant-bank-allotment-list.component.scss']
})
export class ApplicantBankAllotmentListComponent implements OnInit {

  username: string
  userEmail: string
  activateStatus: string

  applicantID = ''
  bankAllotmentList = []
  sttgSmrtTbl = {
    actions: {
      position: 'right',
      add: false,
      edit: false,
      delete: false,
    },
    hideSubHeader: true,
    columns: {
      Id: {
        title: 'Id',
        hide: true
      },
      No: {
        title: this.translate.instant("userBankAllotmentListPage.tblBankAllotment.header1"),
      },
      BeneType: {
        title: this.translate.instant("label.type"),
        filter: false,
      },
      Name: {
        title: this.translate.instant("userBankAllotmentListPage.tblBankAllotment.header2"),
        filter: false,
      },
      BankName: {
        title: this.translate.instant("userBankAllotmentListPage.tblBankAllotment.header3"),
        filter: false,
      },
      BankAccount: {
        title: this.translate.instant("userBankAllotmentListPage.tblBankAllotment.header4"),
        filter: false,
      },
      // Chk:{
      //   title: 'Select',
      //   type: 'custom',
      //   filter: false,
      //   renderComponent: CheckboxComponent,
      //   valuePrepareFunction: (value, row) => {
      //     return { value, column: 'Chk', callback: () => {
      //         console.log("Updating bank allotment selection...")
      //         this.idSelected = row.Id;
      //         this.updateSelection()
      //       }
      //     }
      //   },
      // },
      action: {
        title: this.translate.instant("userBankAllotmentListPage.tblBankAllotment.header5"),
        type: 'html',
        filter: false,
      },
    },
  }

  isChange = false
  idSelected = ''

  constructor(private router: Router, private bankallotmentService:BankAllotmentService, private translate:TranslateService) { }

  ngOnInit(): void {
    this.username = localStorage.getItem('user_name');
    this.userEmail = localStorage.getItem("user_email");
    this.activateStatus = localStorage.getItem("verify_status");

    if (this.activateStatus === "0") {
      alert("Please activate your account first.")
      this.router.navigate(['/pages/applicant_jobportal/change-password'])
    }
    
    this.getBankAllotment()
  }

  onAddBankAllotment($event) {
    this.router.navigate(
      ['pages/applicant_jobportal/applicant-bank-allotment', 'add'] 
    )
  }

  // Service section
  getBankAllotment() {
    return new Promise((resolve, reject) => {
      let applicantId = localStorage.getItem('user_email')
      const subscription = this.bankallotmentService.getBankAllotmentByApplicantId(applicantId).subscribe(
        (result: any) => {
          if (result) {
            this._refreshBankAllotmentListData(result)
          }
          return resolve(true)
        },
        (err) => {
          alert('Failed to load list')
          return reject(false)
        }
      )
    })
  }

  updateSelection() {
    return new Promise((resolve, reject) => {
      let dataToUpdate = {
        ApplicantID:localStorage.getItem('user_email'),
        IdNew:this.idSelected
      }
      const subscription = this.bankallotmentService.updateBankAllotmentSelections(dataToUpdate).subscribe(
        (result: any) => {
          if (result.success) {
            alert(result.message)
            window.location.reload();
          }
          return resolve(true)
        },
        (err) => {
          alert(this.translate.instant("userBankAllotmentListPage.alert.errorList"))
          return reject(false)
        }
      )
    })
  }

  // Alter service response section
  _refreshBankAllotmentListData(data) {
    this.bankAllotmentList = [];
    for (let i = 0; i < data.length; i++) {
      let beneficiary = data[i]
      let beneficiaryName = beneficiary.Name
      if (beneficiary.MiddleName) {
        beneficiaryName += ' ' + beneficiary.MiddleName
      }
      if (beneficiary.LastName) {
        beneficiaryName += ' ' + beneficiary.LastName
      }
      this.bankAllotmentList.push({
        No: i + 1,
        Id: beneficiary.Id,
        BeneType: beneficiary.BeneficiaryType,
        Name: beneficiaryName,
        BankName: beneficiary.BankName,
        BankAccount: beneficiary.AccountNumber,
        Chk: beneficiary.Chk,
        action: '<a href="pages/applicant_jobportal/applicant-bank-allotment/' + beneficiary.Id + '">'+ this.translate.instant('btn.edit') +'</a>',
      })
    }
  }
}
