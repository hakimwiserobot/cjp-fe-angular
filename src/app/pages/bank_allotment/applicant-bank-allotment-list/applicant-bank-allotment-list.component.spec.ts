import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantBankAllotmentListComponent } from './applicant-bank-allotment-list.component';

describe('ApplicantBankAllotmentListComponent', () => {
  let component: ApplicantBankAllotmentListComponent;
  let fixture: ComponentFixture<ApplicantBankAllotmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantBankAllotmentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantBankAllotmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
