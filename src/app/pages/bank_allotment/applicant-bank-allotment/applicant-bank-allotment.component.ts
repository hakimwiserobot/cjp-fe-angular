import { Component, OnInit, OnDestroy } from '@angular/core';
import { Injectable } from '@angular/core'
import { Subscription } from 'rxjs'
import { Router, ActivatedRoute } from '@angular/router'
import { MatDialogRef, MatDialog } from '@angular/material/dialog'

import { ApplicantService } from '../../../services/applicant.service'
import { ApplicationService } from '../../../services/application.service'
import { BankAllotmentService } from '../../../services/bankAllotment.service'
import { RelationshipService } from '../../../services/relationship.service'
import { UserService } from '../../../services/user.service'
import { CountryService } from '../../../services/country.service'

import { Relationship } from '../../../interfaces/relationship'
import { BankAllotment } from '../../../interfaces/bankallotment'

import { SimpleDialogComponent, DialogContentExampleDialog } from '../../applicant_jobportal/crew-job-portal.component'
import { SignatureDialogComponent} from '../../../shared/signature-dialog/signature-dialog.component'
import { PasswordDialogComponent} from '../../../shared/password-dialog/password-dialog.component'

import { environment } from '../../../../environments/environment'
import { resolve } from 'dns';

import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'ngx-applicant-bank-allotment',
  templateUrl: './applicant-bank-allotment.component.html',
  styleUrls: ['./applicant-bank-allotment.component.scss']
})
export class ApplicantBankAllotmentComponent implements OnInit, OnDestroy {

  Id = ''
  Password = ''
  saveFileSuccess = false
  docSupporting = [
    // {
    // DocumentID:'1',
    // Document:'Bank Book',
    // FileName: '',
    // File:null,
    // ColumnType:'',
    // show:true,
    // },
    // {
    //   DocumentID:'2',
    //   Document:'Marriage Cert',
    //   FileName: '',
    //   File:null,
    //   ColumnType:'',
    //   show:true,
    // },
    // {
    //   DocumentID:'3',
    //   Document:'IC / Passport',
    //   FileName: '',
    //   File:null,
    //   ColumnType:'',
    //   show:true,
    // },
    // {
    //   DocumentID:'4',
    //   Document:'Birth Cert Crew',
    //   FileName: '',
    //   File:null,
    //   ColumnType:'',
    //   show:true,
    // },
    // {
    //   DocumentID:'5',
    //   Document:'Birth Cert Sibling',
    //   FileName: '',
    //   File:null,
    //   ColumnType:'',
    //   show:true,
    // }
  ]

  applicant:any = {
    Id:localStorage.getItem('user_id'),
    LoginEmail:localStorage.getItem('user_email'),
    Name:'',
    MiddleName:'',
    LastName:''
  }

  details?:BankAllotment = {
    Id:0,
    Name:'',
    MiddleName:'',
    LastName:'',
    Relationship:'',
    ContactCtryCode:'',
    ContactNumber: '',
    BeneficiaryType:'',
    IdentityType:'',
    Identification:'',
    BankName:'',
    BankAccountNum:'',
    BankPlace:'',
    BankSwissCode:'',
    Currency:'MYR',
    FileBankBook:'',
    FileMarriageCert:'',
    FileICPassport:'',
    FileBirthCert:'',
    FileBirthCert2:'',
    FileFamilyCard:'',
    Signature:'',
    isSelected:false
  }

  SelectionBeneficiaryType:Array<string> = ['Own', 'Spouse', 'Parent'];
  SelectionIdentityType:Array<string> = ['IC', 'Passport'];
  SelectionRelationship:Array<any> = [];
  SelectionCurrency:Array<any> = [];
  SelectionPhoneCode: Array<any> = [];
  isFileUpdate = false;
  isAdd = true;

  lblBtnAdd = '';

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private applicantService: ApplicantService, 
    private applicationService: ApplicationService, 
    private bankallotmentService: BankAllotmentService,
    private relationshipService: RelationshipService,
    private userService: UserService,
    private countryService: CountryService,
    private translate: TranslateService) { }

  async ngOnInit(): Promise<void> {
    this.details.BeneficiaryType = 'Choose...'
    this.Id = this.activatedRoute.snapshot.params.Id
    await this.getRelationships()
    await this.getCountryPhoneCode();
    await this.getCountryCurrency();
    await this.getApplicantByLoginEmail()
    if (this.Id != 'add') {
      this.lblBtnAdd = this.translate.instant('btn.update');
      this.isAdd = false
      await this.getBankAllotmentDetails()
    } else {
      this.lblBtnAdd = this.translate.instant('btn.add');
    }
  }

  ngOnDestroy(): void {
    
  }

  updateBankAllotmentBeneficiaryType($event) {
    // If BeneficiaryType == 'own', auto fill applicant details
    if (this.details.BeneficiaryType == 'Own') {
      this.details.Name = this.applicant.Name;
      this.details.MiddleName = this.applicant.MiddleName;
      this.details.LastName = this.applicant.LastName;
      if (this.applicant.IC != null && this.applicant.IC.length > 0) {
        this.details.IdentityType = 'IC'
        this.details.Identification = this.applicant.IC
      } else if (this.applicant.Passport != null && this.applicant.Passport.length > 0) {
        this.details.IdentityType = 'Passport'
        this.details.Identification = this.applicant.Passport
      }
    } else {
      this.details.Name = '';
      this.details.MiddleName = '';
      this.details.LastName = '';
      this.details.IdentityType = '';
      this.details.Identification = '';
    }

    this.updateSupportingDoc()
  }

  updateSupportingDoc() {
    // Updating require supporting documents base on beneficiary type
    this.docSupporting = [{
      DocumentID:'1',
      Document:this.translate.instant('doc.bankBook'),
      FileName: '',
      File:null,
      ColumnType:'',
      show:true,
    },
    {
      DocumentID:'3',
      Document:this.translate.instant('doc.icPassport'),
      FileName: '',
      File:null,
      ColumnType:'',
      show:true,
    }];
    if (this.details.BeneficiaryType == 'Spouse') {
      this.docSupporting.push({
        DocumentID:'2',
        Document:this.translate.instant('doc.certMarriage'),
        FileName: '',
        File:null,
        ColumnType:'',
        show:true,
      })
    }
    if (this.details.BeneficiaryType == 'Parent' || this.details.BeneficiaryType == 'Sibling') {
      this.docSupporting.push({
        DocumentID:'4',
        Document:this.translate.instant('doc.certBirthCrew'),
        FileName: '',
        File:null,
        ColumnType:'',
        show:true,
      })
    }
    if (this.details.BeneficiaryType == 'Sibling') {
      this.docSupporting.push({
        DocumentID:'5',
        Document:this.translate.instant('doc.certBirthSibling'),
        FileName: '',
        File:null,
        ColumnType:'',
        show:true,
      })
    }
    // Family Card
    if (this.details.BeneficiaryType == 'Sibling' || this.details.BeneficiaryType == 'Parent' || this.details.BeneficiaryType == 'Spouse') {
      this.docSupporting.push({
        DocumentID:'6',
        Document:this.translate.instant('doc.familyCard'),
        FileName: '',
        File:null,
        ColumnType:'',
        show:true,
      })
    }
  }

  validateSupportingDoc() {
    var isMissing = false;
    for (var i = 0; i < this.docSupporting.length; i++) {
      if (this.docSupporting[i].File == null && this.docSupporting[i].FileName == '') {
        return false;
      }
    }

    return true;
  }

  validateSelection() {
    
  }

  initSupportingDocuments() {

  }

  // Service section
  getBankAllotmentDetails() {
    return new Promise((resolve, reject) => {
      const subscription = this.bankallotmentService.getBankAllotment(this.Id).subscribe(
        (result: any) => {
          if (result) {
            this._refreshBankAllotmentData(result)
          }
          return resolve(true)
        },
        (err) => {
          alert('Failed to load details. Please try again')
          return reject(false)
        }
      )
    })
  }

  getRelationships() {
    return new Promise((resolve, reject) => {
      const subscription = this.relationshipService.getAllRelationships().subscribe(
        (result: any) => {
          this.SelectionRelationship = result
          this._refreshRelationshipData()
          return resolve(true)
        },
        (err) => {
          alert('Failed to load relationships')
          return reject(false)
        }
      )
    })
  }

  getCountryCurrency() {
    return new Promise((resolve, reject) => {
      const subscription = this.countryService.getAllCountriesCurrency().subscribe(
        (result: any) => {
          this.SelectionCurrency = result
          return resolve(true)
        },
        (err) => {
          alert('Failed to load currency')
          return reject(false)
        }
      )
    })
  }

  getCountryPhoneCode() {
    return new Promise((resolve, reject) => {
      const subscription = this.countryService.getAllCountriesPhoneCodes().subscribe(
        (result: any) => {
          if (result) {
            this.SelectionPhoneCode = [];
            let topPhoneCode = [];
            Object.keys(result).forEach((key) => {
              let code = result[key];
              code = code.replace(/and/gi, '/');
              if (code == "60" || code == "62") {
                topPhoneCode.push({ country:key, phoneCode:code });
              } else {
                this.SelectionPhoneCode.push({ country:key, phoneCode:code });
              }
            });
            this.SelectionPhoneCode.sort((a, b) => (a.country > b.country) ? 1 : -1);
            this.SelectionPhoneCode = topPhoneCode.concat(this.SelectionPhoneCode);
            return resolve(true);
          }
          return resolve(false);
        },
        (err) => { 
          alert('Failed to countries phone codes');
          return reject(false);
        }
      )
    });
  }

  getApplicantByLoginEmail() {
    return new Promise((resolve, reject) => {
      const subscription = this.applicantService.getApplicantByLoginEmail(this.applicant.LoginEmail).subscribe(
        (result) => {
          this.applicant = result
          return resolve(true)
        },
        (err) => { 
          alert('Failed to load applicant details')
          return resolve(false)
        }
      )
    })
  }

  // Alter service response section
  _refreshBankAllotmentData(data) {
    this.details.Id = data.Id
    this.details.Name = data.Name
    this.details.MiddleName = data.MiddleName
    this.details.LastName = data.LastName
    this.details.Relationship = data.Relationship
    this.details.ContactCtryCode = data.ContactCtryCode;
    this.details.ContactNumber = data.ContactNumber;
    this.details.BeneficiaryType = data.BeneficiaryType
    this.details.IdentityType = data.IdentityType
    this.details.Identification = data.Identification
    this.details.BankName = data.BankName
    this.details.BankAccountNum = data.AccountNumber
    this.details.BankPlace = data.BankPlace
    this.details.BankSwissCode = data.BankSwiftCode
    this.details.Currency = data.Currency
    this.details.FileBankBook = data.FileBankBook
    this.details.FileMarriageCert = data.FileMarriageCert
    this.details.FileICPassport = data.FileICPassport
    this.details.FileBirthCert = data.FileBirthCert
    this.details.FileBirthCert2 = data.FileBirthCert2
    this.details.FileFamilyCard = data.FileFamilyCard // Family Card
    this.details.isSelected = data.Chk == 'N' ? false : true

    // Update supporting documents
    this.updateSupportingDoc()
    for (let i = 0; i < this.docSupporting.length; i++) {
      if (this.docSupporting[i].DocumentID == '1' && this.details.FileBankBook != null) {
        this.docSupporting[i].FileName = this.details.FileBankBook;
      }
      if (this.docSupporting[i].DocumentID == '2' && this.details.FileMarriageCert != null) {
        this.docSupporting[i].FileName = this.details.FileMarriageCert;
      }
      if (this.docSupporting[i].DocumentID == '3' && this.details.FileICPassport != null) {
        this.docSupporting[i].FileName = this.details.FileICPassport;
      } 
      if (this.docSupporting[i].DocumentID == '4' && this.details.FileBirthCert != null) {
        this.docSupporting[i].FileName = this.details.FileBirthCert;
      } 
      if (this.docSupporting[i].DocumentID == '5' && this.details.FileBirthCert2 != null) {
        this.docSupporting[i].FileName = this.details.FileBirthCert2;
      }
      // Family Card
      if (this.docSupporting[i].DocumentID == '6' && this.details.FileFamilyCard != null) {
        this.docSupporting[i].FileName = this.details.FileFamilyCard;
      }
    }    
  }

  _refreshRelationshipData() {
    this.SelectionRelationship.map((item: Relationship) => {
      return {
        Id: item.Id,
        Relationship: item.Relationship
      }
    })
  }

  // Adding Bank Allotment Section
  async openDialog(event) {
    if (this.details.BeneficiaryType == 'Own') {
      if (this.details.IdentityType == 'IC' && this.applicant.IC != this.details.Identification) {
        await this.onUpdateUserICPassport(true, false);
      }
      if (this.details.IdentityType == 'Passport' && this.applicant.Passport != this.details.Identification) {
        await this.onUpdateUserICPassport(false, true);
      }
    }

    if (this.details.IdentityType == '' || this.details.IdentityType == null || this.details.Identification == '' || this.details.Identification == null) {
      alert(this.translate.instant("alert.emptyBeneIdentityField"));
    } else if (this.details.BankName == '' || this.details.BankName == null) {
      alert(this.translate.instant("alert.emptyFieldBankName"));
    } else if (this.details.BankPlace == '' || this.details.BankPlace == null) {
      alert(this.translate.instant("alert.emptyFieldBankPlace"));
    } else if (this.details.BankAccountNum == '' || this.details.BankAccountNum == null) {
      alert(this.translate.instant("alert.emptyFieldBankAcc"));
    } else if (this.details.IdentityType == 'Passport' && (this.details.BankSwissCode == null || this.details.BankSwissCode == '')) {
      alert(this.translate.instant("alert.emptyFieldBanSwissCode"));
    } else if (!this.validateSupportingDoc()) { 
      alert(this.translate.instant("alert.emptySupportingDoc"));
    } else {

      // Request signature
      const dialogRef = this.dialog.open(SignatureDialogComponent, {
        width: '100%',
        panelClass: 'custom-modalbox',
        data:this.details.Signature
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The signature dialog was closed');

        if (result) {
          this.details.Signature = result;

          // Request password
          const dialogRef2 = this.dialog.open(PasswordDialogComponent, {
            panelClass: 'custom-modalbox',
            data:this.applicant.LoginEmail
          });
          dialogRef2.afterClosed().subscribe(result => {
            console.log('The password dialog was closed');

            if (result) {
              // Call add bank allotment
              if (this.isAdd) {
                this.onCreateBankAllotment()
              } else {
                this.onUpdateDetails()
                // if (this.isFileUpdate) {
                //   this.onUpdateFiles()
                // }
              }
            }
          });

        }
      });
    }
  }

  onCreateBankAllotment() {

    const dataToCreate = {
      LoginEmail:this.applicant.LoginEmail,
      BeneficiaryType:this.details.BeneficiaryType,
      Name:this.details.Name,
      MiddleName:this.details.MiddleName,
      LastName:this.details.LastName,
      Relationship: this.details.Relationship,
      ContactCtryCode: this.details.ContactCtryCode,
      ContactNumber: this.details.ContactNumber,
      IdentityType:this.details.IdentityType,
      Identification:this.details.Identification,
      BankName:this.details.BankName,
      BankPlace:this.details.BankPlace,
      BankSwiftCode:this.details.BankSwissCode,
      AccountNumber:this.details.BankAccountNum,
      Currency:this.details.Currency,
      FileBankBook:this.details.FileBankBook,
      FileICPassport:this.details.FileICPassport,
      FileMarriageCert:this.details.FileMarriageCert,
      FileBirthCert:this.details.FileBirthCert,
      FileBirthCert2:this.details.FileBirthCert2,
      FileFamilyCard:this.details.FileFamilyCard, // Family Card
      Signature:this.details.Signature
    }

    const btnAdd = (<HTMLInputElement>document.getElementById('btnAdd'));
    const btnCancel = (<HTMLInputElement>document.getElementById('btnCancel'));
    
    btnAdd.disabled = true;
    btnCancel.disabled = true;
    
    const subscription = this.bankallotmentService.addBankAllotment(dataToCreate).subscribe((res: any) => {
      if (res.success) {
        this.details.Id = res.Id;
        if (this.details.Id != 0) {
          this.onSaveFile()
        }
        alert(res.message)
        this.router.navigateByUrl('/pages/applicant_jobportal/applicant-bank-allotment-list') 
      } else {
        alert(res.message)
        btnAdd.disabled = false;
        btnCancel.disabled = false;
      }
      subscription.unsubscribe()
    }, (error)=>{
      alert(error);
      btnAdd.disabled = false;
      btnCancel.disabled = false;
    })
  }

  onUpdateDetails() {
    const dataToUpdate = {
      Id:this.Id,
      LoginEmail:this.applicant.LoginEmail,
      BeneficiaryType:this.details.BeneficiaryType,
      Name:this.details.Name,
      MiddleName:this.details.MiddleName,
      LastName:this.details.LastName,
      Relationship: this.details.Relationship,
      ContactCtryCode: this.details.ContactCtryCode,
      ContactNumber: this.details.ContactNumber,
      IdentityType:this.details.IdentityType,
      Identification:this.details.Identification,
      BankName:this.details.BankName,
      BankPlace:this.details.BankPlace,
      BankSwiftCode:this.details.BankSwissCode,
      AccountNumber:this.details.BankAccountNum,
      Currency:this.details.Currency,
      Signature:this.details.Signature
    }

    const btnAdd = (<HTMLInputElement>document.getElementById('btnAdd'));
    const btnCancel = (<HTMLInputElement>document.getElementById('btnCancel'));
    
    btnAdd.disabled = true;
    btnCancel.disabled = true;
    
    const subscription = this.bankallotmentService.updateBankAllotmentDetails(dataToUpdate).subscribe((res: any) => {
      if (res.success) {
        this.onSaveFile()
        alert(res.message)
        this.router.navigateByUrl('/pages/applicant_jobportal/applicant-bank-allotment-list') 
      } else {
        alert(res.message)
        btnAdd.disabled = false;
        btnCancel.disabled = false;
      }
      subscription.unsubscribe()
    }, (error)=>{
      alert(error);
      btnAdd.disabled = false;
      btnCancel.disabled = false;
    })
  }

  onUpdateFiles() {
    const dataToUpdate = {
      Id:this.Id,
      LoginEmail:this.applicant.LoginEmail,
      FileBankBook:this.details.FileBankBook,
      FileICPassport:this.details.FileICPassport,
      FileMarriageCert:this.details.FileMarriageCert,
      FileBirthCert:this.details.FileBirthCert,
      FileBirthCert2:this.details.FileBirthCert2,
      FileFamilyCard:this.details.FileFamilyCard // Family Card
    }
    
    const subscription = this.bankallotmentService.updateBankAllotmentfiles(dataToUpdate).subscribe((res: any) => {
      if (res.success) {
        this.details.Id = res.Id;
        if (this.details.Id != 0) {
          this.onSaveFile()
        }
        alert(res.message)
        this.router.navigateByUrl('/pages/applicant_jobportal/applicant-bank-allotment-list') 
      } else {
        alert(res.message)
      }
      subscription.unsubscribe()
    }, (error)=>{
      alert(error);
    })
  }

  // File section
  viewFile(filePath) {
    console.log("filePath: ", filePath)
    if (filePath && filePath.length > 0) {
      window.open(`${environment.documentPathPrefix}/` + filePath, '_blank')
    }
  }

  fileChange(event, documentId, columnType) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let updatedDoc = this.docSupporting.find(function(doc, index) {
        if(doc.DocumentID == documentId)
          return true;
      })
      updatedDoc.File = file
      updatedDoc.ColumnType = 'supportingDocument'
      this.isFileUpdate = true
    }
  }

  onSaveFile() {
    console.log("this.fileUploadList: ", this.docSupporting)
    for (var i = 0; i < this.docSupporting.length; i++) {
      if (this.docSupporting[i].File == null) {
        continue;
      }
      const fileUploadSubscription = this.applicationService.uploadDocument(this.docSupporting[i].DocumentID,
        this.details.Id.toString(), this.docSupporting[i].File, this.docSupporting[i].ColumnType).subscribe(
        (sigRes: any) => {
          if (!sigRes) {
            alert('Failed to upload document')
          } 
          // comment by joe to remove multiple alert message
          // else {
          //   alert('Save as Draft New Record Successful 1111')
          //   //this.router.navigate(['pages/jobportal/crew_job_portal/', this.applicant.LoginEmail])
          // }
          this.saveFileSuccess = false
          fileUploadSubscription.unsubscribe()
        },
        (err) => {
          alert('Failed to upload file ' + this.docSupporting[i].File.name + '. Make sure you select the correct file.')
        }
      )
    }
  }

  onRemoveFile(documentId) {
    if (window.confirm(this.translate.instant('userBankAllotmentPage.alert.removeFile'))) {
      let dataToUpdate = {
        Id: this.details.Id,
        LoginEmail: this.applicant.LoginEmail,
        FileBankBook: documentId == '1' ? '':this.details.FileBankBook,
        FileMarriageCert: documentId == '2' ? '':this.details.FileMarriageCert,
        FileICPassport: documentId == '3' ? '':this.details.FileICPassport,
        FileBirthCert: documentId == '4' ? '':this.details.FileBirthCert,
        FileBirthCert2: documentId == '5' ? '':this.details.FileBirthCert2,
        FamilyCard: documentId == '6' ? '':this.details.FileFamilyCard // Family Card
      }
      const subscription = this.bankallotmentService.updateBankAllotmentfiles(dataToUpdate).subscribe(
        (result: any) => {
          console.log(result)
          if(result.success) {
            alert(result.message)
            window.location.reload()
          }
        },
        (err) => alert(err)
      )
    }
  }

  // Cancel button section
  onCancel(event) {
    if (!this.isAdd) {
      if (window.confirm(this.translate.instant('userBankAllotmentPage.alert.cancelUpdate'))) {
        this.router.navigateByUrl('/pages/applicant_jobportal/applicant-bank-allotment-list') 
      } else {
        event.confirm.reject()
      }
    } else {
      this.router.navigateByUrl('/pages/applicant_jobportal/applicant-bank-allotment-list') 
    }
  }

  // Updating ic and passport section
  async onUpdateUserICPassport(isIC:boolean, isPassport:boolean) {
    let msg = "";
    if (isIC && !isPassport) {
      msg = this.translate.instant('word.ic');
    } else if (!isIC && isPassport) {
      msg = this.translate.instant('word.passport');
    }

    if (window.confirm(this.translate.instant('alert.newICPassport', { icpassport:msg }))) {
      let dataToUpdate = {};
      if (this.details.IdentityType == 'IC') {
        dataToUpdate = {
          Id:this.applicant.Id,
          IC:this.details.Identification,
        }
      } else if (this.details.IdentityType == 'Passport') {
        dataToUpdate = {
          Id:this.applicant.Id,
          Passport:this.details.Identification,
        }
      }
      
      return new Promise((resolve, reject) => {
        this.userService.updateUserICPassport(dataToUpdate).subscribe((res: any) => {
          if (res.success) {
            return resolve(true);
          } else {
            alert(res.message);
            return resolve(false);
          }
        }, (error)=>{
          alert(error);
          return resolve(false);
        })
      })
    } else {
      console.log("Cancel update nric/passport");
    }
  }
}
