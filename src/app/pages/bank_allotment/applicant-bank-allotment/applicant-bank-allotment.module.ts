import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ApplicantBankAllotmentComponent } from './applicant-bank-allotment.component';

import { ThemeModule } from '../../../@theme/theme.module'
import { NbThemeModule, NbButtonModule, NbLayoutModule } from '@nebular/theme'
import { TranslateModule } from '@ngx-translate/core'

@NgModule({
  declarations: [ApplicantBankAllotmentComponent],
  imports: [
    FormsModule,
    ThemeModule,
    NbThemeModule,
    NbButtonModule,
    NbLayoutModule,
    TranslateModule
  ],
})
export class ApplicantBankAllotmentModule { }
