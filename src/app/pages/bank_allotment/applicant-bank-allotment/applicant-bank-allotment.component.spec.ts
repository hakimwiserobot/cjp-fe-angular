import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantBankAllotmentComponent } from './applicant-bank-allotment.component';

describe('ApplicantBankAllotmentComponent', () => {
  let component: ApplicantBankAllotmentComponent;
  let fixture: ComponentFixture<ApplicantBankAllotmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantBankAllotmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantBankAllotmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
