import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBankAllotmentListComponent } from './admin-bank-allotment-list.component';

describe('AdminBankAllotmentListComponent', () => {
  let component: AdminBankAllotmentListComponent;
  let fixture: ComponentFixture<AdminBankAllotmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBankAllotmentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBankAllotmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
