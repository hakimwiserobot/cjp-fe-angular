import { Component, OnDestroy, OnInit } from '@angular/core'
import { DownloadButton } from '../../../shared/DownloadButton.component'

import { LocalDataSource } from 'ng2-smart-table'
import { Subscription } from 'rxjs'
import { BankAllotmentService } from '../../../services/bankAllotment.service'
import { GenerateDocService } from '../../../services/generateDoc.service'
import { VesselNameService } from '../../../services/vessel-name.service'
import { Position } from '../../../interfaces/position'
import { Applicant, ApplicantDocument } from '../../../interfaces/applicant'
import { formatDate } from '@angular/common'
import { BankAllotment } from '../../../interfaces/bankallotment'
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-admin-bank-allotment-list',
  templateUrl: './admin-bank-allotment-list.component.html',
  styleUrls: ['./admin-bank-allotment-list.component.scss']
})
export class AdminBankAllotmentListComponent implements OnInit, OnDestroy {

  bankAllotmentList: any[] = []
  vesselList: any[] = []
  searchCrewEmail: string = ''
  searchName: string = ''
  searchEmpId: string = ''
  searchVessel: string = ''
  searchDate: any
  searchDate2: any
  username: string
  userEmail: string
  activateStatus: string

  exportData: any[] = []
  public source = new LocalDataSource()

  settings = {
    hideSubHeader: true, // hide the add new fields
    columns: {
      No: {
        title: 'No',
        filter: false,
        editable: false,
        addable: false,
      },
      Id: {
        title: 'Id',
        hide: true
      },
      ApplyID: {
        title: 'ApplyID',
        hide: true
      },
      Email: {
        title: 'Crew Email',
        hide: false
      },
      Vessel: {
        title: 'Vessel',
        filter: false,
      },
      ContractPeriodFrom: {
        title: 'Contract Period Start',
        filter: false,
      },
      ContractPeriodTo: {
        title: 'Contract Period End',
        filter: false,
      },
      Status: {
        title: 'Status',
        filter: false,
      },
      Name: {
        title: 'Beneficiary Name',
        filter: false,
      },
      ICPassport: {
        title: 'Beneficiary IC/Passport',
        filter: false,
      },
      BankName: {
        title: 'Bank Name',
        filter: false,
      },
      BankAccountNumber: {
        title: 'Bank Account Number',
        filter: false,
      },
      BankPlace: {
        title: 'Bank Place',
        filter: false,
      },
      BankSwiftCode: {
        title: 'Bank Swift Code',
        filter: false,
      },
      Currency: {
        title: 'Currency',
        filter: false,
      },
      DocAuthorize: {
        title: 'Authorization',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          const data = row;
          if (value) {
            return { label: 'Download Authorization', filePath: value }
          } else {
            // return { label: 'Not Available', filePath: value, type: 'generate' }
            return { label: 'Generate BAF', filePath: value, type: 'generate', callback: () => {
              this._generateDocService.BAF(data.Email, data.ApplyID, localStorage.getItem('adminUsername')).subscribe(
                (result: any[]) => {
                  alert('Successfully generate BAF documents for ' + data.Email)
                  window.location.reload();
                },
                (err) => alert('Failed to generate document')
              )
            }}
          }
        }
      },
      DocBankBook: {
        title: 'Bank Book',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          if (value) {
            return { label: 'Download Bank Book', filePath: value }
          } else {
            return { label: 'Not Available', filePath: value, type: 'generate' }
          }
        }
      },
      DocICPassport: {
        title: 'IC / Passport',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          if (value) {
            return { label: 'Download IC/Passport', filePath: value }
          } else {
            return { label: 'Not Available', filePath: value, type: 'generate' }
          }
        }
      },
      DocBirthCert: {
        title: 'Birth Cert Crew',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          if (value) {
            return { label: 'Download Birth Cert', filePath: value }
          } else {
            return { label: 'Not Available', filePath: value, type: 'generate' }
          }
        }
      },
      DocBirthCert2: {
        title: 'Birth Cert Sibling',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          if (value) {
            return { label: 'Download Birth Cert', filePath: value }
          } else {
            return { label: 'Not Available', filePath: value, type: 'generate' }
          }
        }
      },
      DocMarriageCert: {
        title: 'Marriage Cert',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => {
          if (value) {
            return { label: 'Download Marriage Cert', filePath: value }
          } else {
            return { label: 'Not Available', filePath: value, type: 'generate' }
          }
        }
      },
    },
    actions: {
      delete: false,
      add: false,
      edit: false,
    },
  }

  constructor(
    // private _subscription: Subscription,
    private _bankAllotmentService: BankAllotmentService,
    private _vesselNameService: VesselNameService,
    private _generateDocService: GenerateDocService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.username = localStorage.getItem('admin_user_name');
    this.userEmail = localStorage.getItem("admin_user_email");
    this.activateStatus = localStorage.getItem("admin_verify_status");
    if (this.activateStatus === "0" && this.username !== 'admin') {
      alert("Please activate your account first.")
      this.router.navigate(['/pages/settings/change-password'])
    }

    this._getBankAllotment()
    this._getVessel()
  }

  ngOnDestroy(): void {
    // if (this._subscription) {
    //   this._subscription.unsubscribe()
    // }
  }

  formatDate(date: Date) {
    return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear()
  }

  filterDate(dateFrom: Date, dateTo: Date, date: Date) {
    return date >= dateFrom && date <= dateTo
  }

  // Service section
  private async _getBankAllotment() {
    const subscription = await new Promise((resolve, reject) => {
      return this._bankAllotmentService.getAllBankAllotment().subscribe(
        (result: any) => {
          if (result) {
            this.bankAllotmentList = result
            this._refreshBankAllotmentListData()
          }
          resolve(true)
        },
        (err) => {
          alert('Failed to load list')
          reject(false)
        }
      )
    })
  }

  private async _getVessel() {
    const subcription = await new Promise((resolve, reject) => {
      return this._vesselNameService.getVesselName().subscribe(
        (result: any) => {
          this.vesselList = [{ VesselCode:'All', VesselName:'All'}]
          this.vesselList = this.vesselList.concat(result) 
          resolve(true)
        },
        (err) => { 
          alert('Failed to load Vessel Option(s)')
          resolve(true)
        }
      )
    })
  }

  // Alter service response section
  async _refreshBankAllotmentListData() {
    await this.source.load(
      this.bankAllotmentList.map((item, index: number) => {
        var fullname = [item.Name, item.MiddleName, item.LastName].filter(Boolean).join(" ");
        return {
          No: index + 1,
          Id: item.Id,
          ApplyID: item.ApplyID,
          Email: item.LoginEmail,
          Vessel: item.NameofVessel,
          ContractPeriodFrom: this.formatDate(new Date(item.ContractPeriodFrom)),
          ContractPeriodTo: this.formatDate(new Date(item.ContractPeriodTo)),
          Status: item.Status,
          Name:fullname,
          ICPassport:item.Identification,
          BankName:item.BankName,
          BankAccountNumber:item.AccountNumber,
          BankPlace:item.BankPlace,
          BankSwiftCode:item.BankSwiftCode,
          Currency:item.Currency,
          DocAuthorize:item.FileBAF ? item.LoginEmail + '/' + item.FileBAF : '',
          DocBankBook:item.FileBankBook ? item.LoginEmail + '/' + item.FileBankBook : '',
          DocICPassport:item.FileICPassport ? item.LoginEmail + '/' + item.FileICPassport : '',
          DocBirthCert:item.FileBirthCert ? item.LoginEmail + '/' + item.FileBirthCert : '',
          DocBirthCert2:item.FileBirthCert2 ? item.LoginEmail + '/' + item.FileBirthCert2 : '',
          DocMarriageCert:item.FileMarriageCert ? item.LoginEmail + '/' + item.FileMarriageCert : ''
        }
      })
    )
  }

  async onSearch(position: string = '') {

    let isSearchByDate = true;
    let dateToObject = new Date();
    let dateFromObject = new Date();
    if (this.searchDate2 && this.searchDate) {
      const dateTo = this.searchDate2
      const dateFrom = this.searchDate
      let dts = dateTo.split(/[-]/)
      dateToObject = new Date(dateTo)
      dts = dateFrom.split(/[-]/)
      dateFromObject = new Date(dateFrom)
    } else {
      isSearchByDate = false;
    }

    const retval = []
    for (let i = 0; i < this.bankAllotmentList.length; i++) {
      const dateString = this.bankAllotmentList[i].ContractPeriodFrom
      const dateString2 = this.bankAllotmentList[i].ContractPeriodTo
      // const t = dateString.split(/[- :]/)
      // const date = new Date(parseInt(t[0]), parseInt(t[1]) - 1, parseInt(t[2]))
      const dateContractFrom = new Date(dateString)
      const dateContractTo = new Date(dateString2)
      let filterCond = true

      if (isSearchByDate) { // No dateTo
        filterCond = this.filterDate(dateFromObject, dateToObject, dateContractFrom)
      }

      if (this.searchVessel.length > 0 && this.searchVessel != 'All' && this.bankAllotmentList[i].NameofVessel != this.searchVessel) {
        filterCond = false
      }

      let applicantName = ''

      if (this.bankAllotmentList[i].Name != null && this.bankAllotmentList[i].Name != null ) {
        applicantName = applicantName + this.bankAllotmentList[i].Name
      }

      if (this.bankAllotmentList[i].MiddleName != null && this.bankAllotmentList[i].MiddleName != '') {
        applicantName = applicantName + ' ' + this.bankAllotmentList[i].MiddleName
      }

      if (this.bankAllotmentList[i].LastName != null && this.bankAllotmentList[i].LastName != '') {
        applicantName = applicantName + ' ' + this.bankAllotmentList[i].LastName
      }

      if (this.searchName.length > 0 && applicantName.toLowerCase().indexOf(this.searchName.toLowerCase()) < 0) {
        filterCond = false
      } 

      if (this.searchCrewEmail.length > 0 && this.bankAllotmentList[i].ApplicantID.toLowerCase().indexOf(this.searchCrewEmail.toLowerCase()) < 0) {
        filterCond = false
      } 

      if (filterCond) {
        retval.push(this.bankAllotmentList[i])
      }
    }

    await this.source.load(
      retval.map((item, index: number) => {
        var fullname = [item.Name, item.MiddleName, item.LastName].filter(Boolean).join(" ");
        return {
          No: index + 1,
          Id: item.Id,
          ApplyID: item.ApplyID,
          Email: item.LoginEmail,
          Vessel: item.NameofVessel,
          ContractPeriodFrom: this.formatDate(new Date(item.ContractPeriodFrom)),
          ContractPeriodTo: this.formatDate(new Date(item.ContractPeriodTo)),
          Status: item.Status,
          Name:fullname,
          ICPassport:item.Identification,
          BankName:item.BankName,
          BankAccountNumber:item.AccountNumber,
          BankPlace:item.BankPlace,
          BankSwiftCode:item.BankSwiftCode,
          Currency:item.Currency,
          DocAuthorize:item.FileBAF ? item.LoginEmail + '/' + item.FileBAF : '',
          DocBankBook:item.FileBankBook ? item.LoginEmail + '/' + item.FileBankBook : '',
          DocICPassport:item.FileICPassport ? item.LoginEmail + '/' + item.FileICPassport : '',
          DocBirthCert:item.FileBirthCert ? item.LoginEmail + '/' + item.FileBirthCert : '',
          DocBirthCert2:item.FileBirthCert2 ? item.LoginEmail + '/' + item.FileBirthCert2 : '',
          DocMarriageCert:item.FileMarriageCert ? item.LoginEmail + '/' + item.FileMarriageCert : ''
        }
      })
    )
    await this._setupExportData()
  }

  private async _setupExportData() {
    const data = (await this.source.getAll() as any[]).map((row) => { 
      const rowData = []
      rowData.push(row.ApplyPosition)
      rowData.push(row.LoginEmail)
      rowData.push(row.Name)
      rowData.push(row.ApplyDtApplication)
      rowData.push(row.ApplyStatus)
      return rowData
    })
    this.exportData = [['Position', 'Email', 'Applicant Name', 'Profile Updated Date', 'Status'], ...data]
  }
}
