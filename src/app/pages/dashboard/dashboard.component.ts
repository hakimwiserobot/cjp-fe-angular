import { Component, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router';
import { Subscription } from 'rxjs'

import { formatDate } from '@angular/common'
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable'

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
})

export class DashboardComponent implements OnInit, OnDestroy {
  _subscription: Subscription
  username: string
  userEmail: string
  activateStatus: string

  constructor(
    private router: Router
  ) {}
  
  ngOnInit(): void {
    this.username = localStorage.getItem('admin_user_name');
    this.userEmail = localStorage.getItem("admin_user_email");
    this.activateStatus = localStorage.getItem("admin_verify_status");

    console.log("enter admin dashboard")
    console.log(this.username)
    console.log(this.userEmail)
    console.log(this.activateStatus)

    if (this.activateStatus === "0" && this.username !== 'admin') {
      alert("Please activate your account first.")
      this.router.navigate(['/pages/settings/change-password'])
    }

    // if (!localStorage.getItem('foo')) { 
    //   localStorage.setItem('foo', 'no reload') 
    //   location.reload() 
    // } else {
    //   localStorage.removeItem('foo')  
    // }
   
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe()
    }
  }
}
