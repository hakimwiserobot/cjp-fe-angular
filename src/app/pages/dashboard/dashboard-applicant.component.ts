import { Component, OnDestroy, OnInit } from '@angular/core'
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard-applicant.component.html',
})
export class DashboardApplicantComponent implements OnInit {
  username: string

  constructor(private translate:TranslateService) {

  }

  ngOnInit(): void {
    this.username = localStorage.getItem('user_name');
  }

  useLanguage(event) {
    if (event.srcElement.innerText == "BM") {
      event.srcElement.innerText = 'EN'
      localStorage.setItem('language', 'my');
      this.translate.use('my');
    } else {
      event.srcElement.innerText = 'BM'
      localStorage.setItem('language', 'en');
      this.translate.use('en');
    }
  }
}
