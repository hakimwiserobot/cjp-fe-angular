import { TestBed } from '@angular/core/testing'

import { VesselTimesheetService } from './vesseltimesheet.service'

describe('ImonoService', () => {
  let service: VesselTimesheetService

  beforeEach(() => {
    TestBed.configureTestingModule({})
    service = TestBed.inject(VesselTimesheetService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
