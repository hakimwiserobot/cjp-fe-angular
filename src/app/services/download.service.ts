import { Injectable } from '@angular/core';
import { BaseService } from './base.service'

@Injectable({
  providedIn: 'root'
})
export class DownloadService {

  private _entity = 'download'

  constructor(private service: BaseService) { }

  file(email: string, filename: string) {
    return this.service.downloadFile(email, filename)
  }

  timesheet(filename: string) {
    return this.service.downloadFile_Timesheet(filename)
  }
}
