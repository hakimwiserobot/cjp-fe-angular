import { Injectable } from '@angular/core';
import { BaseService } from './base.service'

@Injectable({
  providedIn: 'root'
})
export class BankAllotmentService {
  private _entity = 'bankallotment'

  constructor(private service: BaseService) { }

  getAllBankAllotment() {
    return this.service.getSecondaryEntity(this._entity, 'all' + this._entity)
  }

  getBankAllotment(Id) {
    return this.service.getById(this._entity, Id)
  }

  getBankAllotmentByApplicantId(ApplicantId) {
    return this.service.getSecondaryEntityById(this._entity, this._entity + 'applicant', ApplicantId)
  }

  addBankAllotment(data: any) {
    return this.service.add(this._entity, data)
  }

  updateBankAllotmentDetails(data: any) {
    return this.service.updateSecondaryEntity(this._entity, this._entity + 'details', data)
  }

  updateBankAllotmentfiles(data: any) {
    return this.service.updateSecondaryEntity(this._entity, this._entity + 'files', data)
  }

  updateBankAllotmentSelections(data: any) {
    return this.service.updateSecondaryEntity(this._entity, this._entity + 'selections', data)
  }
}
