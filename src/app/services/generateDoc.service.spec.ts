import { TestBed } from '@angular/core/testing';

import { GenerateDocService } from './generateDoc.service';

describe('GenerateDocService', () => {
  let service: GenerateDocService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenerateDocService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
