import { TestBed } from '@angular/core/testing';

import { BankAllotmentService } from './bankAllotment.service';

describe('BankAllotmentService', () => {
  let service: BankAllotmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BankAllotmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
