import { Injectable } from '@angular/core';

import { BaseService } from './base.service'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _entity = 'users'
  private _secondaryentity = 'admin'
  private _thirdentity = 'user'
  private _updateuserpassword = 'userpassword'
  private _updateuseremail = 'useremail'
  private _updateadminemail = 'adminemail'
  private _updateadmindefaultemail = 'admindefaultemail'
  private _updateadminpassword = 'adminpassword'
  private _updateusericpassport = 'usericpassport'
  private _resendactivation = 'resendactivation'
  private _resendactivationadmin = 'resendactivationadmin'
  private _forgotpassword = 'forgotpassword' 
  private _verifyemail = 'verifyemail' 
  private _verifyemailadmin = 'verifyemailadmin' 

  constructor(private service: BaseService) { }

  loginAdmin(data: any) {
    return this.service.loginAdmin(this._entity, this._secondaryentity, data)
  }

  loginUser(data: any) {
    return this.service.loginUser(this._entity, this._thirdentity, data)
  }

  addApplicantUser(data: any) {
    return this.service.addSecondaryEntity(this._entity, this._thirdentity, data)
  }

  getAdminUser() {
    return this.service.get(this._entity)
  }

  addAdminUser(data: any) {
    return this.service.add(this._entity, data)
  }

  updateAdminPassword(data: any) {
    return this.service.updateSecondaryEntity(this._entity,this._updateadminpassword, data)
  }

  updateUserPassword(data: any) {
    return this.service.updateSecondaryEntity(this._entity,this._updateuserpassword, data)
  }

  updateUserEmail(data: any) {
    return this.service.updateSecondaryEntity(this._entity,this._updateuseremail, data)
  }

  updateAdminEmail(data: any) {
    return this.service.updateSecondaryEntity(this._entity,this._updateadminemail, data)
  }

  updateAdminDefaultEmail(data: any) {
    return this.service.updateSecondaryEntity(this._entity,this._updateadmindefaultemail, data)
  }

  forgotPassword(data: any) {
    return this.service.customPutSecondaryEntity(this._entity,this._forgotpassword, data)
  }

  resendActivation(data: any) {
    return this.service.updateSecondaryEntity(this._entity,this._resendactivation, data)
  }

  resendActivationAdmin(data: any) {
    return this.service.updateSecondaryEntity(this._entity,this._resendactivationadmin, data)
  }

  verifyEmail(data: any) {
    return this.service.customPutSecondaryEntity(this._entity,this._verifyemail, data)
  }

  verifyEmailAdmin(data: any) {
    return this.service.customPutSecondaryEntity(this._entity,this._verifyemailadmin, data)
  }

  deleteAdmin(id: string) {
    return this.service.delete(this._entity, id)
  }

  updateUserICPassport(data: any) {
    return this.service.updateSecondaryEntity(this._entity,this._updateusericpassport, data)
  }
}