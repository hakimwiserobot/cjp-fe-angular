import { Injectable } from '@angular/core';
import { BaseService } from './base.service'

@Injectable({
  providedIn: 'root'
})
export class GenerateDocService {

  private _entity = 'generate'

  constructor(private service: BaseService) { }

  AFE(userEmail: string, applyId: string, adminName: string) {
    return this.service.addSecondaryEntity(this._entity, 'AFE', { LoginEmail:userEmail, ApplyID:applyId })
  }

  CV(userEmail: string, applyId: string, adminName: string) {
    return this.service.addSecondaryEntity(this._entity, 'CV', { LoginEmail:userEmail, ApplyID:applyId })
  }

  SEA(userEmail: string, applyId: string, adminName: string) {
    return this.service.addSecondaryEntity(this._entity, 'SEA', { LoginEmail:userEmail, ApplyID:applyId })
  }

  BAF(userEmail: string, applyId: string, adminName: string) {
    return this.service.addSecondaryEntity(this._entity, 'BAF', { LoginEmail:userEmail, ApplyID:applyId })
  }

  SendEmail(applicationId: string) {
    return this.service.addSecondaryEntity('email', 'approved', { Id:applicationId })
  }
}
