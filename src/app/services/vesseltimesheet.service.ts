import { Injectable } from '@angular/core'

import { BaseService } from './base.service'

@Injectable({
  providedIn: 'root'
})
export class VesselTimesheetService {
  private _entity = 'vesseltimesheet'

  constructor(private service: BaseService) { }

  getAllVesselTimesheets(name: string, vessel: string, position: string, dateFrom: string, dateTo: string) {
    return this.service.getWithParams(this._entity, { dateFrom, dateTo, position, vessel, name });
  }

  getExport(data: any) {
    return this.service.addSecondaryEntity(this._entity, "export", data)
  }
}
