import { Injectable } from '@angular/core'
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http'

import { throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { AuthService } from '../auth.service'

import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment'
import { StringMapWithRename } from '@angular/compiler/src/compiler_facade_interface';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  private apiServer = environment.apiUrlPrefix
  private isRefreshingToken = false;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient, private authService: AuthService,
    private router: Router) {
    //auth_token = authService.loggedIn
  }

  constructHttpOptions() {
    const lang = localStorage.getItem('language');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.authService.getAccessToken(),
        'lang': lang ? lang : ''
      })
    }
  }

  registerAdmin(email: string, username: string, password: string, retyped_password: string) {
    this.constructHttpOptions()
    return this.httpClient
    .post<{token: string, name: string, email: string, verifyStatus: string}>(`${this.apiServer}` + '/users/add_admin',
    {email: email, username: username, password: password, retyped_password: retyped_password}, this.httpOptions)
    .pipe(
      map(result => {
        if(result.token) {
          localStorage.setItem('access_token', result.token);
          localStorage.setItem('admin_user_name', result.name);
          localStorage.setItem('admin_user_email', result.email);
          localStorage.setItem('admin_verify_status', result.verifyStatus);
          return true;
        }
        return false
      })
    );
  }



  loginAdmin(email: string, username: string, password: string) {
    this.constructHttpOptions()
    return this.httpClient
    .post<{token: string, name: string, email: string, UserID: string, AccessModule: any, verifyStatus: string}>(`${this.apiServer}` + '/users/login_admin',
    {email: email, username: username, password: password}, this.httpOptions)
    .pipe(
      map(result => {
        console.log('Admin Login')
        console.log(result);
        if(result.token) {
          if (localStorage.getItem('remember_me') != 'true') {
            sessionStorage.setItem('access_token', result.token);
          } else {
            localStorage.setItem('access_token', result.token);
          }       
          
          localStorage.setItem('admin_user_name', result.name);
          localStorage.setItem('admin_user_email', result.email);
          localStorage.setItem('admin_userID', result.UserID);
          localStorage.setItem('admin_accessModule', JSON.stringify(result.AccessModule));
          localStorage.setItem('admin_verify_status', result.verifyStatus);
          return true;
        }
        return false
        })
    );
  }

  registerUser(email: string, username: string, nric: string, passport: string, password: string, retyped_password: string) {
    this.constructHttpOptions()
    return this.httpClient
    .post<{token: string, name: string, ic: string, passport:string, email: string, id: string, verifyStatus: string}>(`${this.apiServer}` + '/users/add_user',
    {email: email, username: username, ic: nric, passport: passport, password: password, retyped_password: retyped_password}, this.httpOptions)
    .pipe(
      map(result => {
        if(result.token) {
          if (localStorage.getItem('remember_me') != 'true') {
            sessionStorage.setItem('access_token', result.token);
          } else {
            localStorage.setItem('access_token', result.token);
          }

          localStorage.setItem('verify_status', result.verifyStatus);
          localStorage.setItem('user_name', result.name);
          localStorage.setItem('user_email', result.email);
          localStorage.setItem('user_id', result.id);

          return true;
        }
        return false
      })
    );
  }

  loginUser(email: string, username: string, password: string) {
    this.constructHttpOptions()
    return this.httpClient
    .post<{token: string, name: string, email: string, id: string, verifyStatus: string}>(`${this.apiServer}` + '/users/login_user',
    {email: email, username: username, password: password}, this.httpOptions)
      .pipe(
        map(result => {
          if(result.token) {
            if (localStorage.getItem('remember_me') != 'true') {
              sessionStorage.setItem('access_token', result.token);
            } else {
              localStorage.setItem('access_token', result.token);
            }

            localStorage.setItem('verify_status', result.verifyStatus);
            localStorage.setItem('user_name', result.name);
            localStorage.setItem('user_email', result.email);
            localStorage.setItem('user_id', result.id);

            return true;
          }
          return false
         })
      );
    /*return this.httpClient
    .post<{token: string, name: string, email: string}>(`${this.apiServer}` + '/users/login_user',
    {email: email, username: username, password: password}, this.httpOptions)
    .pipe(catchError(this.errorHandler));*/
  }

  logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('user_name');
    localStorage.removeItem('user_email');
    localStorage.removeItem('verify_status');
    localStorage.clear();
    sessionStorage.clear()
  }

  logoutAdmin() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('admin_user_name');
    localStorage.removeItem('admin_user_email');
    localStorage.removeItem('admin_verify_status');
    localStorage.clear();
    sessionStorage.clear()
  }

  async getAccessToken(): Promise<string> {
    // Verify if token has expired
    // Refresh token if not yet expired
    let dateToday = new Date();
    let dateExpired = '';
    let token = '';

    if (sessionStorage.getItem('access_token')) {
      dateExpired = sessionStorage.getItem('token_expired')
      token = sessionStorage.getItem('access_token');
    } else if (localStorage.getItem('access_token')) {
      dateExpired = localStorage.getItem('token_expired')
      token = localStorage.getItem('access_token');
    } else if (sessionStorage.getItem('access_token')) {
      dateExpired = sessionStorage.getItem('token_expired')
      token = sessionStorage.getItem('access_token');
    } else {
      dateExpired = localStorage.getItem('token_expired')
      token = localStorage.getItem('access_token');
    }

    // if (!this.isRefreshingToken) {
    //   if ((dateExpired == null && token != null) 
    //   || (new Date().getHours() == new Date(dateExpired).getHours() && new Date().getMinutes() <= (new Date(dateExpired).getMinutes()+15))) {
    //     await new Promise((resolve, reject) => {
    //       this.refreshToken().subscribe(result => {
    //         console.log(result);
    //         resolve(result);
    //       })
    //     })
    //   }
    // }

    return token;
  }

  refreshToken() {
    this.isRefreshingToken = true;
    let isAdmin = false;

    let token = '';
    if (sessionStorage.getItem('access_token')) {
      isAdmin = true;
      token = sessionStorage.getItem('access_token');
    } else if (localStorage.getItem('access_token')) {
      isAdmin = true;
      token = localStorage.getItem('access_token');
    } else if (sessionStorage.getItem('access_token')) {
      token = sessionStorage.getItem('access_token');
    } else {
      token = localStorage.getItem('access_token');
    }

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      })
    }    
    return this.httpClient
    .post<{success: boolean, token: string, expiredAt: string }>(`${this.apiServer}` + '/token-refresh', {}, this.httpOptions)
      .pipe(
        map(result => {
          if(result.success) {
            if (localStorage.getItem('remember_me') != 'true') {
              if (isAdmin) {
                sessionStorage.setItem('access_token', result.token);
              } else {
                sessionStorage.setItem('access_token', result.token);
              }
              sessionStorage.setItem('token_expired', result.expiredAt);
            } else {
              if (isAdmin) {
                localStorage.setItem('access_token', result.token);
              } else {
                localStorage.setItem('access_token', result.token);
              }
              localStorage.setItem('token_expired', result.expiredAt);
            }

            this.isRefreshingToken = false;
            return true;
          }
          return false
         })
      );
  }

  public get loggedIn(): boolean {
    if (sessionStorage.getItem('access_token') !== null) {
      return (sessionStorage.getItem('access_token') !== null);
    } else {
      return (localStorage.getItem('access_token') !== null);
    }
  }

  get(entity: string) {
    this.constructHttpOptions()
    return this.httpClient
      .get(`${this.apiServer}/${entity}/get_${entity}`,
      this.httpOptions)
      .pipe(catchError(this.errorHandler))
  }

  getWithParams(entity: string, params: any) {
    var paramQuery = "" // Update by Hakim on 1 Feb 2021

    for (let key in params) {
      paramQuery += `${key}=${params[key]}&`
    }

    return this.httpClient
      .get(`${this.apiServer}/${entity}/get_${entity}?${paramQuery}`)
      .pipe(catchError(this.errorHandler))
  }

  getWithParamsAsUrl(entity: string, params: any) {
    const paramQuery = Object.values(params).join('/') // Update by Hakim on 1 Feb 2021

    return this.httpClient
      .get(`${this.apiServer}/${entity}/get_${entity}/${paramQuery}`)
      .pipe(catchError(this.errorHandler))
  }

  getSecondaryEntityWithParams(entity: string, secondaryEntity: string, params: any) {
    this.constructHttpOptions()
    var paramQuery = "" // Update by Hakim on 1 Feb 2021

    for (let key in params) {
      paramQuery += `${key}=${params[key]}&`
    }

    return this.httpClient
      .get(`${this.apiServer}/${entity}/get_${secondaryEntity}?${paramQuery}`,
      this.httpOptions)
      .pipe(catchError(this.errorHandler))
  }

  getSecondaryEntity(entity: string, secondaryEntity: string) {
    this.constructHttpOptions()
    return this.httpClient
      .get(`${this.apiServer}/${entity}/get_${secondaryEntity}`,
      this.httpOptions)
      .pipe(catchError(this.errorHandler))
  }

  getSecondaryEntityById(entity: string, secondaryEntity: string, id: string) {
    this.constructHttpOptions()
    return this.httpClient
      .get(`${this.apiServer}/${entity}/get_${secondaryEntity}/${id}`,
      this.httpOptions)
      .pipe(catchError(this.errorHandler))
  }

  getById(entity: string, id: string) {
    this.constructHttpOptions()
    return this.httpClient
      .get(`${this.apiServer}/${entity}/get_${entity}/${id}`,
      this.httpOptions)
      .pipe(catchError(this.errorHandler))
  }

  getWithUrl(url: string) {
    // this.constructHttpOptions()
    return this.httpClient
      .get(`${url}`)
      .pipe(catchError(this.errorHandler))
  }

  add(entity: string, data: any) {
    this.constructHttpOptions()
    return this.httpClient
      .post(
        `${this.apiServer}/${entity}/add_${entity}`,
        data,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  addSecondaryEntity(entity: string, secondaryEntity: string, data: any) {
    this.constructHttpOptions()
    return this.httpClient
      .post(
        `${this.apiServer}/${entity}/add_${secondaryEntity}`,
        data,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  update(entity: string, data: any) { 
    this.constructHttpOptions()
    return this.httpClient
      .put(
        `${this.apiServer}/${entity}/update_${entity}`,
        data,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  updateWithId(entity: string, updateId: string, data: any) {
    this.constructHttpOptions()
    return this.httpClient
      .put(
        `${this.apiServer}/${entity}/update_${entity}/${updateId}`,
        data,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  // Added by Hakim on 3 Feb 2021 - Start
  updatePasswordWithId(entity: string, updateId: string, data: any) {
    this.constructHttpOptions()
    return this.httpClient
      .put(
        `${this.apiServer}/${entity}/update_${entity}password/${updateId}`,
        data,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }
  // Added by Hakim on 3 Feb 2021 - End

  updateSecondaryEntity(entity: string, secondaryEntity: string, data: any) {
    this.constructHttpOptions()
    return this.httpClient
      .put(
        `${this.apiServer}/${entity}/update_${secondaryEntity}`,
        data,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  customPutSecondaryEntity(entity: string, customSecondaryEntity: string, data: any) {
    this.constructHttpOptions()
    return this.httpClient
      .put(
        `${this.apiServer}/${entity}/${customSecondaryEntity}`,
        data,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  delete(entity: string, id: string) {
    this.constructHttpOptions()
    return this.httpClient
      .delete(
        `${this.apiServer}/${entity}/delete_${entity}/${id}`,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  deleteSecondaryEntity(entity: string, secondaryEntity: string, id: string) {
    this.constructHttpOptions()
    return this.httpClient
      .delete(
        `${this.apiServer}/${entity}/delete_${secondaryEntity}/${id}`,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  downloadFile(email: string, filename: string) {
    const lang = localStorage.getItem('language');
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.getAccessToken(),
      'lang': lang ? lang : ''
    });

    return this.httpClient
    .get(`${this.apiServer}/download/get_document?email=${email}&filename=${filename}`, { headers, responseType: 'blob' }) //set response Type properly (it is not part of headers)
    .pipe(catchError(this.errorHandler))
  }

  downloadFile_Timesheet(filename: string) {
    const lang = localStorage.getItem('language');
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.getAccessToken(),
      'lang': lang ? lang : ''
    });

    return this.httpClient
    .get(`${this.apiServer}/download/get_timesheet?filename=${filename}`, { headers, responseType: 'blob' }) //set response Type properly (it is not part of headers)
    .pipe(catchError(this.errorHandler))
  }

  uploadFile(Id: string, userId: string, file: File, columnType: string) {
    //this.constructHttpOptions()
    // reconstruct http opt
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.getAccessToken()
      })
    }

    const formData = new FormData()
    formData.append('Id', Id)
    formData.append('file', file)
    formData.append('UserID', userId)
    formData.append('ColumnType', columnType)

    return this.httpClient
      .post(
        `${this.apiServer}/file_upload/update_fileupload`,
        formData,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  uploadFileWithData(path: string, data:{}, file: File) {
    //this.constructHttpOptions()
    // reconstruct http opt
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.getAccessToken()
      })
    }

    const formData = new FormData()
    formData.append('file', file)

    return this.httpClient
      .post(
        `${this.apiServer}${path}`,
        formData,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  deleteFile(entity: string, columnToDelete: string, Id: string) {
    this.constructHttpOptions()
    return this.httpClient
      .delete(
        `${this.apiServer}/${entity}/delete_${columnToDelete}/${Id}`,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  getAdminDetails(entity: string,secondaryEntity: string, Id: string) {
    this.constructHttpOptions()
    return this.httpClient
      .get(
        `${this.apiServer}/${entity}/${secondaryEntity}/${Id}`,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler))
  }

  errorHandler(error: any) {
    let errorMessage = ''
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message
    } else if (error.error.message != null) {
      errorMessage = error.error.message
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`
      if(error.status == 401) {
        this.router.navigate(['applicant-login']);
      }
    }

    return throwError(errorMessage)
  }
}
