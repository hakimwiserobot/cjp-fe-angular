import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table'
import { NbSpinnerModule } from '@nebular/theme'

import { PagingTableComponent } from './paging-table.component';

@NgModule({
  declarations: [PagingTableComponent],
  exports: [PagingTableComponent],
  imports: [
    CommonModule,
    Ng2SmartTableModule,
    NbSpinnerModule
  ]
})
export class PagingTableModule { }
