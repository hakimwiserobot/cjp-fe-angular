import { Component, Input, OnInit, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';

import { DownloadButton } from '../../shared/DownloadButton.component'
import { LocalDataSource } from 'ng2-smart-table'

import { GenerateDocService } from '../../services/generateDoc.service'
import { Int } from 'mssql';

@Component({
  selector: 'paging-table',
  templateUrl: './paging-table.component.html',
  styleUrls: ['./paging-table.component.scss']
})
export class PagingTableComponent implements OnInit, OnChanges {

  @Input() value = {
    data: [],
    totalRow: 0,
    page: {
      current: 0,
      total: 0
    },
  };
  @Input() isRefreshingTable = true;
  @Input() onChangePage: (next: number) => void;

  private _data = [];
  private _index = 1;
  private _rowPerPage = 10;
  public currentPage: number;
  private _totalRow: number;
  totalPage: number;
  
  public source = new LocalDataSource();
  public prevPagesToSelect = [];
  public nextPagesToSelect = [];
  public settings = {
    hideSubHeader: true, // hide the add new fields
    columns: {
      No: {
        title: 'No',
        filter: false,
        editable: false,
        addable: false,
      },
      Id: {
        title: 'Id',
        hide: true
      },
      Position: {
        title: 'Position',
        filter: false,
      },
      Email: {
        title: 'Email',
        filter: false,
      },
      Name: {
        title: 'Name',
        filter: false,
      },
      VesselName: {
        title: 'Vessel Name',
        filter: false,
      },
      DtApplication: {
        title: 'Profile',
        filter: false,
      },
      Status: {
        title: 'Status',
        filter: false,
      },
      Afe: {
        title: 'AFE',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => { return this.valuePrepareFunctionBtnDownload(value, row, 'AFE'); }
      },
      Cv: {
        title: 'CV',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => { return this.valuePrepareFunctionBtnDownload(value, row, 'CV'); }
      },
      Sea: {
        title: 'SEA',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => { return this.valuePrepareFunctionBtnDownload(value, row, 'SEA'); }
      },
      SeaPDF: {
        title: 'SEA PDF',
        type: 'custom',
        filter: false,
        renderComponent: DownloadButton,
        valuePrepareFunction: (value, row) => { return this.valuePrepareFunctionBtnDownload(value, row, 'SEA PDF'); }
      }
    },
    actions: {
      delete: false,
      add: false,
      edit: false,
    },
  };

  constructor(private generateDocService: GenerateDocService) { }

  ngOnInit(): void {
    this._data = this.value.data;
    if (this._data) {
      this.source.load(this._data);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (property === 'value') {
        this.value = changes[property].currentValue;
        this.setData();
      } else if (property === 'isRefreshingTable') {
        this.isRefreshingTable = changes[property].currentValue;
      }
    }
  }

  setData() {
    this.currentPage = this.value.page.current;
    this.totalPage = this.value.page.total;
    this._data = this.value.data;
    this._totalRow = this.value.totalRow;
    this._index = (this.currentPage-1) * this._rowPerPage;

    // Update index
    for (let i = 0; i < this._data.length; i++) {
      this._data[i].No = ++this._index;
    }

    this.source.load(this._data);
    this.setPageForSelection();
  }

  setPageForSelection() {
    this.prevPagesToSelect = [];
    this.nextPagesToSelect = [];
    
    let previousPage = this.currentPage - 1;
    let nextPage = this.currentPage + 1;
    for (let i = 1; i < 3; i++) {
      if (previousPage > 0) {
        this.prevPagesToSelect.unshift(previousPage--);
      }
      if (nextPage <= this.totalPage) {
        this.nextPagesToSelect.push(nextPage++);
      }
    }
  }

  valuePrepareFunctionBtnDownload(value: any, row: any, docType: String) {
    if (value) {

      if (docType === 'SEA') {
        value = value.replace('-result', '');
        value = value.replace('.pdf', '.docx');
      }

      return { label: `Download ${docType}`, filePath: value, type: 'download' }
    } else {
      const data = row;
      return { label: `Generate ${docType}`, filePath: value, type: 'generate', callback: () => {
          if (docType == 'SEA' || docType == 'SEA PDF') {

            this.generateDocService.SEA(data.Email, data.Id, localStorage.getItem('adminUsername')).subscribe(
              (result: any[]) => {
                alert(`Successfully generate SEA documents for ` + data.Email)
                window.location.reload();
              },
              (err) => alert('Failed to load generate documents')
            )
          } else if (docType == 'CV') {
            this.generateDocService.CV(data.Email, data.Id, localStorage.getItem('adminUsername')).subscribe(
              (result: any[]) => {
                alert('Successfully generate CV document for ' + data.Email)
                window.location.reload();
              },
              (err) => alert('Failed to load generate document')
            )
          } else if (docType == 'AFE') {
            this.generateDocService.AFE(data.Email, data.Id, localStorage.getItem('adminUsername')).subscribe(
              (result: any[]) => {
                alert('Successfully generate AFE document for ' + data.Email)
                window.location.reload();
              },
              (err) => alert('Failed to load generate document')
            )
          }
        }
      }
    }
  }

  async onClickChangePage(page) {
    console.log('Change to page ' + page)
    this.isRefreshingTable = true;
    if (typeof this.onChangePage === 'function') {
      await this.onChangePage(page);
    } else {
      console.log('No callback provided');
    }
    this.isRefreshingTable = false;
  }
}
