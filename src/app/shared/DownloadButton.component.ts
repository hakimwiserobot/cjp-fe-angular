import { Component, Input, OnInit } from '@angular/core'
import { DefaultEditor, ViewCell } from 'ng2-smart-table'
import { environment } from '../../environments/environment';
import { DownloadService } from '../services/download.service'

//var path = require("path");

@Component({
  selector: 'ngx-downloadbuttoncomponent',
  templateUrl: './DownloadButton.component.html',
})
export class DownloadButton extends DefaultEditor implements OnInit, ViewCell {
  @Input() value;
  @Input() rowData: any

  label: string
  type: string
  filePath: string | null
  url:string
  public myCallback: () => void;

  constructor(private downloadService: DownloadService) {
    super()
  }

  ngOnInit(): void {
    this.label = this.value.label
    this.type = this.value.type
    this.myCallback = this.value.callback
    this.filePath = this.value.filePath
    this.url = environment.documentPathPrefix +'/'+ this.filePath
  }

  download() {
    if (this.type == 'generate') {
      this.myCallback()
    } else {
      this._getApplicantDocument()
    }
  }

  private _getApplicantDocument() {
    
    if (this.filePath) {
      var arrFilePath = this.filePath.split('/');
      var email = arrFilePath[0];
      var filename = arrFilePath[1];

      if (arrFilePath.length == 2) {
        email = arrFilePath[0];
        filename = arrFilePath[1];
      } else if (arrFilePath.length == 3) {
        email = arrFilePath[1];
        filename = arrFilePath[2];
      } else {
        email = '';
        filename = arrFilePath[0];
      }

      arrFilePath = this.filePath.split('\\');
      if (arrFilePath.length == 4) {
        email = '';
        filename = arrFilePath[3];
      }

      this.downloadService.file(email, filename).subscribe(
        (result: any) => {
          if (result) {
            var url = window.URL.createObjectURL(result);
            var elementA = document.createElement("a");
            elementA.href = url;
            elementA.download = filename;
            elementA.click();
          }
        },
        (err) => {
          alert('Failed to download file');
        }
      )
    }
  }

  
}
