import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import SignaturePad from 'signature_pad';

@Component({
  selector: 'ngx-signature-dialog',
  templateUrl: './signature-dialog.component.html',
  styleUrls: ['./signature-dialog.component.scss']
})
export class SignatureDialogComponent implements OnInit {
  @ViewChild('sPadContainer', {static: true}) signaturePadContainerElement;
  @ViewChild('sPad', {static: true}) signaturePadElement;
  signaturePad: any;

  sPadWidth : number
  sPadHeight : number

  constructor(private translate: TranslateService,
    public dialogRef: MatDialogRef<SignatureDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    let cardReveal = document.getElementById('cardReveal') as HTMLSelectElement
    let cardDeclaration = document.getElementById('cardDeclaration') as HTMLSelectElement
    let signpad = document.getElementById('dialogContent') as HTMLSelectElement
    this.sPadWidth = this.signaturePadContainerElement.nativeElement.clientWidth;
    this.sPadHeight = window.innerHeight * 50 / 100;
    // this.sPadHeight = cardDeclaration.offsetHeight - 5; // Minus the pad default padding
    // this.sPadHeight = signpad.clientHeight;
    this.signaturePad = new SignaturePad(this.signaturePadElement.nativeElement);
    if (this.data) {
      this.signaturePad.fromDataURL(this.data);
    }
    this.signaturePad.minWidth = 2.5;
    this.signaturePad.maxWidth = 7;
  }

  onClear(): void {
    this.signaturePad.clear()
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {

    if (this.signaturePad.isEmpty()) {
      alert(this.translate.instant("alert.emptySignature"));
    } else {
      const dataURL = this.signaturePad.toDataURL();
      this.data = dataURL;
      this.dialogRef.close(this.data);
    }
  }

}
