import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { SignatureDialogComponent } from './signature-dialog.component';
import { MatDialogModule } from '@angular/material/dialog'
import { MatButtonModule} from '@angular/material/button'
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core'
import { NbAccordionModule } from '@nebular/theme';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    TranslateModule,
    NbAccordionModule
  ],
  declarations: [SignatureDialogComponent],
})
export class SignatureDialogModule { }
