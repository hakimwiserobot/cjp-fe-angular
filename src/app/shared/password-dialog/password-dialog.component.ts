import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApplicationService } from '../../services/application.service'
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ngx-password-dialog',
  templateUrl: './password-dialog.component.html',
  styleUrls: ['./password-dialog.component.scss']
})
export class PasswordDialogComponent implements OnInit {

  nricpassport: string = '';
  password: string = '';
  withNRICPassport: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<PasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public email: string,
    private applicationService: ApplicationService,
    private translate: TranslateService) { }

  ngOnInit(): void {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  async onConfirm(event) {
    var isAuthorize = await this.authenticateUserApplication();
    this.dialogRef.close(isAuthorize);
  }

  authenticateUserApplication() {
    var btnCancel = (<HTMLInputElement>document.getElementById('btnCancel'));
    var btnConfirm = (<HTMLInputElement>document.getElementById('btnConfirm'));

    btnCancel.disabled = true;
    btnConfirm.disabled = true;
    btnConfirm.innerText = this.translate.instant("btn.confirming");

    if(this.email == null || this.email == '') {
      this.email = localStorage.getItem("user_email")
    }

    return new Promise((resolve, reject) => {
      const subscription = this.applicationService.authenticateUserApplication(
        JSON.stringify({LoginEmail: this.email, Password: this.password, NRICPassport: this.nricpassport }))
        .subscribe((res: any) => {
          if (res == null || res === undefined || (Array.isArray(res) && res.length == 0) ) {
            alert('Incorrect NRIC, passport, or password');
            btnCancel.disabled = false;
            btnConfirm.disabled = false;
            btnConfirm.innerText = this.translate.instant("btn.confirm");
          } else {
            return resolve(true);
          }
          subscription.unsubscribe()
        })
    })
  }
}
